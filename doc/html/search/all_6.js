var searchData=
[
  ['galactic2equatorial',['Galactic2Equatorial',['../classROASt_1_1TAstroCatalogueConversions.html#a248fb616617619856cfff489af706e8c',1,'ROASt::TAstroCatalogueConversions::Galactic2Equatorial(const Double_t l, const Double_t b)'],['../classROASt_1_1TAstroCatalogueConversions.html#af532d5285c60896898d2872c88c19181',1,'ROASt::TAstroCatalogueConversions::Galactic2Equatorial(const fAstroGalacticCoordinates GalacticCoord)']]],
  ['galacticcoord',['GalacticCoord',['../structROASt_1_1fAstroCatalogueObject.html#aca1686073c4a0c56055268a8a0c3eb0b',1,'ROASt::fAstroCatalogueObject']]]
];
