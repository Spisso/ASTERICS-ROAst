var searchData=
[
  ['handle',['handle',['../structROASt_1_1elem__t.html#a7102d4654b40f486bfd036d09d05783d',1,'ROASt::elem_t']]],
  ['heliocentric2rectangular',['Heliocentric2Rectangular',['../classROASt_1_1TAstroCatalogueConversions.html#a85ac2a318d94fe34e83810019013743f',1,'ROASt::TAstroCatalogueConversions']]],
  ['horizontal2equatorial',['Horizontal2Equatorial',['../classROASt_1_1TAstroCatalogueConversions.html#aaf408c45efd855b6bf93851aaea12445',1,'ROASt::TAstroCatalogueConversions::Horizontal2Equatorial(const Double_t Alt, const Double_t Az, const Int_t day, Int_t month, Int_t year, const Int_t hour, const Int_t min, const Double_t sec, const Double_t Longitude, const Double_t Latitude)'],['../classROASt_1_1TAstroCatalogueConversions.html#ac14747f435d13d947df750004538d449',1,'ROASt::TAstroCatalogueConversions::Horizontal2Equatorial(const fAstroHorizontalCoordinates HorizontalCoord, const fAstroUTCCoordinates UTCCoord, fAstroLatLongCoordinates LatLongCoord)']]],
  ['horizontalcoord',['HorizontalCoord',['../structROASt_1_1fAstroCatalogueObject.html#aad75c1dcf34773696749982d3b91624d',1,'ROASt::fAstroCatalogueObject']]],
  ['hour',['hour',['../structROASt_1_1fAstroUTCCoordinates.html#a51265bbf87399e578470e6172ec8d335',1,'ROASt::fAstroUTCCoordinates']]]
];
