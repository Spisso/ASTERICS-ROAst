var searchData=
[
  ['heliocentric2rectangular',['Heliocentric2Rectangular',['../classROASt_1_1TAstroCatalogueConversions.html#a85ac2a318d94fe34e83810019013743f',1,'ROASt::TAstroCatalogueConversions']]],
  ['horizontal2equatorial',['Horizontal2Equatorial',['../classROASt_1_1TAstroCatalogueConversions.html#aaf408c45efd855b6bf93851aaea12445',1,'ROASt::TAstroCatalogueConversions::Horizontal2Equatorial(const Double_t Alt, const Double_t Az, const Int_t day, Int_t month, Int_t year, const Int_t hour, const Int_t min, const Double_t sec, const Double_t Longitude, const Double_t Latitude)'],['../classROASt_1_1TAstroCatalogueConversions.html#ac14747f435d13d947df750004538d449',1,'ROASt::TAstroCatalogueConversions::Horizontal2Equatorial(const fAstroHorizontalCoordinates HorizontalCoord, const fAstroUTCCoordinates UTCCoord, fAstroLatLongCoordinates LatLongCoord)']]]
];
