var searchData=
[
  ['usedecliptic',['UsedEcliptic',['../structROASt_1_1fAstroCatalogueObject.html#af9087a1c59a0f0ab0f20542946364deb',1,'ROASt::fAstroCatalogueObject']]],
  ['usedgalactic',['UsedGalactic',['../structROASt_1_1fAstroCatalogueObject.html#ac9a4e6f0130c58993652d97af0ef39da',1,'ROASt::fAstroCatalogueObject']]],
  ['usedhorizontal',['UsedHorizontal',['../structROASt_1_1fAstroCatalogueObject.html#acd99dc81c7054d68c019272894979786',1,'ROASt::fAstroCatalogueObject']]],
  ['utmeasting',['UTMEasting',['../structROASt_1_1fAstroUTMCoordinates.html#ae5307be3f97bccdd4a67121d630dfd2d',1,'ROASt::fAstroUTMCoordinates']]],
  ['utmnorthing',['UTMNorthing',['../structROASt_1_1fAstroUTMCoordinates.html#aa019a2f6cf9ef6692aefd65ec575e585',1,'ROASt::fAstroUTMCoordinates']]],
  ['utmzone',['UTMZone',['../structROASt_1_1fAstroUTMCoordinates.html#adeb60fb2f42c3d9fdc0eb5f111cda924',1,'ROASt::fAstroUTMCoordinates']]]
];
