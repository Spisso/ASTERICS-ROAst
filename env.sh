#!/bin/bash
export  ROOTSYS="/home/dino/root"
export  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib:/usr/lib/x86_64-linux-gnu/
export  PATH=$PATH:$ROOTSYS/bin
export  VIZQUERYPATH="/home/dino/ASTERICS-ROAst-git/ASTERICS-ROAst/"
#export  VIZQUERYPATH=
export  VIZIERSITE="vizier.u-strasbg.fr"
export  URAT1INDEXFILEPATH="/home/dino/root/URAT1/v12/"
export  URAT1FILEPATH="/home/dino/root/URAT1/v12/"
export  UCAC4INDEXFILEPATH="/home/dino/root/UCAC4/u4i/"
export  UCAC4FILEPATH="/home/dino/root/UCAC4/u4b/"


