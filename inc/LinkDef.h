#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ namespace ROASt;
#pragma link C++ defined_in namespace ROASt;
#pragma link C++ defined_in curl.h;
#pragma link C++ class ROASt::TROAstCatalogue;
#pragma link C++ class ROASt::TROAstConversions;
#pragma link C++ class ROASt::TROAstMoon;
#pragma link C++ class ROASt::TROAstSun;
#endif
