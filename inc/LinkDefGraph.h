#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ namespace ROASt;
#pragma link C++ defined_in namespace ROASt;
#pragma link C++ class ROASt::TROAstGraphics;
#endif

