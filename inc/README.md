The ROOT analysis framework is one of the most used software for the analysis and indeed it is the “de facto” 
standard for high-energy physics. The goal of the ROAst library (ROot extensions for ASTronomy)
is to extend the ROOT capabilities adding packages and tools for astrophysical research.


ROAst comes with three feature sets:

access to astronomical catalogues;

coordinate conversion tools;

high-precision Moon and Sun position models relative to the Earth;

graphical tools to produce commonly used plots (general and partial skymaps).

ROAst provides seamless access to the following catalogues: UCAC4, URAT1 (local), the VizieR online catalogue repository, 
VO (Virtual Observatory) catalogues (such as Euro-VO, MAST, GAVO, VAO, IVOA). An intermediate abstraction layer makes 
the addition of more catalogues easy to implement. Catalogue querying can be done extracting regions of various shapes 
(rectangles, circles, ellipses) around each object. ROAst supports equatorial, galactic, ecliptic, horizontal astronomical coordinates 
(using Lat-Long and UTM as geographical coordinates) and allows coordinate transformations.  Plots can be obtained in “flat” and “Aitoff” projection, 
in equatorial, galactic and horizontal coordinate systems (some combinations of projection and coordinate systems are not allowed).

