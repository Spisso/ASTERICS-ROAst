#ifndef __TROAstCatalogue__
#define __TROAstCatalogue__


#include "TROAstConversions.hh"

namespace ROASt
{

/** Main Abstract class.**/
class TROAstCatalogue : public TObject
{
protected:

	TROAstConversions *Conv;
	std::string GetStringEnv(char const* Env)
		{
		char const *env = getenv(Env);
		if ( env && !env[0] ) 
			{
			std::cout<<"Environment variable "<<Env<<" not set"<<std::endl;
			return 0;
			} 
		else 
			{
		   return env ;
			}
		}

public:
	  
	
	/** Public vector containing the astronomical objects found.**/
	std::vector<fAstroCatalogueObject> ObjectsCollection;

	/** Project the catalogue objects on a rectangular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector.**/
	virtual std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height, const std::string &Source) = 0;

	/** Project the catalogue objects on a rectangular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path.**/
	virtual std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height) = 0;

	/** Project the catalogue objects on a rectangular region, defined via horizontal coordinates, populating the ObjectsCollection vector.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height,const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &Source) = 0;
	
	/** Project the catalogue objects on a rectangular region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height,const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude) = 0;
	
	/** Project the catalogue objects on a circular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius, const std::string &Source) = 0;

	/** Project the catalogue objects on a circular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius) = 0;
	
	/** Project the catalogue objects on a circular region, defined via horizontal coordinates, populating the ObjectsCollection vector.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const  Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &Source) = 0;

	/** Project the catalogue objects on a circular region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const  Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude) = 0;
	
	/** Project the catalogue objects on a elliptical region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis, const std::string &Source) = 0;

	/** Project the catalogue objects on a elliptical region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis) = 0;
	
	/** Project the catalogue objects on a elliptical region, defined via horizontal coordinates, populating the ObjectsCollection vector.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &Source) = 0;

	/** Project the catalogue objects on a elliptical region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path.**/
	virtual	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude) = 0;

	/** Returns true if the specified object is found in the ObjectsCollection vector, with the option [Filter]  all the non matching object are erased from the vector.**/
	virtual Bool_t FindObject(const std::string &Options,const std::string &FeatureName, const Double_t MinFeature, const Double_t MaxFeature) = 0;

	/** Writes the contents of the ObjectsCollection vector to the [OutFileName] named file.**/
	virtual	void WriteObjects(const std::string &OutFileName) = 0 ;

	/** Prints to the stdandard output the list of the catalogue features.**/
	virtual void ListFeatures() = 0;

	/** Prints to the stdandard output the contents of the ObjectsCollection vector.**/
	virtual	void Print() = 0 ;

	/** Prints the content of j-th element of the ObjectsCollection vector to the stdandard output.**/
	virtual	void Print(const Int_t j) = 0;

	/** Creates the UCAC4 catalogue and returns a pointer to an instance of the TROAstUCAC4 class.**/
	static TROAstCatalogue *UCAC4Catalogue();

	/** Creates the URAT1 catalogue and returns a pointer to an instance of the TROAstURAT1 class.**/
	static TROAstCatalogue *URAT1Catalogue();

	/** Creates a generic Virtual Observatory (VO) on-line catalogue and returns apointer to an instance of the TROAstVO class.**/
	static TROAstCatalogue *VOCatalogue();

	/** Creates a generic VizieR on-line catalogue and returns a pointer to an instance of the TROAstVizR class.**/
	static TROAstCatalogue *VizRCatalogue();

	/** ROOT ClassDef directive for rootcint.**/
	ClassDef(TROAstCatalogue,1);

};

};

#endif



