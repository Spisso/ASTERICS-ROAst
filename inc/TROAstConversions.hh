  
#ifndef __TROAstConversions__
#define __TROAstConversions__

#include "TROAstElementsClasses.hh"

namespace ROASt
{
/** Coordinate conversions class **/
class TROAstConversions : public TObject
{
private:



	/** Returns the UTM letter for a given latitude **/
	static Char_t UTMLetterDesignator(const Double_t Lat);

	static Double_t GetDynamicalDiffSH1(Double_t JulianDay);
	static Double_t GetDynamicalDiffSH2(Double_t JulianDay);
	static Double_t GetDynamicalDiffTable(Double_t JulianDay);
	static Double_t GetDynamicalDiffNear(Double_t JulianDay);
	static Double_t GetDynamicalDiffOther(Double_t JulianDay);
	static Double_t GetDynamicalTimeDiff(Double_t JulianDay);


public:



	/** Default constructor.**/
	TROAstConversions() { std::cout << "This is the ROASt-Conversions Class,  v 1.0" << std::endl;};

	/** Destructor.**/
	~TROAstConversions(){};


	static Double_t JDtoJDE(Double_t JulianDay);


	static fAstroNutationCoordinates GetNutation(Double_t JulianDay);

	/** Returns the sidereal local time for a given Gregorian day and UTC time.**/
	static Double_t LocalMeanSiderealTime(const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude)  ;


	/** Converts LatLong geographical coordinates to UTM coordinates.**/
	static fAstroUTMCoordinates LL2UTM(Int_t ReferenceEllipsoid, const Double_t Longitude, const Double_t Latitude);

	/** Converts LatLong geographical coordinates to UTM coordinates.**/
	static fAstroUTMCoordinates LL2UTM(const Int_t ReferenceEllipsoid, const fAstroLatLongCoordinates LatLongCoord);


	/** Converts UTM geographical coordinates to LatLong coordinates.**/
	static fAstroLatLongCoordinates UTM2LL(const Int_t ReferenceEllipsoid, const Double_t UTMNorthing, const Double_t UTMEasting, const std::string &UTMZone);

	/** Converts UTM geographical coordinates to LatLong coordinates.**/
	static fAstroLatLongCoordinates UTM2LL(const Int_t ReferenceEllipsoid, const fAstroUTMCoordinates UTMCoord);

	/** Converts UTC to JulianDay.**/
	static Double_t UTC2JulianDay(const Int_t day, Int_t month, Int_t year, const Int_t hour, const Int_t min,const Double_t sec);

	/** Converts UTC to Uinix time.**/
	static time_t UTC2UnixTime(const fAstroUTCCoordinates);

	/** Converts Uinix time to UTC.**/
	static fAstroUTCCoordinates UnixTime2UTC(const time_t UnixTime);

	 /** Converts equatorial astronomical coordinates to horizontal coordinate for all the objects in the Collection vector.**/
	static void Equatorial2Horizontal(std::vector<fAstroCatalogueObject> &Collection, const Int_t day,  Int_t month,  Int_t year,const Int_t hour, const Int_t min,const Double_t sec,const Double_t Longitude,const Double_t Latitude);

	/** Converts equatorial astronomical coordinates to horizontal coordinate for all the objects in the Collection vector.**/
	static void Equatorial2Horizontal(std::vector<fAstroCatalogueObject> &Collection,const fAstroUTCCoordinates UTCCoord,const fAstroLatLongCoordinates LatLongCoord);


	/** Converts equatorial astronomical coordinates to horizontal coordinate.**/
	static fAstroHorizontalCoordinates  Equatorial2Horizontal(const Double_t RA, const Double_t Decl,const Int_t day, Int_t month,  Int_t year,  const Int_t hour, const Int_t min,const Double_t sec,const Double_t Longitude,const Double_t Latitude);

	/** Converts equatorial astronomical coordinates to horizontal coordinate.**/
	static fAstroHorizontalCoordinates  Equatorial2Horizontal(const fAstroEquatorialCoordinates EquatorialCoord,const fAstroUTCCoordinates UTCCoord,const fAstroLatLongCoordinates LatLongCoord);

	/** Converts horizontal astronomical coordinates to equatorial coordinates.**/
	static fAstroEquatorialCoordinates  Horizontal2Equatorial(const Double_t Alt, const Double_t Az, const Int_t day,  Int_t month,  Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);

	/** Converts horizontal astronomical coordinates to equatorial coordinates.**/
	static fAstroEquatorialCoordinates Horizontal2Equatorial(const fAstroHorizontalCoordinates HorizontalCoord,const fAstroUTCCoordinates UTCCoord, fAstroLatLongCoordinates LatLongCoord);

	/** Converts equatorial astronomical coordinates to galactic coordinate for all the objects in the Collection vector.**/
	static void Equatorial2Galactic(std::vector<fAstroCatalogueObject> &Collection);

	/** Converts equatorial astronomical coordinates to galactic coordinate.**/
	static fAstroGalacticCoordinates  Equatorial2Galactic(const Double_t RA, const Double_t Decl);

	/** Converts equatorial astronomical coordinates contained in fAstroEquatorialCoordinates to galactic coordinates contained in fAstroGalacticCoordinates.**/
	static fAstroGalacticCoordinates  Equatorial2Galactic(const fAstroEquatorialCoordinates EquatorialCoord);

	/** Converts galactic astronomical coordinates to equatorial coordinates contained in fAstroEquatorialCoordinates.**/
	static fAstroEquatorialCoordinates  Galactic2Equatorial(const Double_t l, const Double_t b);

	/** Converts galactic astronomical coordinates contained in fAstroGalacticCoordinates to equatorial coordinates contained in fAstroEquatorialCoordinates .**/
	static fAstroEquatorialCoordinates Galactic2Equatorial(const fAstroGalacticCoordinates GalacticCoord);

	/** Converts ecliptical astronomical coordinates to equatorial coordinates contained in fAstroEclipticCoordinates.**/
	static fAstroEquatorialCoordinates Ecliptic2Equatorial(const Double_t JulianDay, const fAstroEclipticCoordinates EclipticCoord);

	/** Converts ecliptical astronomical coordinates to equatorial coordinates contained in fAstroEclipticCoordinates.**/
	static fAstroEquatorialCoordinates Ecliptic2Equatorial(const fAstroEclipticCoordinates EclipticCoord);

	/** Converts equatorial astronomical coordinates to ecliptical coordinate.**/
	static fAstroEclipticCoordinates Equatorial2Ecliptic(const Double_t JulianDay,const Double_t RA, const Double_t Decl);

	/** Converts equatorial astronomical coordinates to ecliptical coordinates contained in fAstroEquatorialCoordinates.**/
	static fAstroEclipticCoordinates Equatorial2Ecliptic(const Double_t JulianDay, const fAstroEquatorialCoordinates EquatorialCoord);

	/** Converts equatorial astronomical coordinates to ecliptical coordinate for all the objects in the Collection vector.**/
	static void Equatorial2Ecliptic(const Double_t JulianDay,std::vector<fAstroCatalogueObject> &Collection);

	/** Converts equatorial astronomical coordinates to ecliptical coordinate using the mean obliquity.**/
	static fAstroEclipticCoordinates Equatorial2Ecliptic(const Double_t RA, const Double_t Decl);

	/** Converts equatorial astronomical coordinates to ecliptical coordinates contained in fAstroEquatorialCoordinates using the mean obliquity.**/
	static fAstroEclipticCoordinates Equatorial2Ecliptic(const fAstroEquatorialCoordinates EquatorialCoord);

	/** Converts equatorial astronomical coordinates to ecliptical coordinate for all the objects in the Collection vector using the mean obliquity.**/
	static void Equatorial2Ecliptic(std::vector<fAstroCatalogueObject> &Collection);

	/** Converts equatorial astronomical coordinates to ecliptical coordinates contained in fAstroEquatorialCoordinates.**/
	static fAstroEclipticCoordinates Rectangular2Ecliptic(const fAstroRectangularCoordinates RectangularCoord);

	/** Converts equatorial rectangular heliocentric coordinates to rectangular geocentric coordinates contained in fAstroEquatorialCoordinates.**/
	static fAstroRectangularCoordinates Heliocentric2Rectangular(const fAstroHeliosCoordinates HeliosPosition, Double_t  JulianDay);


	ClassDef(TROAstConversions,1);

};

};

#endif


