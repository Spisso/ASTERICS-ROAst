#ifndef __TROAstElementsClasses__
#define __TROAstElementsClasses__

#include <string>
#include <map>
#include <iostream>
#include <TObject.h>
#include <algorithm>
#include <vector>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <math.h>
#include <iterator>
typedef std::runtime_error GeneralException;


namespace ROASt
{

/** Time UTC coordinates.**/
struct fAstroUTCCoordinates
{
	/** Gregorian day at the oseravation time.**/
	Int_t day;
	/** Gregorian month at the oseravation time.**/
	Int_t month;
	/** Gregorian year at the oseravation time.**/
	Int_t year;
	/** hour at the oseravation time.**/
	Int_t hour;
	/** Minute at the oseravation time.**/
	Int_t min;
	/** Second at the oseravation time.**/
	Double_t sec;
	/** Longitude in degree of the observer.**/

	/** Default constructor.**/
	fAstroUTCCoordinates(){};

	/**Constructor which set the coords. **/
	fAstroUTCCoordinates( Int_t _day,  Int_t _month, Int_t _year, Int_t _hour, Int_t _min, Double_t _sec)
	{day=_day; month=_month; year=_year; hour=_hour; min=_min; sec=_sec; }
};

/** UTM LatLong coordinates.**/
struct fAstroLatLongCoordinates
{
	/** Latitude coordinate.**/
	Double_t Latitude;
	/** Longitude coordinate.**/
	Double_t Longitude;

	/** Default constructor.**/
	fAstroLatLongCoordinates(){};
	/**Constructor which set the coords. **/
	fAstroLatLongCoordinates(Double_t _Latitude, Double_t _Longitude){Latitude = _Latitude; Longitude = _Longitude;}

};

/** UTM geographical coordinates.**/
struct fAstroUTMCoordinates
{
	/** UTMNorthing coordinate.**/
	Double_t UTMNorthing;
	/** UTMEasting coordinate.**/
	Double_t UTMEasting;
	/** UTMZone coordinate.**/
	std::string UTMZone;

	/** Default constructor.**/
	fAstroUTMCoordinates(){};

	/**Constructor which set the coords. **/
	fAstroUTMCoordinates(Double_t _UTMNorthing, Double_t _UTMEasting,std::string &_UTMZone){UTMNorthing = _UTMNorthing; UTMEasting = _UTMEasting; UTMZone = _UTMZone;}

};


/** Equatorial astronomical coordinates.**/
struct fAstroEquatorialCoordinates
{
	/** Right ascension acoordinate.**/
	Double_t RA;
	/** Declination coordinate.**/
	Double_t Decl;
	/** Standard deviation of the right ascension coordinate.**/
	Double_t RASigma;
	/** Standard deviation of the declination coordinate.**/
	Double_t DeclSigma;

	/** Default constructor.**/
	fAstroEquatorialCoordinates(){};

	/**Constructor which set the coords. **/
	fAstroEquatorialCoordinates(Double_t _ra, Double_t _decl){RA = _ra; Decl = _decl;}

};

/** Galactic astronomical coordinates.**/
struct fAstroGalacticCoordinates
{

	/** l coordinate.**/
	Double_t l;
	/** b coordinate.**/
	Double_t b;
	/** Standard deviation of the l coordinate.**/
	Double_t lSigma;
	/** Standard deviation of the b coordinate.**/
	Double_t bSigma;

	/** Default constructor.**/
	fAstroGalacticCoordinates(){};

	/**Constructor which set the coords. **/
	fAstroGalacticCoordinates(Double_t _l, Double_t _b){l = _l; b = _b;}

};

/** Horizontal astronomical coordinates **/
struct fAstroHorizontalCoordinates
{
	/** Altitude coordinate.**/
	Double_t Alt;
	/** Azimut coordinate.**/
	Double_t Az;
	/** Standard deviation of the Altitude coordinate.**/
	Double_t AltSigma;
	/** Standard deviation of the Azimut coordinate.**/
	Double_t AzSigma;

	/** Default constructor.**/
	fAstroHorizontalCoordinates(){};

	/**Constructor which set the coords. **/
	fAstroHorizontalCoordinates(Double_t alt, Double_t az) { Alt = alt;	Az = az;}

};


/** Ecliptic  Longitude and Latitude. Angles are expressed in degrees. East is positive, West negative. **/
struct fAstroEclipticCoordinates
{
	Double_t Latitude; /** latitude. Object latitude **/
	Double_t Longitude; /** longitude. Object longitude. **/

	/** Standard deviation of the l coordinate.**/
	Double_t LatitudeSigma;
	/** Standard deviation of the b coordinate.**/
	Double_t LongitudeSigma;

	/** Default constructor.**/
	fAstroEclipticCoordinates(){};

	/**Constructor which set the coords. **/
	fAstroEclipticCoordinates(Double_t latitude, Double_t longitude) { Latitude = latitude;	Longitude = longitude;}
};


/**  Rectangular coordinates. **/
struct fAstroRectangularCoordinates
{
	Double_t X;	/**  Rectangular X coordinate **/
	Double_t Y;	/**  Rectangular Y coordinate **/
	Double_t Z;	/**  Rectangular Z coordinate **/

	/** Default constructor.**/
	fAstroRectangularCoordinates(){};

	/**Constructor which set the coords. **/
	fAstroRectangularCoordinates(Double_t x, Double_t y, Double_t z) { X = x;Y = y; Z = z;}

};

/** Rectangular heliocentric coordinates. **/
struct fAstroHeliosCoordinates
{
	Double_t L;	/** Heliocentric longitude **/
	Double_t B;	/** Heliocentric latitude **/
	Double_t R;	/** Heliocentric radius vector **/

	/** Default constructor.**/
	fAstroHeliosCoordinates(){};

	/**Constructor which set the coords. **/
	fAstroHeliosCoordinates(Double_t l, Double_t b, Double_t r) { L = l;B = b; R = r;}


};



/** General astronomical object container.**/

struct fAstroCatalogueObject
{
	/** Equatorial astronomical coordinates.**/
	fAstroEquatorialCoordinates  EquatorialCoord;

	/** Galactic astronomical coordinates.**/
	fAstroGalacticCoordinates GalacticCoord;

	/** Horizontal astronomical coordinates. **/
	fAstroHorizontalCoordinates HorizontalCoord;

	/** Horizontal astronomical coordinates. **/
	fAstroEclipticCoordinates EclipticCoord;


	/** 1 : galactic astronomical coordinates are used. **/
	Int_t UsedGalactic = 0;

	/** 1 : horizontal astronomical coordinates are used. **/
	Int_t UsedHorizontal = 0;

	/** 1 : galactic astronomical coordinates are used. **/
	Int_t UsedEcliptic = 0;


	/** Map for the double features of the catalogue.**/
	std::map<std::string,Double_t> RealFeatures;

	/** Map for the int features of the catalogue.**/
	std::map<std::string,Int_t> IntFeatures;

	/** Map for the std::string features of the catalogue.**/
	std::map<std::string,std::string> StringFeatures;
};

/** Universal Transverse Mercator  Ellipsoid.**/
class fUTMEllipsoid
	{
	public:
		/** Default constructor.**/
		fUTMEllipsoid(){};

		/**Constructor which set the Ellipsoid data. **/
		fUTMEllipsoid(Int_t Id, std::string name, Double_t radius, Double_t ecc)
		{
			id = Id; ellipsoidName = name;
			EquatorialRadius = radius; eccentricitySquared = ecc;
		}

		/** Universal Transverse Mercator  Ellipsoid id.**/
		Int_t id;

		/** Universal Transverse Mercator  Ellipsoid name.**/
		std::string ellipsoidName;

		/** Universal Transverse Mercator  Ellipsoid radius.**/
		Double_t EquatorialRadius;

		/** Universal Transverse Mercator  Ellipsoid eccentricity squared.**/
		Double_t eccentricitySquared;

	};



/** Contains Nutation in longitude, obliquity and ecliptic obliquity.**/
struct fAstroNutationCoordinates
{
	Double_t Longitude;	/**  Nutation in longitude, in degrees **/
	Double_t Obliquity;	/** Nutation in obliquity, in degrees **/
	Double_t Ecliptic;	/** Mean obliquity of the ecliptic, in degrees **/

	/** Default constructor.**/
	fAstroNutationCoordinates(){};

	/**Constructor which set the coords. **/
	fAstroNutationCoordinates(Double_t latitude, Double_t obliquity, Double_t ecliptic) { Longitude = latitude; Obliquity = obliquity; Ecliptic = ecliptic;}
};

/** used for elp1 - 3 **/
	struct fMainProblem
	{
		Int_t iCoefficients[4];
		Double_t Amplitude;
		Double_t Derivatives[5];
	};

	/** used for elp 4 - 9 **/
	struct fEarthPerturbations
	{
		Int_t izCoefficient;
		Int_t iCoefficients[4];
		Double_t Phase;
		Double_t Amplitude;
		Double_t Period;
	};

	/** used for elp 10 - 21 **/
	struct fPlanetPerturbations
	{
		Int_t iPlanetaryCoefficients[11];
		Double_t Phase;
		Double_t Amplitude;
		Double_t Period;
	};

	struct vsop
	{
		Double_t A;
		Double_t B;
		Double_t C;
	};

}
#endif

