
#ifndef __TROAstGraphics__
#define __TROAstGraphics__


#include <TGraph.h>
#include <TGaxis.h>
#include <TGraphErrors.h>
#include <TMarker.h>
#include <TGraph2D.h>
#include <TStyle.h>
#include "TROAstCatalogue.hh"
#include <TMath.h>
#include <TH2.h>
#include <TCanvas.h>

namespace ROASt
{

/** Optional graphics class.**/
class TROAstGraphics: public TGraphErrors
{

public:

 	 Bool_t gAstroXAxisEnable = 1 ;

	/** TGraph2D  basic graphic pubblic instance.**/
        TGraph2D  *AstroGraph2D = new TGraph2D();
	

	/** Default constructor.**/
	TROAstGraphics() {std::cout << "This is the ROASt Graphics Class,  v 1.0" << std::endl;};

	/** Destructor.**/
	~TROAstGraphics(){ };

	/** Draws the astronomical objects positions of [Catalogue] using equatorial coordinate.**/
	void Draw(std::vector<fAstroCatalogueObject> *Catalogue ,const Option_t *option);

	/** Draws the astronomical objects positions of [Catalogue] using equatorial/galactic/horizontal coordinate.**/
	void Draw(std::vector<fAstroCatalogueObject> *Catalogue,const std::string &CoordinateType,const Option_t *option);

	void Draw(fAstroEquatorialCoordinates &Position,const Option_t *option);

	/** Draws the selected feature of the astronomical objects of [Catalogue] using equatorial coordinate.**/
	void DrawFeature(std::vector<fAstroCatalogueObject> *Catalogue,const std::string &FeatureName,const Option_t *option);

	/** Draws the selected feature of astronomical objects  of [Catalogue] using equatorial/galactic/horizontal coordinate.**/
	void DrawFeature(std::vector<fAstroCatalogueObject> *Catalogue,const std::string &CoordinateType,const std::string &FeatureName,const Option_t *option);

	void DrawAitoff(std::vector<fAstroCatalogueObject> *Catalogue,const Option_t *option);

 	void DrawAitoff(fAstroEquatorialCoordinates &Position,Double_t Range,const Option_t *option);

	void DrawSkyMap(std::vector<fAstroCatalogueObject> *Catalogue,const Option_t *option);

	void DrawSkyMap(fAstroEquatorialCoordinates &Position ,const Option_t *option);


	/** ROOT ClassDef directive for rootcint.**/
	ClassDef(TROAstGraphics,1);

};

}

#endif
