  
#ifndef __TROAstMoon__
#define __TROAstMoon__

#include "TROAstConversions.hh"


namespace ROASt
{

/** Lunar coordinate class **/
class TROAstMoon : public TObject
{
private:

/* precision */
	static Double_t Pre[3];

	static Double_t Cache_precision;

/* cache values */


        static Double_t Cache_JulianDay;
        static Double_t Cache_X;
        static Double_t Cache_Y ;
	static Double_t Cache_Z ;




	 static TROAstConversions *Conv;

	 static Double_t RangeRadians(Double_t Angle);

	 static Double_t SumSeriesElp1(Double_t* t);
	 static Double_t SumSeriesElp2(Double_t* t);
	 static Double_t SumSeriesElp3(Double_t* t);
	 static Double_t SumSeriesElp4(Double_t* t);
	 static Double_t SumSeriesElp5(Double_t* t);
	 static Double_t SumSeriesElp6(Double_t* t);
	 static Double_t SumSeriesElp7(Double_t* t);
	 static Double_t SumSeriesElp8(Double_t* t);
	 static Double_t SumSeriesElp9(Double_t* t);
	 static Double_t SumSeriesElp10(Double_t* t);
	 static Double_t SumSeriesElp11(Double_t* t);
	 static Double_t SumSeriesElp12(Double_t* t);
	 static Double_t SumSeriesElp13(Double_t* t);
	 static Double_t SumSeriesElp14(Double_t* t);
	 static Double_t SumSeriesElp15(Double_t* t);
	 static Double_t SumSeriesElp16(Double_t* t);
	 static Double_t SumSeriesElp17(Double_t* t);
	 static Double_t SumSeriesElp18(Double_t* t);
	 static Double_t SumSeriesElp19(Double_t* t);
	 static Double_t SumSeriesElp20(Double_t* t);
	 static Double_t SumSeriesElp21(Double_t* t);
	 static Double_t SumSeriesElp22(Double_t* t);
	 static Double_t SumSeriesElp23(Double_t* t);
	 static Double_t SumSeriesElp24(Double_t* t);
	 static Double_t SumSeriesElp25(Double_t* t);
	 static Double_t SumSeriesElp26(Double_t* t);
	 static Double_t SumSeriesElp27(Double_t* t);
	 static Double_t SumSeriesElp28(Double_t* t);
	 static Double_t SumSeriesElp29(Double_t* t);
	 static Double_t SumSeriesElp30(Double_t* t);
	 static Double_t SumSeriesElp31(Double_t* t);
	 static Double_t SumSeriesElp32(Double_t* t);
	 static Double_t SumSeriesElp33(Double_t* t);
	 static Double_t SumSeriesElp34(Double_t* t);
	 static Double_t SumSeriesElp35(Double_t* t);
	 static Double_t SumSeriesElp36(Double_t* t);

public:


	/** Default constructor.**/
	TROAstMoon() { std::cout << "This is the ROASt-TROAstMoon Class,  v 1.0" << std::endl;};

	/** Destructor.**/
	~TROAstMoon(){};

	 static fAstroRectangularCoordinates GetLunarRectGeoPosition(Double_t JulianDay, Double_t precision);

	 static fAstroEclipticCoordinates GetLunarEclipticCoords(Double_t JulianDay, Double_t precision);

	 static fAstroEquatorialCoordinates GetLunarEquatorialCoords(Double_t JulianDay,Double_t precision);

	 static fAstroEquatorialCoordinates GetLunarEquatorialCoords(Double_t JulianDay);

	 static Double_t GetLunarEarthDistance(Double_t JulianDay);

	ClassDef(TROAstMoon,1);

};

}
#endif
