  
#ifndef __TROAstSun__
#define __TROAstSun__

#include "TROAstConversions.hh"


namespace ROASt
{

/** Lunar coordinate class **/
class TROAstSun : public TObject
{
private:

	static Double_t SeriesResult(const vsop * data, Int_t terms, Double_t t);
        static void VSOP87ToFK5(fAstroHeliosCoordinates Position, Double_t JulianDay);

 	static TROAstConversions *Conv;

        static Double_t Cache_JulianDaySun; /* cache variables */
	static Double_t Cache_B;
	static Double_t Cache_R;
	static Double_t Cache_L;



public:

	/** Default constructor.**/
	TROAstSun() { std::cout << "This is the ROASt-TROAstSun Class,  v 1.0" << std::endl;};

	/** Destructor.**/
	~TROAstSun(){};

	static fAstroHeliosCoordinates GetEarthRectHeliosCoords(Double_t JulianDay);

	static fAstroHeliosCoordinates GetSolarRectHeliosCoords(Double_t JulianDay);

	static fAstroRectangularCoordinates GetSolarRectCoords(Double_t JulianDay);

	static fAstroEclipticCoordinates  GetSolarEclipticCoords(Double_t JulianDay);

	static fAstroEquatorialCoordinates  GetSolarEquatorialCoords(Double_t JulianDay);

	ClassDef(TROAstSun,1);

};

}
#endif
