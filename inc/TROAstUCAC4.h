#include "TROAstCatalogue.hh"

namespace ROASt
{

#define SizefUCAC4BaseObject 78


/**  UCAC4 catalogue class **/
class TROAstUCAC4 : public TROAstCatalogue
{
	/** Basic data block of the UCAC4 catalogue **/
	struct fUCAC4BaseObject
	{
		int32_t RA, Spd;                 /** RA/Spd at J2000.0,  ICRS,  in milliarcsec **/
		uint16_t Mag1,Mag2;              /** UCAC fit model & aperture mags, .001 mag **/
		uint8_t MagSigma;				/** Sigmas for the mags */
		uint8_t ObjType, DoubleStarFlag;/** Object type and double Object flag  **/
		int8_t RASigma, DeclSigma;       /** sigmas in RA and dec at central epoch **/
		uint8_t NUcacTotal;      		/** Number of UCAC observations of this Object **/
		uint8_t NUcacUsed;     			/** # UCAC observations _used_ for this Object **/
		uint8_t NCatsUsed;  			/** # catalogs (epochs) used for prop motion **/
		uint16_t EpochRa;  			    /** Central epoch for mean RA, minus 1900, .01y **/
		uint16_t EpochDec;       		/** Central epoch for mean DE, minus 1900, .01y **/
		int16_t PmRa;            		/** prop motion, .1 mas/yr = .01 arcsec/cy **/
		int16_t PmDec;           	   	/** prop motion, .1 mas/yr = .01 arcsec/cy **/
		int8_t PmRaSigma;      			/** sigma for RA in same units **/
		int8_t PmDecSigma;				/** sigma for DEC in same units **/
		uint32_t TwomassId;      	    /** 2MASS pts_key Object identifier **/
		uint16_t MagJ, MagH, MagK;  	/** 2MASS J, H, K_s mags,  in millimags **/
		uint8_t IcqFlag[3];
		uint8_t E2mpho[3];              /** 2MASS error photometry (in centimags) **/
		uint16_t ApassMag[5];           /** in millimags **/
		uint8_t ApassMagSigma[5];		/** also in millimags **/
		uint8_t YaleGcflags;      		/** Yale SPM g-flag * 10 + c-flag **/
		uint32_t CatalogFlags;
		uint8_t LedaFlag;               /** LEDA galaxy match flag **/
		uint8_t TwomassExtFlag;         /** 2MASS extended source flag **/
		uint32_t IdNumber;
		uint16_t UCAC2Zone;				/** Zone number in UCAC2 **/
		uint32_t UCAC2Number;			/** ID numbet in UCAC2 **/

		 /**Cast overloadinng for the UCAC4 catalogue basic data block **/
		operator fAstroCatalogueObject()
				{
			fAstroCatalogueObject Object;

			Object.EquatorialCoord.RA = (Double_t)this->RA / 3600000;
			Object.EquatorialCoord.Decl = ((Double_t)this->Spd / 3600000) - 90;
			Object.EquatorialCoord.RASigma = ((Double_t)this->RASigma  + 128)/360000;
			Object.EquatorialCoord.DeclSigma = ((Double_t)this->DeclSigma + 128)/360000;

			Object.IntFeatures.insert(std::make_pair("ObjType",(Int_t)this->ObjType));
			Object.IntFeatures.insert(std::make_pair("DoubleStarFlag",(Int_t)this->DoubleStarFlag));

			Object.RealFeatures.insert(std::make_pair("Mag1",(Double_t)this->Mag1 / 1000));
			Object.RealFeatures.insert(std::make_pair("Mag2",(Double_t)this->Mag2 / 1000));
			Object.RealFeatures.insert(std::make_pair("MagSigma",(Double_t)this->MagSigma));
			Object.RealFeatures.insert(std::make_pair("NUcacTotal",(Double_t)this->NUcacTotal));
			Object.RealFeatures.insert(std::make_pair("NCatsUsed",(Double_t)this->NCatsUsed));
			Object.RealFeatures.insert(std::make_pair("EpochRa",(Double_t)this->EpochRa));
			Object.RealFeatures.insert(std::make_pair("EpochDec",(Double_t)this->EpochDec));
			Object.RealFeatures.insert(std::make_pair("PmRa",(Double_t)this->PmRa));
			Object.RealFeatures.insert(std::make_pair("PmDec",(Double_t)this->PmDec));
			Object.RealFeatures.insert(std::make_pair("PmRaSigma",(Double_t)this->PmRaSigma));
			Object.RealFeatures.insert(std::make_pair("PmDecSigma",(Double_t)this->PmDecSigma));
			Object.RealFeatures.insert(std::make_pair("TwomassId",(Double_t)this->TwomassId));
			Object.RealFeatures.insert(std::make_pair("MagJ",(Double_t)this->MagJ));
			Object.RealFeatures.insert(std::make_pair("MagH",(Double_t)this->MagH));
			Object.RealFeatures.insert(std::make_pair("MagK",(Double_t)this->MagK));
			Object.RealFeatures.insert(std::make_pair("YaleGcflags",(Double_t)this->YaleGcflags));
			Object.RealFeatures.insert(std::make_pair("CatalogFlags",(Double_t)this->CatalogFlags));
			Object.RealFeatures.insert(std::make_pair("LedaFlag",(Double_t)this->LedaFlag));
			Object.RealFeatures.insert(std::make_pair("TwomassExtFlag",(Double_t)this->TwomassExtFlag));
			Object.RealFeatures.insert(std::make_pair("IdNumber",(Double_t)this->IdNumber));
			Object.RealFeatures.insert(std::make_pair("UCAC2Zone",(Double_t)this->UCAC2Zone));
			Object.RealFeatures.insert(std::make_pair("UCAC2Number",(Double_t)this->UCAC2Number));
			Object.RealFeatures.insert(std::make_pair("IcqFlag1",(Double_t)this->IcqFlag[0]));
			Object.RealFeatures.insert(std::make_pair("IcqFlag2",(Double_t)this->IcqFlag[1]));
			Object.RealFeatures.insert(std::make_pair("IcqFlag3",(Double_t)this->IcqFlag[2]));
			Object.RealFeatures.insert(std::make_pair("E2mpho1",(Double_t)this->E2mpho[0]));
			Object.RealFeatures.insert(std::make_pair("E2mpho2",(Double_t)this->E2mpho[1]));
			Object.RealFeatures.insert(std::make_pair("E2mpho3",(Double_t)this->E2mpho[2]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[0]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[1]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[2]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[0]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[3]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[4]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[0]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[1]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[2]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[0]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[3]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[4]));

			return Object;
				}
	};

	/** Basic object variable for the UCAC4 catalogue **/
	fUCAC4BaseObject BaseObject;

	/** Coefficients for the PM detrmination. **/
	const int16_t kLookupTable[5] = { 275, 325, 375, 450, 500 };

	/** Coefficient table copied from /u4i/u4hpm.dat. **/
	const int32_t kPmLookupTable[76] = {
			1,    41087,   31413,
			2,    41558,   32586,
			200137,   -37758,    7655,
			200168,   -36004,    9521,
			200169,   -36782,    4818,
			200229,    39624,  -25374,
			200400,    65051,  -57308,
			200503,    56347,  -23377,
			200530,    67682,   13275,
			200895,   -22401,  -34203,
			201050,    -7986,  103281,
			201349,    22819,   53694,
			201526,    -5803,  -47659,
			201550,    40033,  -58151,
			201567,    41683,   32691,
			201633,   -44099,    9416,
			201803,    34222,  -15989,
			249921,   -10015,  -35427,
			249984,    -9994,  -35419,
			268357,     5713,  -36943,
			80118783,    32962,    5639,
			93157181,   -22393,  -34199,
			106363470,   -37060,  -11490,
			110589580,   -38420,  -27250,
			113038183,    10990,  -51230, 0};

	 /**Extracts a single object.**/
	Int_t ExtractObjects(const Double_t RA, const Double_t Decl,const Double_t width, const Double_t height, const std::string &PathZone);

	/**Returns the index file offset.**/
	Long_t GetIndexFileOffset(const Int_t Zone, const Int_t RaObjectt) const;

	/**Returns the proper motion  of an object.**/
	int32_t GetActualProperMotion(const Int_t GetDecPm, const int16_t PmRa, const int16_t PmDec, const uint32_t IdNumber) const;

	/**Returns the standard deviation of the proper motion of an object.**/
	Int_t GetActualProperMotionSigma(const int8_t PmSigma) const ;

	/** Project the UCAC4 catalogue objects on a rectangular region, defined via only equatorial coordinates, populating the ObjectsCollection vector.**/
	void ExtractObject(const Int_t Zone,const long Offset, const std::string &PathZone);
    
	std::string UCAC4IndexFilePath;
	std::string UCAC4FilePath;
	
public:

	
	/** Basic object variable for the catalogue **/
	fAstroCatalogueObject Object;

	/** Default constructor.**/
	TROAstUCAC4() 
		{
		std::cout << "This is the ROASt-UCAC4 Catalogue Class,  v 1.0" << std::endl;
		UCAC4IndexFilePath = GetStringEnv("UCAC4INDEXFILEPATH");
		UCAC4FilePath = GetStringEnv("UCAC4FILEPATH");
		};

	/** Destructor.**/
	~TROAstUCAC4(){ };

	/** Project the UCAC4 catalogue objects on a rectangular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height,  const std::string &PathZone);
	
	/** Project the UCAC4 catalogue objects on a rectangular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height);
	
	/** Project the  UCAC4 catalogue objects on a rectangular region, defined via horizontal coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height, const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &PathZone);

	/** Project the  UCAC4 catalogue objects on a rectangular region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height,const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);
		
	/**Project the UCAC4 catalogue objects on a circular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius,  const std::string &PathZone);

	/**Project the UCAC4 catalogue objects on a circular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius);
	
	/**Project the UCAC4 catalogue objects on a circular region, defined via horizontal coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const  Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &PathZone);
	
	/**Project the UCAC4 catalogue objects on a circular region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const  Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);

	/** Project the UCAC4 catalogue objects on a elliptical region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis, const std::string &PathZone);

	/** Project the UCAC4 catalogue objects on a elliptical region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis);

	/** Project the UCAC4 catalogue objects on a elliptical region, defined via horizontal coordinates, populating the ObjectsCollection vector. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &PathZone);

	/** Project the UCAC4 catalogue objects on a elliptical region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);
	
	/** Returns true if the specified object is found in the ObjectsCollection vector, with the option [Filter]  all the non matching object are erased from the vector.**/
	Bool_t FindObject(const std::string &Options,const std::string &FeatureName, const Double_t MinFeature, const Double_t MaxFeature);

	/** Writes the contents of the ObjectsCollection vector to the [OutFileName] named file.**/
	void WriteObjects(const std::string &OutFileName);

	/** Prints to the stdandard output the list of the  UCAC4 catalogue features.**/
	void ListFeatures();

	/** Prints to the stdandard output the contents of the ObjectsCollection vector. **/
	void Print() ;

	/** Prints the content of j-th element of the ObjectsCollection vector to the stdandard output. **/
	void Print(const Int_t j);

	/** ROOT ClassDef directive for rootcint.**/
	//ClassDef(TROAstUCAC4,1);


};


};
