#include "TROAstCatalogue.hh"

namespace ROASt
{

#define SizefURAT1BaseObject 80


/**  URAT1 catalogue class **/
class TROAstURAT1 : public TROAstCatalogue
{
	/** Basic data block of the URAT1 catalogue **/
	struct fURAT1BaseObject
	{

		int32_t RA;   				/*   mean RA on ICRF at URAT mean obs.epoch  */
		int32_t Spd;   				/*   mean South Pole Distance = Dec + 90 deg */
		int16_t Sigs;		 		/*   position error per coord. from scatter  */
		int16_t Sigm;  			 	/*   position error per coord. from model    */
		int8_t  Nst;			 	/*   tot. number of sets the star is in      */
		int8_t  Nsu;  			 	/*   number of sets used for mean position   */
		int16_t Epoch;  			/*   mean URAT obs. epoch - 2000.0           */
		uint16_t MMag; 			 	/*   mean URAT model fit magnitude           */
		int16_t Sigp;  			 	/*   URAT photometry error                   */
		int8_t  Nsm;   			 	/*   number of sets used for URAT magnitude  */
		int8_t  Ref;   			 	/*   largest reference star flag             */
		int16_t Nit;   			 	/*   total number of images (observations)   */
		int16_t Niu;   			 	/*   number of images used for mean position */
		int8_t  Ngt;   			 	/*   total number of 1st order grating obs.  */
		int8_t  Ngu;   			 	/*   number of 1st order grating pairs used  */
		int16_t PmRA;  			 	/*   proper motion RA*cosDec (from 2MASS)    */
		int16_t PmDec; 			 	/*   proper motion Dec                       */
		int16_t PmSigma; 			/*   proper motion error per coordinate      */
		int8_t  Mfm;			  	/*   match flag URAT with 2MASS              */
		int8_t  Mfa; 			  	/*   match flag URAT with APASS                   */
		int32_t Id2; 			  	/*   unique 2MASS star identification number      */
		uint16_t TwomassMag[3];   	/*   2MASS J mag, 2MASS H mag, 2MASS K mag   				   */
		int16_t TwomassMagSigma[3]; /*   error 2MASS J mag, error 2MASS H mag, error 2MASS K mag   */
		int8_t IccFlag[3];          /*   ICC flag 2MASS J, ICC flag 2MASS H, ICC flag 2MASS K      */
		int8_t PhotoFlag[3];		/*   photometry quality flag 2MASS J, photometry quality flag 2MASS H, photometry quality flag 2MASS K  */
	    uint16_t ApassMag[5];      	/*   APASS B mag,APASS V mag, APASS g mag, APASS r mag, APASS i mag  */
		int16_t ApassMagSigma[5]; 	/*   error APASS B mag, error APASS V mag, error APASS g mag, error APASS r mag, error APASS i mag  */
		int8_t  Ann;   			    /*   APASS numb. of nights                   */
		int8_t  Ano;   				/*	 APASS numb. of observ.                  */

		 /**Cast overloadinng for the URAT1 catalogue basic data block **/
		operator fAstroCatalogueObject()
				{
			fAstroCatalogueObject Object;

			Object.EquatorialCoord.RA = (Double_t)this->RA / 3600000;
			Object.EquatorialCoord.Decl = ((Double_t)this->Spd / 3600000) - 90;
			Object.EquatorialCoord.RASigma = ((Double_t)this->Sigs  )/360000;
			Object.EquatorialCoord.DeclSigma = ((Double_t)this->Sigs )/360000;

			Object.IntFeatures.insert(std::make_pair("TotNrOfSetsForTheStar",(Int_t)this->Nst));
			Object.IntFeatures.insert(std::make_pair("NrOfSetsForMeanPos",(Int_t)this->Nsu));
			Object.IntFeatures.insert(std::make_pair("Epoch",(Int_t)this->Epoch));
			Object.IntFeatures.insert(std::make_pair("NrOfSetsForMag",(Int_t)this->Nsm));
			Object.IntFeatures.insert(std::make_pair("LargeStarReference",(Int_t)this->Ref));
			Object.IntFeatures.insert(std::make_pair("TotNrOfImages",(Int_t)this->Nit));
			Object.IntFeatures.insert(std::make_pair("NrOfImagesForMeanPos",(Int_t)this->Niu));
			Object.IntFeatures.insert(std::make_pair("TotNrOf1stOrderGratingObs",(Int_t)this->Ngt));
			Object.IntFeatures.insert(std::make_pair("NrOf1stOrderGratingPairs",(Int_t)this->Ngu));
			Object.IntFeatures.insert(std::make_pair("URAT2MASSs",(Int_t)this->Mfm));
			Object.IntFeatures.insert(std::make_pair("URATAPASS",(Int_t)this->Mfa));
			Object.IntFeatures.insert(std::make_pair("TwomassId",(Int_t)this->Id2));
			Object.IntFeatures.insert(std::make_pair("PhotoFlagTwomassJ",(Int_t)this->PhotoFlag[0]));
			Object.IntFeatures.insert(std::make_pair("PhotoFlagTwomassH",(Int_t)this->PhotoFlag[1]));
			Object.IntFeatures.insert(std::make_pair("PhotoFlagTwomassK",(Int_t)this->PhotoFlag[2]));
			Object.IntFeatures.insert(std::make_pair("APASSNrOfNights",(Int_t)this->Ann));
			Object.IntFeatures.insert(std::make_pair("APASSNrOfObs",(Int_t)this->Ano));

			Object.RealFeatures.insert(std::make_pair("Sigm",(Double_t)this->Sigm/360000));
			Object.RealFeatures.insert(std::make_pair("PmRa",(Double_t)this->PmRA));
			Object.RealFeatures.insert(std::make_pair("PmDec",(Double_t)this->PmDec));
			Object.RealFeatures.insert(std::make_pair("PmSigma",(Double_t)this->PmSigma));
			Object.RealFeatures.insert(std::make_pair("MeanMag",(Double_t)this->MMag));
			Object.RealFeatures.insert(std::make_pair("MeanMagSigma",(Double_t)this->Sigp));
			Object.RealFeatures.insert(std::make_pair("TwomassMagJ",(Double_t)this->TwomassMag[0]));
			Object.RealFeatures.insert(std::make_pair("TwomassMagH",(Double_t)this->TwomassMag[1]));
			Object.RealFeatures.insert(std::make_pair("TwomassMagK",(Double_t)this->TwomassMag[2]));
			Object.RealFeatures.insert(std::make_pair("TwomassMagSigmaJ",(Double_t)this->TwomassMagSigma[0]));
			Object.RealFeatures.insert(std::make_pair("TwomassMagSigmaH",(Double_t)this->TwomassMagSigma[1]));
			Object.RealFeatures.insert(std::make_pair("TwomassMagSigmaK",(Double_t)this->TwomassMagSigma[2]));
			Object.RealFeatures.insert(std::make_pair("IccFlag1",(Double_t)this->IccFlag[0]));
			Object.RealFeatures.insert(std::make_pair("IccFlag2",(Double_t)this->IccFlag[1]));
			Object.RealFeatures.insert(std::make_pair("IccFlag3",(Double_t)this->IccFlag[2]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[0]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[1]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[2]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[0]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[3]));
			Object.RealFeatures.insert(std::make_pair("ApassMag",(Double_t)this->ApassMag[4]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[0]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[1]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[2]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[0]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[3]));
			Object.RealFeatures.insert(std::make_pair("ApassMagSigma",(Double_t)this->ApassMagSigma[4]));

			return Object;
				}
	};

	/** Basic object variable for the URAT1 catalogue **/
	fURAT1BaseObject BaseObject ;



	 /**Extracts a single object.**/
	Int_t ExtractObjects(const Double_t RA, const Double_t Decl,const Double_t width, const Double_t height, const std::string &PathZone);

	/**Returns the index file offset.**/
	Long_t GetIndexFileOffset(const Int_t Zone, const Int_t RaObjectt) const;

	/** Project the URAT1 catalogue objects on a rectangular region, defined via only equatorial coordinates, populating the ObjectsCollection vector.**/
	void ExtractObject(const Int_t Zone,const long Offset, const std::string &PathZone);

	std::string URAT1IndexFilePath ;
	std::string URAT1FilePath ;
	
	
public:



	/** Basic object variable for the catalogue **/
	fAstroCatalogueObject Object;

	/** Default constructor.**/
	TROAstURAT1() 
		{ 
		std::cout << "This is the ROASt-URAT1 Catalogue Class,  v 1.0" << std::endl;
		URAT1IndexFilePath = GetStringEnv("URAT1INDEXFILEPATH");
		URAT1FilePath = GetStringEnv("URAT1FILEPATH");
		};

	/** Destructor.**/
	~TROAstURAT1(){ };

	/** Project the URAT1 catalogue objects on a rectangular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height, const std::string &PathZone);

	/** Project the URAT1 catalogue objects on a rectangular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height);

	/** Project the  URAT1 catalogue objects on a rectangular region, defined via horizontal coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &PathZone);

	/** Project the  URAT1 catalogue objects on a rectangular region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);
	
	/**Project the URAT1 catalogue objects on a circular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius, const std::string &PathZone);

	/**Project the URAT1 catalogue objects on a circular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius);
	
	/**Project the URAT1 catalogue objects on a circular region, defined via horizontal coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const  Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &PathZone);

	/**Project the URAT1 catalogue objects on a circular region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const  Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);
	
	/** Project the URAT1 catalogue objects on a elliptical region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis, const std::string &PathZone);

	/** Project the URAT1 catalogue objects on a elliptical region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis);
	
	/** Project the URAT1 catalogue objects on a elliptical region, defined via horizontal coordinates, populating the ObjectsCollection vector. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &PathZone);

	/** Project the URAT1 catalogue objects on a elliptical region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);
	
	/** Returns true if the specified object is found in the ObjectsCollection vector, with the option [Filter]  all the non matching object are erased from the vector.**/
	Bool_t FindObject(const std::string &Options,const std::string &FeatureName, const Double_t MinFeature, const Double_t MaxFeature);

	/** Writes the contents of the ObjectsCollection vector to the [OutFileName] named file.**/
	void WriteObjects(const std::string &OutFileName);

	/** Prints to the stdandard output the list of the  URAT1 catalogue features.**/
	void ListFeatures();

	/** Prints to the stdandard output the contents of the ObjectsCollection vector. **/
	void Print() ;

	/** Prints the content of j-th element of the ObjectsCollection vector to the stdandard output. **/
	void Print(const Int_t j);

	/** ROOT ClassDef directive for rootcint.**/
	//ClassDef(TROAstURAT1,1);


};


};

