
#include "TROAstCatalogue.hh"

#include <iterator>
namespace ROASt
{



class TROAstVizR : public TROAstCatalogue
{
    /**Extracts a single object.**/
   Int_t ExtractObjects(const Double_t RA, const Double_t Decl,const Double_t width, const Double_t height, const std::string &Source);;

   Int_t CallConeService(const std::string &Source, Double_t RA, Double_t Decl, Double_t Radius);
   std::string VizQueryPath;
   std::string VizieRSite;
   std::vector<std::string> Descripions;
public:

	/** Basic object variable for the catalogue **/
	fAstroCatalogueObject Object;

	/** Default constructor.**/
	TROAstVizR() { 
    std::cout << "This is the ROASt-VizR  Catalogues Class,  v 1.0" << std::endl;
    std::string VizQueryPath = GetStringEnv("VIZQUERYPATH");
    std::string VizieRSite = GetStringEnv("VIZIERSITE");
	};

	/** Destructor.**/
	~TROAstVizR(){ };

	/** Project the VizR catalogue objects on a rectangular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height, const std::string &Source);

	/** Project the VizR catalogue objects on a rectangular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height);

	/** Project the  VizR catalogue objects on a rectangular region, defined via horizontal coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &Source);

	/** Project the  VizR catalogue objects on a rectangular region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);
	
	/**Project the VizR catalogue objects on a circular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius, const std::string &Source);
	
	/**Project the VizR catalogue objects on a circular region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius);
	
	/**Project the VizR catalogue objects on a circular region, defined via horizontal coordinates, populating the ObjectsCollection vector.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const  Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &Source);

	/**Project the VizR catalogue objects on a circular region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path.**/
	std::vector<fAstroCatalogueObject>* ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const  Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);
	
	/** Project the VizR catalogue objects on a elliptical region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis, const std::string &Source);

	/** Project the VizR catalogue objects on a elliptical region, defined via galctic or equatorial coordinates, populating the ObjectsCollection vector using the default path. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis);
	
	/** Project the VizR catalogue objects on a elliptical region, defined via horizontal coordinates, populating the ObjectsCollection vector. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &Source);

	/** Project the VizR catalogue objects on a elliptical region, defined via horizontal coordinates, populating the ObjectsCollection vector using the default path. **/
	std::vector<fAstroCatalogueObject>* ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year,  const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude);

	
	/** Returns true if the specified object is found in the ObjectsCollection vector, with the option [Filter]  all the non matching object are erased from the vector.**/
	Bool_t FindObject(const std::string &Options,const std::string &FeatureName, const Double_t MinFeature, const Double_t MaxFeature);

	/** Writes the contents of the ObjectsCollection vector to the [OutFileName] named file.**/
	void WriteObjects(const std::string &OutFileName);

	/** Prints to the stdandard output the list of the  VizR catalogue features.**/
	void ListFeatures();

	/** Prints to the stdandard output the contents of the ObjectsCollection vector. **/
	void Print() ;

	/** Prints the content of j-th element of the ObjectsCollection vector to the stdandard output. **/
	void Print(const Int_t j);

	/** ROOT ClassDef directive for rootcint.**/
	//ClassDef(TROAstVizR,1);


};


};



