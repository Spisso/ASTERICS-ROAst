
# =============================== 
# Makefile for ROAST
# =============================== 

.SUFFIXES:
.SUFFIXES: .cpp .o .cc 

ROOTGLIBS     = $(shell $(ROOTSYS)/bin/root-config --glibs)
ROOTCFLAGS    = $(shell $(ROOTSYS)/bin/root-config --cflags) 
PWD	          = $(shell pwd)
OBJDIR		  = $(PWD)/obj
SRC 		  = $(PWD)/src
INC		  = $(PWD)/inc
DIC		  = $(PWD)/dic
vpath %.cpp $(SRC)

CXX           = g++
CXXFLAGS      =  -g -Wall -fPIC -O3 -I $(INC)
SOFLAGS       = -lcurl -lexpat -shared   

CXXFLAGS     += $(ROOTCFLAGS) 

ROAST-OBJDIRECTS = $(addprefix $(OBJDIR)/, TROAstUCAC4.o TROAstURAT1.o TROAstVO.o TROAstVizR.o TROAstVOtAttr.o TROAstVOtElement.o TROAstVOtExpatCB.o TROAstDict.o \
	 TROAstVOtHandle.o TROAstVOtParse.o  TROAstVOtStack.o TROAstConversions.o  TROAstCatalogue.o TROAstMoon.o TROAstSun.o)

ROAST-INCDIRECTS = $(addprefix $(INC)/, TROAstVOtParseP.h TROAstVOtParse.h TROAstCatalogue.hh TROAstConversions.hh TROAstMoon.hh TROAstUCAC4.h \
	 TROAstURAT1.h TROAstElementsClasses.hh TROAstVO.h TROAstVizR.h TROAstSun.hh)

# =============================== 


libTROAst.so:  $(ROAST-OBJDIRECTS)  
	$(CXX)  $(ROAST-OBJDIRECTS) -o libTROAst.so $(ROOTGLIBS) $(SOFLAGS)

libTROAstGraphics.so:   $(OBJDIR)/TROAstGraphics.o  $(OBJDIR)/TROAstDictGraphics.o
	$(CXX) $(OBJDIR)/TROAstGraphics.o  $(OBJDIR)/TROAstDictGraphics.o -o libTROAstGraphics.so $(ROOTGLIBS) $(SOFLAGS)

$(OBJDIR)/%.o: %.cpp   $(ROAST-INCDIRECTS)
	mkdir -p $(@D)
	$(CXX) -c -o $@ $<  $(CXXFLAGS) 

$(OBJDIR)/TROAstGraphics.o: $(SRC)/TROAstGraphics.cpp
	mkdir -p $(@D)
	$(CXX) -c $(SRC)/TROAstGraphics.cpp -o $(OBJDIR)/TROAstGraphics.o $(CXXFLAGS) 

$(OBJDIR)/TROAstDict.o: $(DIC)/TROAstDict.cc 
	$(CXX) -c $(DIC)/TROAstDict.cc -o $(OBJDIR)/TROAstDict.o 	$(CXXFLAGS) 

$(OBJDIR)/TROAstDictGraphics.o: $(DIC)/TROAstDictGraphics.cc 
	$(CXX) -c $(DIC)/TROAstDictGraphics.cc -o $(OBJDIR)/TROAstDictGraphics.o $(CXXFLAGS)

$(DIC)/TROAstDict.cc:  $(ROAST-INCDIRECTS) $(INC)/LinkDef.h
	mkdir -p $(@D)
	$(ROOTSYS)/bin/rootcint  -f $(DIC)/TROAstDict.cc -c  $(ROAST-INCDIRECTS) $(INC)/LinkDef.h
	mv $(DIC)/*.pcm .
	
$(DIC)/TROAstDictGraphics.cc: $(INC)/TROAstGraphics.hh $(INC)/LinkDefGraph.h
	mkdir -p $(@D)
	$(ROOTSYS)/bin/rootcint -f $(DIC)/TROAstDictGraphics.cc -c $(INC)/TROAstGraphics.hh $(INC)/LinkDefGraph.h
	mv $(DIC)/*.pcm .

.PHONY : clean

clean: 
	rm -rf *.pcm $(OBJDIR)/ $(DIC)/ *.so
# =============================== 
