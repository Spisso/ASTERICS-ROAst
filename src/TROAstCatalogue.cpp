#include "TROAstUCAC4.h"
#include "TROAstURAT1.h"
#include "TROAstVO.h"
#include "TROAstVizR.h"

ClassImp(ROASt::TROAstCatalogue);


namespace ROASt
{

TROAstCatalogue *TROAstCatalogue::UCAC4Catalogue()
{
       //TROAstUCAC4 *UCAC4 = new TROAstUCAC4(); 
       // TROAstUCAC4 &UCAC4R = *UCAC4;
       //return UCAC4R;
       return new TROAstUCAC4();
}


TROAstCatalogue *TROAstCatalogue::URAT1Catalogue()
{
        //TROAstURAT1 *URAT1 = new TROAstURAT1(); 
        //TROAstURAT1 &URAT1R = *URAT1;
        //return URAT1R;
	return new TROAstURAT1();
}

TROAstCatalogue *TROAstCatalogue::VOCatalogue()
{
	// TROAstVO *VO = new TROAstVO(); 
        //TROAstVO &VOR = *VO;
        //return VOR;
	return new TROAstVO();
}


TROAstCatalogue *TROAstCatalogue::VizRCatalogue()
{
        // TROAstVizR *VizR = new TROAstVizR(); 
        // TROAstVizR &VizRR = *VizR;
        // return VizRR;
	return new TROAstVizR();
}

}
