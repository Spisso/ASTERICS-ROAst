#include "TROAstConversions.hh"

ClassImp(ROASt::TROAstConversions);

namespace ROASt
{

struct fNutationArguments
{
	Double_t D;
	Double_t M;
	Double_t MM;
	Double_t F;
	Double_t O;
};

struct fNutationCoefficients
{
	Double_t Longitude1;
	Double_t Longitude2;
	Double_t Obliquity1;
	Double_t Obliquity2;
};




/* Dynamical time in seconds for every second year from 1620 to 1992 */
const static Double_t delta_t[192] =
{   124.0, 115.0, 106.0, 98.0, 91.0,
		85.0, 79.0, 74.0, 70.0, 65.0,
		62.0, 58.0, 55.0, 53.0, 50.0,
		48.0, 46.0, 44.0, 42.0, 40.0,
		37.0, 35.0, 33.0, 31.0, 28.0,
		26.0, 24.0, 22.0, 20.0, 18.0,
		16.0, 14.0, 13.0, 12.0, 11.0,
		10.0, 9.0, 9.0, 9.0, 9.0,
		9.0, 9.0, 9.0, 9.0, 10.0,
		10.0, 10.0, 10.0, 10.0, 11.0,
		11.0, 11.0, 11.0, 11.0, 11.0,
		11.0, 12.0, 12.0, 12.0, 12.0,
		12.0, 12.0, 13.0, 13.0, 13.0,
		13.0, 14.0, 14.0, 14.0, 15.0,
		15.0, 15.0, 15.0, 16.0, 16.0,
		16.0, 16.0, 16.0, 17.0, 17.0,
		17.0, 17.0, 17.0, 17.0, 17.0,
		17.0, 16.0, 16.0, 15.0, 14.0,
		13.7, 13.1, 12.7, 12.5, 12.5,
		12.5, 12.5, 12.5, 12.5, 12.3,
		12.0, 11.4, 10.6, 9.6, 8.6,
		7.5, 6.6, 6.0, 5.7, 5.6,
		5.7, 5.9, 6.2, 6.5, 6.8,
		7.1, 7.3, 7.5, 7.7, 7.8,
		7.9, 7.5, 6.4, 5.4, 2.9,
		1.6, -1.0, -2.7, -3.6, -4.7,
		-5.4, -5.2, -5.5, -5.6, -5.8,
		-5.9, -6.2, -6.4, -6.1, -4.7,
		-2.7, 0.0, 2.6, 5.4, 7.7,
		10.5, 13.4, 16.0, 18.2, 20.2,
		21.2, 22.4, 23.5, 23.9, 24.3,
		24.0, 23.9, 23.9, 23.7, 24.0,
		24.3, 25.3, 26.2, 27.3, 28.2,
		29.1, 30.0, 30.7, 31.4, 32.2,
		33.1, 34.0, 35.0, 36.5, 38.3,
		40.2, 42.2, 44.5, 46.5, 48.5,
		50.5, 52.2, 53.8, 54.9, 55.8,
		56.9, 58.3
};


/* Stephenson and Houlden  for years prior to 948 A.D.*/
Double_t TROAstConversions::GetDynamicalDiffSH1(Double_t JulianDay)
{
	double DynamicalTime,E;

	/* number of centuries from 948 */
	E = (JulianDay - 2067314.5) / 36525.0;

	DynamicalTime = 1830.0 - 405.0 * E + 46.5 * E * E;
	return (DynamicalTime);
}

/* Stephenson and Houlden for years between 948 A.D. and 1600 A.D.*/
Double_t TROAstConversions::GetDynamicalDiffSH2(Double_t JulianDay)
{
	Double_t DynamicalTime,t;

	/* number of centuries from 1850 */
	t = (JulianDay - 2396758.5) / 36525.0;

	DynamicalTime = 22.5 * t * t;
	return DynamicalTime;
}


Double_t TROAstConversions::GetDynamicalDiffTable(Double_t JulianDay)
{
	Double_t DynamicalTime = 0;
	Double_t a,b,c,n;
	Int_t i;

	/* get no days since 1620 and divide by 2 years */
	i = (Int_t)((JulianDay - 2312752.5) / 730.5);

	/* get the base interpolation factor in the table */
	if (i > (192 - 2))
		i = 192 - 2;

	/* calc a,b,c,n */
	a = delta_t[i+1] - delta_t[i];
	b = delta_t[i+2] - delta_t[i+1];
	c = a - b;
	n = ((JulianDay - (2312752.5 + (730.5 * i))) / 730.5);

	DynamicalTime = delta_t[i+1] + n / 2 * (a + b + n * c);

	return DynamicalTime;
}

/* get the dynamical time diff in the near past / future 1992 .. 2010 */
Double_t TROAstConversions::GetDynamicalDiffNear(Double_t JulianDay)
{
	Double_t DynamicalTime = 0;
	/* DynamicalTime for 1990, 2000, 2010 */
	Double_t delta_T[3] = {56.86, 63.83, 70.0};
	Double_t a,b,c,n;

	/* calculate DynamicalTime by interpolating value */
	a = delta_T[1] - delta_T[0];
	b = delta_T[2] - delta_T[1];
	c = b - a;

	/* get number of days since 2000 and divide by 10 years */
	n = (JulianDay - 2451544.5) / 3652.5;
	DynamicalTime = delta_T[1] + (n / 2) * (a + b + n * c);

	return DynamicalTime;
}

/* Calc JDE for othe JD values */
Double_t TROAstConversions::GetDynamicalDiffOther(Double_t JulianDay)
{
	Double_t DynamicalTime;
	Double_t a;

	a = (JulianDay - 2382148);
	a *= a;

	DynamicalTime = -15 + a / 41048480;

	return (DynamicalTime);
}

/** Calculates the DynamicalTime (TD) difference in seconds (delta T) from universal time. **/
Double_t TROAstConversions::GetDynamicalTimeDiff(Double_t JulianDay)
{
	Double_t DynamicalTime;

	/* check when JulianDay is, and use corresponding formula */
	/* check for date < 948 A.D. */
	if ( JulianDay < 2067314.5 )
		/* Stephenson and Houlden */
		DynamicalTime = GetDynamicalDiffSH1(JulianDay);

	else if ( JulianDay >= 2067314.5 && JulianDay < 2305447.5 )
		/* check for date 948..1600 A.D. Stephenson and Houlden */
		DynamicalTime = GetDynamicalDiffSH2(JulianDay);

	else if ( JulianDay >= 2312752.5 && JulianDay < 2448622.5 )
		/* check for value in table 1620..1992  interpolation of table */
		DynamicalTime =GetDynamicalDiffTable(JulianDay);

	else if ( JulianDay >= 2448622.5 && JulianDay <= 2455197.5 )
		/* check for near future 1992..2010 interpolation */
		DynamicalTime = GetDynamicalDiffNear(JulianDay);

	else
		/* other time period outside */
		DynamicalTime = GetDynamicalDiffOther(JulianDay);

	return DynamicalTime;
}


/** Calculates the JulianEphemerisDay(JDE) from the given julian day **/

Double_t TROAstConversions::JDtoJDE(Double_t JulianDay)
{
	Double_t JulianEphemerisDay;
	Double_t secs_in_day = 24 * 60 * 60;

	JulianEphemerisDay = JulianDay + GetDynamicalTimeDiff(JulianDay) / secs_in_day;

	return JulianEphemerisDay;
}


/**  UTM Ellipsoid structure, id, fUTMEllipsoid name, Equatorial Radius, square of eccentricity **/
const fUTMEllipsoid utmellipsoid[24] =
{
		fUTMEllipsoid(-1, "Placeholder", 0, 0), //placeholder only, To allow array indices to match id numbers
		fUTMEllipsoid( 1, "Airy", 6377563, 0.00667054),
		fUTMEllipsoid( 2, "Australian National", 6378160, 0.006694542),
		fUTMEllipsoid( 3, "Bessel 1841", 6377397, 0.006674372),
		fUTMEllipsoid( 4, "Bessel 1841 (Nambia) ", 6377484, 0.006674372),
		fUTMEllipsoid( 5, "Clarke 1866", 6378206, 0.006768658),
		fUTMEllipsoid( 6, "Clarke 1880", 6378249, 0.006803511),
		fUTMEllipsoid( 7, "Everest", 6377276, 0.006637847),
		fUTMEllipsoid( 8, "Fischer 1960 (Mercury) ", 6378166, 0.006693422),
		fUTMEllipsoid( 9, "Fischer 1968", 6378150, 0.006693422),
		fUTMEllipsoid( 10, "GRS 1967", 6378160, 0.006694605),
		fUTMEllipsoid( 11, "GRS 1980", 6378137, 0.00669438),
		fUTMEllipsoid( 12, "Helmert 1906", 6378200, 0.006693422),
		fUTMEllipsoid( 13, "Hough", 6378270, 0.00672267),
		fUTMEllipsoid( 14, "International", 6378388, 0.00672267),
		fUTMEllipsoid( 15, "Krassovsky", 6378245, 0.006693422),
		fUTMEllipsoid( 16, "Modified Airy", 6377340, 0.00667054),
		fUTMEllipsoid( 17, "Modified Everest", 6377304, 0.006637847),
		fUTMEllipsoid( 18, "Modified Fischer 1960", 6378155, 0.006693422),
		fUTMEllipsoid( 19, "South American 1969", 6378160, 0.006694542),
		fUTMEllipsoid( 20, "WGS 60", 6378165, 0.006693422),
		fUTMEllipsoid( 21, "WGS 66", 6378145, 0.006694542),
		fUTMEllipsoid( 22, "WGS-72", 6378135, 0.006694318),
		fUTMEllipsoid( 23, "WGS-84", 6378137, 0.00669438)
};




/** arguments and coefficients  **/

const fNutationArguments NuArguments[63] = {
		{0.0,	0.0,	0.0,	0.0,	1.0},
		{-2.0,	0.0,	0.0,	2.0,	2.0},
		{0.0,	0.0,	0.0,	2.0,	2.0},
		{0.0,	0.0,	0.0,	0.0,	2.0},
		{0.0,	1.0,	0.0,	0.0,	0.0},
		{0.0,	0.0,	1.0,	0.0,	0.0},
		{-2.0,	1.0,	0.0,	2.0,	2.0},
		{0.0,	0.0,	0.0,	2.0,	1.0},
		{0.0,	0.0,	1.0,	2.0,	2.0},
		{-2.0,	-1.0,	0.0,	2.0,	2.0},
		{-2.0,	0.0,	1.0,	0.0,	0.0},
		{-2.0,	0.0,	0.0,	2.0,	1.0},
		{0.0,	0.0,	-1.0,	2.0,	2.0},
		{2.0,	0.0,	0.0,	0.0,	0.0},
		{0.0,	0.0,	1.0,	0.0,	1.0},
		{2.0,	0.0,	-1.0,	2.0,	2.0},
		{0.0,	0.0,	-1.0,	0.0,	1.0},
		{0.0,	0.0,	1.0,	2.0,	1.0},
		{-2.0,	0.0,	2.0,	0.0,	0.0},
		{0.0,	0.0,	-2.0,	2.0,	1.0},
		{2.0,	0.0,	0.0,	2.0,	2.0},
		{0.0,	0.0,	2.0,	2.0,	2.0},
		{0.0,	0.0,	2.0,	0.0,	0.0},
		{-2.0,	0.0,	1.0,	2.0,	2.0},
		{0.0,	0.0,	0.0,	2.0,	0.0},
		{-2.0,	0.0,	0.0,	2.0,	0.0},
		{0.0,	0.0,	-1.0,	2.0,	1.0},
		{0.0,	2.0,	0.0,	0.0,	0.0},
		{2.0,	0.0,	-1.0,	0.0,	1.0},
		{-2.0,	2.0,	0.0,	2.0,	2.0},
		{0.0,	1.0,	0.0,	0.0,	1.0},
		{-2.0,	0.0,	1.0,	0.0,	1.0},
		{0.0,	-1.0,	0.0,	0.0,	1.0},
		{0.0,	0.0,	2.0,	-2.0,	0.0},
		{2.0,	0.0,	-1.0,	2.0,	1.0},
		{2.0,	0.0,	1.0,	2.0,	2.0},
		{0.0,	1.0,	0.0,	2.0,	2.0},
		{-2.0,	1.0,	1.0,	0.0,	0.0},
		{0.0,	-1.0,	0.0,	2.0,	2.0},
		{2.0,	0.0,	0.0,	2.0,	1.0},
		{2.0,	0.0,	1.0,	0.0,	0.0},
		{-2.0,	0.0,	2.0,	2.0,	2.0},
		{-2.0,	0.0,	1.0,	2.0,	1.0},
		{2.0,	0.0,	-2.0,	0.0,	1.0},
		{2.0,	0.0,	0.0,	0.0,	1.0},
		{0.0,	-1.0,	1.0,	0.0,	0.0},
		{-2.0,	-1.0,	0.0,	2.0,	1.0},
		{-2.0,	0.0,	0.0,	0.0,	1.0},
		{0.0,	0.0,	2.0,	2.0,	1.0},
		{-2.0,	0.0,	2.0,	0.0,	1.0},
		{-2.0,	1.0,	0.0,	2.0,	1.0},
		{0.0,	0.0,	1.0,	-2.0,	0.0},
		{-1.0,	0.0,	1.0,	0.0,	0.0},
		{-2.0,	1.0,	0.0,	0.0,	0.0},
		{1.0,	0.0,	0.0,	0.0,	0.0},
		{0.0,	0.0,	1.0,	2.0,	0.0},
		{0.0,	0.0,	-2.0,	2.0,	2.0},
		{-1.0,	-1.0,	1.0,	0.0,	0.0},
		{0.0,	1.0,	1.0,	0.0,	0.0},
		{0.0,	-1.0,	1.0,	2.0,	2.0},
		{2.0,	-1.0,	-1.0,	2.0,	2.0},
		{0.0,	0.0,	3.0,	2.0,	2.0},
		{2.0,	-1.0,	0.0,	2.0,	2.0}};

const  fNutationCoefficients NuCoefficients[63] = {
		{-171996.0,	-174.2,	92025.0,8.9},
		{-13187.0,	-1.6,  	5736.0,	-3.1},
		{-2274.0, 	-0.2,  	977.0,	-0.5},
		{2062.0,   	0.2,    -895.0,    0.5},
		{1426.0,    -3.4,    54.0,    -0.1},
		{712.0,    0.1,    -7.0,    0.0},
		{-517.0,    1.2,    224.0,    -0.6},
		{-386.0,    -0.4,    200.0,    0.0},
		{-301.0,    0.0,    129.0,    -0.1},
		{217.0,    -0.5,    -95.0,    0.3},
		{-158.0,    0.0,    0.0,    0.0},
		{129.0,	0.1,	-70.0,	0.0},
		{123.0,	0.0,	-53.0,	0.0},
		{63.0,	0.0,	0.0,	0.0},
		{63.0,	0.1,	-33.0,	0.0},
		{-59.0,	0.0,	26.0,	0.0},
		{-58.0,	-0.1,	32.0,	0.0},
		{-51.0,	0.0,	27.0,	0.0},
		{48.0,	0.0,	0.0,	0.0},
		{46.0,	0.0,	-24.0,	0.0},
		{-38.0,	0.0,	16.0,	0.0},
		{-31.0,	0.0,	13.0,	0.0},
		{29.0,	0.0,	0.0,	0.0},
		{29.0,	0.0,	-12.0,	0.0},
		{26.0,	0.0,	0.0,	0.0},
		{-22.0,	0.0,	0.0,	0.0},
		{21.0,	0.0,	-10.0,	0.0},
		{17.0,	-0.1,	0.0,	0.0},
		{16.0,	0.0,	-8.0,	0.0},
		{-16.0,	0.1,	7.0,	0.0},
		{-15.0,	0.0,	9.0,	0.0},
		{-13.0,	0.0,	7.0,	0.0},
		{-12.0,	0.0,	6.0,	0.0},
		{11.0,	0.0,	0.0,	0.0},
		{-10.0,	0.0,	5.0,	0.0},
		{-8.0,	0.0,	3.0,	0.0},
		{7.0,	0.0,	-3.0,	0.0},
		{-7.0,	0.0,	0.0,	0.0},
		{-7.0,	0.0,	3.0,	0.0},
		{-7.0,	0.0,	3.0,	0.0},
		{6.0,	0.0,	0.0,	0.0},
		{6.0,	0.0,	-3.0,	0.0},
		{6.0,	0.0,	-3.0,	0.0},
		{-6.0,	0.0,	3.0,	0.0},
		{-6.0,	0.0,	3.0,	0.0},
		{5.0,	0.0,	0.0,	0.0},
		{-5.0,	0.0,	3.0,	0.0},
		{-5.0,	0.0,	3.0,	0.0},
		{-5.0,	0.0,	3.0,	0.0},
		{4.0,	0.0,	0.0,	0.0},
		{4.0,	0.0,	0.0,	0.0},
		{4.0,	0.0,	0.0,	0.0},
		{-4.0,	0.0,	0.0,	0.0},
		{-4.0,	0.0,	0.0,	0.0},
		{-4.0,	0.0,	0.0,	0.0},
		{3.0,	0.0,	0.0,	0.0},
		{-3.0,	0.0,	0.0,	0.0},
		{-3.0,	0.0,	0.0,	0.0},
		{-3.0,	0.0,	0.0,	0.0},
		{-3.0,	0.0,	0.0,	0.0},
		{-3.0,	0.0,	0.0,	0.0},
		{-3.0,	0.0,	0.0,	0.0},
		{-3.0,	0.0,	0.0,	0.0}};






/** Calculate nutation of longitude and obliquity in degrees from Julian Ephemeris Day **/
fAstroNutationCoordinates TROAstConversions::GetNutation(Double_t JulianDay)
{

	Double_t D,M,MM,F,O,T,JulianEphemerisDay;
	Double_t CoefficentSine, CoefficentCos;
	Double_t Longitude = 0. , Obliquity = 0. ,Ecliptic , Argument;
	Int_t i;

	/* get julian ephemeris day */
	JulianEphemerisDay = JDtoJDE(JulianDay);

	/* calc T */
	T = (JulianEphemerisDay - 2451545.0)/36525;


	/* calculate D,M,M',F and Omega */
	D = (297.85036 + 445267.111480 * T - 0.0019142 * pow(T,2) + pow(T,3) / 189474.0)*M_PI/180.0;
	M = (357.52772 + 35999.050340 * T - 0.0001603 *  pow(T,2) - pow(T,3) / 300000.0)*M_PI/180.0;
	MM =(134.96298 + 477198.867398 * T + 0.0086972 *  pow(T,2) + pow(T,3) / 56250.0)*M_PI/180.0;
	F = (93.2719100 + 483202.017538 * T - 0.0036825 *  pow(T,2) + pow(T,3) / 327270.0)*M_PI/180.0;
	O = (125.04452 - 1934.136261 * T + 0.0020708 *  pow(T,2) + pow(T,3) / 450000.0)*M_PI/180.0;

	for (i=0; i< 63; i++) {
		/* calc coefficients of sine and cosine */
		CoefficentSine = (NuCoefficients[i].Longitude1 + (NuCoefficients[i].Longitude2 * T));
		CoefficentCos = (NuCoefficients[i].Obliquity1 + (NuCoefficients[i].Obliquity2 * T));

		Argument = NuArguments[i].D * D + NuArguments[i].M * M + NuArguments[i].MM * MM + NuArguments[i].F * F + NuArguments[i].O * O;

		Longitude += CoefficentSine * sin(Argument);
		Obliquity += CoefficentCos * cos(Argument);
	}

	/* change to arcsecs */
	Longitude /= (10000 * 60 * 60);
	Obliquity /= (10000 * 60 * 60);

	/* calculate mean ecliptic - Meeus 2nd edition */
	Ecliptic = 23.0 + 26.0 / 60.0 + 21.448 / 3600.0 - 46.8150/3600 * T - 0.00059/3600 * pow(T,2) + 0.001813/3600 * pow(T,3);

	/* Ecliptic += Obliquity;  Uncomment this if function should return true obliquity rather than mean obliquity */

	/* return results */
	return(fAstroNutationCoordinates(Longitude,Obliquity,Ecliptic));
}




fAstroUTCCoordinates TROAstConversions::UnixTime2UTC(const time_t UnixTime)
{
	struct tm * timeinfo;

	timeinfo = gmtime(&UnixTime);

	fAstroUTCCoordinates UTCTime(timeinfo->tm_mday, (timeinfo->tm_mon)+1, (timeinfo->tm_year)+1900, timeinfo->tm_hour, timeinfo->tm_min, (Double_t)timeinfo->tm_sec);

	return UTCTime;
}

time_t TROAstConversions::UTC2UnixTime(const fAstroUTCCoordinates UTCCoord)
{
	struct tm * timeinfo = 0;

	timeinfo->tm_mday = UTCCoord.day;
	timeinfo->tm_mon = UTCCoord.month - 1;
	timeinfo->tm_year = UTCCoord.year - 1900;
	timeinfo->tm_hour = UTCCoord.hour;
	timeinfo->tm_min = UTCCoord.min;
	timeinfo->tm_sec = (Int_t)UTCCoord.sec;

	return mktime(timeinfo);
}



Double_t TROAstConversions::UTC2JulianDay(const Int_t day, Int_t month, Int_t year, const Int_t hour, const Int_t min,const Double_t sec) 
{
	Double_t JulianDay;
	if (month <= 2) {month = month+12; year = year - 1;}
	JulianDay = (Int_t)(365.25 * year) + (Int_t)(30.6001 * (month + 1)) - 15 + 1720996.5 + day + ( 3600 * hour + 60 * min + sec ) / 86400;

	return(JulianDay);
}


Double_t TROAstConversions::LocalMeanSiderealTime(const Int_t day, Int_t month, Int_t year, const Int_t hour, const Int_t min,const Double_t sec,const Double_t Longitude)
{
	Double_t JulianDay,T, GST;
	if (month <= 2) {month = month+12; year = year - 1;}
	JulianDay = (Int_t)(365.25 * year) + (Int_t)(30.6001 * (month + 1)) - 15 + 1720996.5 + day + ( 3600 * hour + 60 * min + sec ) / 86400;
	T = (JulianDay - 2451545.0) / 36525.0;
	GST = 280.46061837 + 360.98564736629*(JulianDay-2451545.0) + 0.000387933 * pow(T,2) - pow(T,3)/38710000.0;

	// std::cout<< std::scientific << std::setprecision(15)<<JulianDay<<std::endl;

	//std::cout<<GST  + Longitude<<std::endl;

	return(fmod(GST,360) + Longitude);

}

void TROAstConversions::Equatorial2Horizontal(std::vector<fAstroCatalogueObject> &Collection,const Int_t day,  Int_t month,  Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude)
{

	fAstroHorizontalCoordinates HorizontalCoord;
	for(UInt_t i=0; i < Collection.size(); i++)
	{

		HorizontalCoord = Equatorial2Horizontal((Collection[i].EquatorialCoord.RA), Collection[i].EquatorialCoord.Decl,day,month,year,hour,min,sec,Longitude,Latitude);

		Collection[i].HorizontalCoord.Alt = HorizontalCoord.Alt;
		Collection[i].HorizontalCoord.Az = HorizontalCoord.Az;
		Collection[i].HorizontalCoord.AltSigma = abs(HorizontalCoord.AltSigma) * (Collection[i].EquatorialCoord.RASigma/Collection[i].EquatorialCoord.RA + Collection[i].EquatorialCoord.DeclSigma/abs(Collection[i].EquatorialCoord.Decl)) ;
		Collection[i].HorizontalCoord.AzSigma = abs(HorizontalCoord.AzSigma) * (Collection[i].EquatorialCoord.RASigma/Collection[i].EquatorialCoord.RA + Collection[i].EquatorialCoord.DeclSigma/abs(Collection[i].EquatorialCoord.Decl)) ;
		Collection[i].UsedHorizontal = 1;
	}

}

void TROAstConversions::Equatorial2Horizontal(std::vector<fAstroCatalogueObject> &Collection,const fAstroUTCCoordinates UTCCoord,const fAstroLatLongCoordinates LatLongCoord)
{

	Equatorial2Horizontal(Collection,UTCCoord.day, UTCCoord.month, UTCCoord.year, UTCCoord.hour, UTCCoord.min, UTCCoord.sec, LatLongCoord.Longitude, LatLongCoord.Latitude);

}


fAstroHorizontalCoordinates TROAstConversions::Equatorial2Horizontal(const Double_t RA, const Double_t Decl,const Int_t day,  Int_t month,  Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude)
{

	Double_t LMST = LocalMeanSiderealTime(day, month, year,hour,min,sec, Longitude);
	Double_t RadLatitude  =  Latitude * M_PI/180.0;
	Double_t RadH  =  (LMST - RA) * M_PI/180.0;
	Double_t RadDec  =  Decl*M_PI/180.0 ;
	Double_t Az;

	Az  = (180.0/M_PI)*atan2(-cos(RadDec)*sin(RadH),-sin(RadLatitude)*cos(RadDec)*cos(RadH)+cos(RadLatitude)*sin(RadDec)) ;

	fAstroHorizontalCoordinates HorizontalCoord(90-(180.0/M_PI)*acos(sin(RadLatitude)*sin(RadDec) + cos(RadLatitude)*cos(RadDec)*cos(RadH)), Az < 0  ?  Az + 360 : Az  );

	return(HorizontalCoord);
}

fAstroHorizontalCoordinates TROAstConversions::Equatorial2Horizontal(const fAstroEquatorialCoordinates EquatorialCoord,const fAstroUTCCoordinates UTCCoord, fAstroLatLongCoordinates LatLongCoord)
{

	fAstroHorizontalCoordinates HorizontalCoord=Equatorial2Horizontal(EquatorialCoord.RA, EquatorialCoord.Decl, UTCCoord.day, UTCCoord.month, UTCCoord.year, UTCCoord.hour, UTCCoord.min, UTCCoord.sec, LatLongCoord.Longitude, LatLongCoord.Latitude);

	return(HorizontalCoord);
}


fAstroEquatorialCoordinates TROAstConversions::Horizontal2Equatorial(const Double_t Alt, const Double_t Az, const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude)
{

	Double_t LMST = LocalMeanSiderealTime(day, month, year,hour,min,sec, Longitude);
	Double_t H;
	Double_t RadLatitude  =  Latitude * M_PI/180.0;
	Double_t RadAz  =  Az*M_PI/180.0;
	Double_t RadAlt  =  (90-Alt)*M_PI/180.0;

	H = (180.0/M_PI)*atan2(- sin(RadAlt)*sin(RadAz),-sin(RadLatitude)*sin(RadAlt)*cos(RadAz)+cos(RadLatitude)*cos(RadAlt));
	fAstroEquatorialCoordinates EquatorialCoord( fmod(LMST - H,360) < 0  ?  fmod(LMST - H,360) + 360 :  fmod(LMST - H,360) ,(180.0/M_PI)*asin(sin(RadLatitude)*cos(RadAlt) + cos(RadLatitude)*sin(RadAlt)*cos(RadAz)));

	return(EquatorialCoord);
}


fAstroEquatorialCoordinates TROAstConversions::Horizontal2Equatorial(const fAstroHorizontalCoordinates HorizontalCoord,const fAstroUTCCoordinates UTCCoord, fAstroLatLongCoordinates LatLongCoord)
{

	fAstroEquatorialCoordinates EquatorialCoord=Horizontal2Equatorial(HorizontalCoord.Alt,HorizontalCoord.Az, UTCCoord.day, UTCCoord.month, UTCCoord.year, UTCCoord.hour, UTCCoord.min,UTCCoord.sec, LatLongCoord.Longitude, LatLongCoord.Latitude);


	return(EquatorialCoord);
}


void TROAstConversions::Equatorial2Galactic(std::vector<fAstroCatalogueObject> &Collection)
{
	fAstroGalacticCoordinates GalacticCoord;

	for(UInt_t i=0; i < Collection.size(); i++)
	{

		GalacticCoord = Equatorial2Galactic(Collection[i].EquatorialCoord.RA, Collection[i].EquatorialCoord.Decl );

		Collection[i].GalacticCoord.b = GalacticCoord.b;
		Collection[i].GalacticCoord.l = GalacticCoord.l;
		Collection[i].GalacticCoord.bSigma = abs(GalacticCoord.b) * (Collection[i].EquatorialCoord.RASigma/Collection[i].EquatorialCoord.RA + Collection[i].EquatorialCoord.DeclSigma/abs(Collection[i].EquatorialCoord.Decl)) ;
		Collection[i].GalacticCoord.lSigma = abs(GalacticCoord.l) * (Collection[i].EquatorialCoord.RASigma/Collection[i].EquatorialCoord.RA + Collection[i].EquatorialCoord.DeclSigma/abs(Collection[i].EquatorialCoord.Decl)) ;
		Collection[i].UsedGalactic = 1;
	}
}


fAstroGalacticCoordinates TROAstConversions::Equatorial2Galactic(const Double_t RA, const Double_t Decl)
{

	Double_t RadRa  =  RA*M_PI/180.0;
	Double_t RadDec  = Decl*M_PI/180.0;
	Double_t  l,b,CosDeltaG=cos((M_PI/180.0) * 27.12833),SinDeltaG=sin((M_PI/180.0) * 27.12833);

	b = (180.0/M_PI)*asin(sin(RadDec)*SinDeltaG + cos(RadDec)*CosDeltaG*cos((M_PI/180.0)*192.8595 - RadRa));

	if((CosDeltaG*sin(RadDec) - SinDeltaG*cos(RadDec)*cos(RadRa - (M_PI/180.0)*192.8595 ))/cos(b*(M_PI/180.0)) < 0) l = 122.9320 + (180.0/M_PI)*asin((sin(RadRa - (M_PI/180.0)*192.8595)*cos(RadDec))/cos(b*(M_PI/180.0))) - 180.0;
	else l = 122.9320 - (180.0/M_PI)*asin((sin(RadRa - (M_PI/180.0)*192.8595)*cos(RadDec))/cos(b*(M_PI/180.0)));

	if(l < 0) l = l + 360;

	fAstroGalacticCoordinates GalacticCoord(l,b);
	return(GalacticCoord);
}

fAstroGalacticCoordinates TROAstConversions::Equatorial2Galactic(const fAstroEquatorialCoordinates EquatorialCoord)
{

	fAstroGalacticCoordinates GalacticCoord=Equatorial2Galactic(EquatorialCoord.RA,EquatorialCoord.Decl);

	return(GalacticCoord);
}


fAstroEquatorialCoordinates TROAstConversions::Galactic2Equatorial(const Double_t l, const Double_t b)
{
	Double_t RA,Decl,Radl  =  l*M_PI/180.0, Radb  =  b*M_PI/180.0, CosDeltaG=cos((M_PI/180.0)*27.12833),SinDeltaG=sin((M_PI/180.0)*27.12833);

	Decl = (180.0/M_PI)*asin(sin(Radb)*SinDeltaG + cos(Radb)*CosDeltaG*cos((M_PI/180.0)*122.9320 - Radl));

	if(((CosDeltaG*sin(Radb) - SinDeltaG*cos(Radb)*cos(+ Radl - (M_PI/180.0)*122.9320))/cos((M_PI/180.0)*Decl)) < 0) RA = 192.8595 - (180.0/M_PI)*asin((sin(- Radl + (M_PI/180.0)*122.9320)*cos(Radb))/cos((M_PI/180.0)*Decl)) - 180.0;
	else RA = 192.8595 + (180.0/M_PI)*asin((sin(- Radl + (M_PI/180.0)*122.9320)*cos(Radb))/cos((M_PI/180.0)*Decl));
	if(RA < 0) RA = RA + 360;

	fAstroEquatorialCoordinates EquatorialCoord(RA,Decl);
	return(EquatorialCoord);
}


fAstroEquatorialCoordinates TROAstConversions::Galactic2Equatorial(const fAstroGalacticCoordinates GalacticCoord)
{

	fAstroEquatorialCoordinates EquatorialCoord=Galactic2Equatorial(GalacticCoord.l, GalacticCoord.b);

	return(EquatorialCoord);
}


Char_t TROAstConversions::UTMLetterDesignator(const Double_t Latitude) 
{
	Char_t LetterDesignator;

	if((84 >= Latitude)      && (Latitude >= 72))  LetterDesignator = 'X';
	else if((72 > Latitude)  && (Latitude >= 64))  LetterDesignator = 'W';
	else if((64 > Latitude)  && (Latitude >= 56))  LetterDesignator = 'V';
	else if((56 > Latitude)  && (Latitude >= 48))  LetterDesignator = 'U';
	else if((48 > Latitude)  && (Latitude >= 40))  LetterDesignator = 'T';
	else if((40 > Latitude)  && (Latitude >= 32))  LetterDesignator = 'S';
	else if((32 > Latitude)  && (Latitude >= 24))  LetterDesignator = 'R';
	else if((24 > Latitude)  && (Latitude >= 16))  LetterDesignator = 'Q';
	else if((16 > Latitude)  && (Latitude >= 8))   LetterDesignator = 'P';
	else if(( 8 > Latitude)  && (Latitude >= 0))   LetterDesignator = 'N';
	else if(( 0 > Latitude)  && (Latitude >= -8))  LetterDesignator = 'M';
	else if((-8 > Latitude)  && (Latitude >= -16)) LetterDesignator = 'L';
	else if((-16 > Latitude) && (Latitude >= -24)) LetterDesignator = 'K';
	else if((-24 > Latitude) && (Latitude >= -32)) LetterDesignator = 'J';
	else if((-32 > Latitude) && (Latitude >= -40)) LetterDesignator = 'H';
	else if((-40 > Latitude) && (Latitude >= -48)) LetterDesignator = 'G';
	else if((-48 > Latitude) && (Latitude >= -56)) LetterDesignator = 'F';
	else if((-56 > Latitude) && (Latitude >= -64)) LetterDesignator = 'E';
	else if((-64 > Latitude) && (Latitude >= -72)) LetterDesignator = 'D';
	else if((-72 > Latitude) && (Latitude >= -80)) LetterDesignator = 'C';
	else LetterDesignator = 'Z'; //This is here as an error flag to show that the Latitude is outside the UTM limits

	return LetterDesignator;
}


fAstroUTMCoordinates  TROAstConversions::LL2UTM(Int_t ReferenceEllipsoid, const Double_t Longitude, const Double_t Latitude)
{

	Double_t UTMNorthing, UTMEasting;
	std::string UTMZone;
	const Double_t eccentricity2 = utmellipsoid[ReferenceEllipsoid].eccentricitySquared;
	Double_t  LongOriginRad, eccPrime2;
	Double_t k0 = 0.9996, k1, k2, k3, k4, k5;
	Double_t LongTemp = (Longitude+180.0)-Int_t((Longitude+180.0)/360)*360-180.0; // -180.0.00 .. 179.9;
	const Double_t LatRad = Latitude *(M_PI / 180.0), LongRad = LongTemp* (M_PI / 180.0);
	Int_t  UTMZoneNumber;

	UTMZoneNumber = (Int_t)((LongTemp + 180.0)/6) + 1;

	if( Latitude >= 56.0 && Latitude < 64.0 && LongTemp >= 3.0 && LongTemp < 12.0 )
		UTMZoneNumber = 32;

	if( Latitude >= 72.0 && Latitude < 84.0 ) // Special zones for Svalbard
	{
		if(      LongTemp >= 0.0  && LongTemp <  9.0 ) UTMZoneNumber = 31;
		else if( LongTemp >= 9.0  && LongTemp < 21.0 ) UTMZoneNumber = 33;
		else if( LongTemp >= 21.0 && LongTemp < 33.0 ) UTMZoneNumber = 35;
		else if( LongTemp >= 33.0 && LongTemp < 42.0 ) UTMZoneNumber = 37;
	}
	LongOriginRad = ((UTMZoneNumber - 1)*6 - 180.0 + 3)*(M_PI / 180.0);  //+3 puts origin in middle of zone
	UTMZone = std::to_string(UTMZoneNumber) + std::to_string(UTMLetterDesignator(Latitude)); //compute the UTM Zone from the latitude and longitude
	eccPrime2 = (eccentricity2)/(1-eccentricity2);
	k1 = utmellipsoid[ReferenceEllipsoid].EquatorialRadius/sqrt(1-eccentricity2*pow(sin(LatRad),2));
	k2 = pow(tan(LatRad),2);
	k3 = eccPrime2*pow(cos(LatRad),2);
	k4 = cos(LatRad)*(LongRad-LongOriginRad);
	k5 = utmellipsoid[ReferenceEllipsoid].EquatorialRadius*((1- eccentricity2/4 - 3*pow(eccentricity2,2)/64 - 5*pow(eccentricity2,3)/256)*LatRad
			- (3*eccentricity2/8 + 3*pow(eccentricity2,2)/32 + 45*pow(eccentricity2,3)/1024)*sin(2*LatRad) + (15*pow(eccentricity2,2)/256
					+ 45*pow(eccentricity2,3)/1024)*sin(4*LatRad) - (35*pow(eccentricity2,3)/3072)*sin(6*LatRad));

	UTMEasting  = (Double_t)(k0*k1*(k4+(1-k2+k3)*pow(k4,3)/6+(5-18*k2+pow(k2,2)+72*k3-58*eccPrime2)*pow(k4,5)/120)+500000.0);
	UTMNorthing = (Double_t)(k0*(k5+k1*tan(LatRad)*(pow(k4,2)/2+(5-k2+9*k3+4*pow(k3,4))*pow(k4,4)/24+(61-58*pow(k2,6)+600*k3-330*eccPrime2)*pow(k4,6)/720)));

	if(Latitude < 0) UTMNorthing += 10000000.0; //10000000 meter offset for southern hemisphere

	fAstroUTMCoordinates UTMCoord(UTMNorthing,UTMEasting,UTMZone);
	return(UTMCoord);
}

fAstroUTMCoordinates  TROAstConversions::LL2UTM(const Int_t ReferenceEllipsoid, const fAstroLatLongCoordinates LatLongCoord)
{
	fAstroUTMCoordinates UTMCoord=LL2UTM(ReferenceEllipsoid,LatLongCoord.Longitude,LatLongCoord.Latitude);
	return(UTMCoord);
}


fAstroLatLongCoordinates TROAstConversions::UTM2LL(const Int_t ReferenceEllipsoid, const Double_t UTMNorthing, const Double_t UTMEasting, const std::string &UTMZone )
{
	Double_t Longitude,Latitude;
	Double_t eccPrime2, mu, phi1Rad, k1, k2, k3, k4, k5, k6,y = UTMNorthing;
	const Double_t eccentricity2 = utmellipsoid[ReferenceEllipsoid].eccentricitySquared, k0 = 0.9996;
	const Double_t e1 = (1-sqrt(1-eccentricity2))/(1+sqrt(1-eccentricity2));
	const Int_t UTMZoneNumber = std::stoi(UTMZone.substr(0, UTMZone.size()-1));
	const Char_t* ZoneLetter = (UTMZone.substr(UTMZone.size()-1, UTMZone.size())).c_str();
	//Int_t NorthernHemisphere; //1 for northern hemispher, 0 for southern

	if((*ZoneLetter - 'N') >= 0) ;//NorthernHemisphere = 1;//point is in northern hemisphere
	else
	{
		//NorthernHemisphere = 0;  //point is in southern hemisphere
		y -= 10000000.0;		 //remove 10,000,000 meter offset used for southern hemisphere
	}

	eccPrime2 = (eccentricity2)/(1-eccentricity2);
	k6 = UTMNorthing / k0;
	mu = k6/(utmellipsoid[ReferenceEllipsoid].EquatorialRadius * (1-eccentricity2/4-3*pow(eccentricity2,2)/64-5*pow(eccentricity2,3)/256));
	phi1Rad = mu + (3*e1/2-27*pow(e1,3)/32)*sin(2*mu) + (21*pow(e1,2)/16-55*pow(e1,4)/32)*sin(4*mu) +(151*pow(e1,3)/96)*sin(6*mu);
	k1 = utmellipsoid[ReferenceEllipsoid].EquatorialRadius/sqrt(1-eccentricity2*pow(sin(phi1Rad),2));
	k2 = pow(tan(phi1Rad),2);
	k3 = eccPrime2*pow(cos(phi1Rad),2);
	k4 = utmellipsoid[ReferenceEllipsoid].EquatorialRadius * ((1-eccentricity2)/pow(1-eccentricity2*pow(sin(phi1Rad),2), 1.5));
	k5 = (UTMEasting - 500000.0) /(k1*k0); //remove 500,000 meter offset for longitude

	Latitude = (phi1Rad - (k1*tan(phi1Rad)/k4)*(pow(k5,2)/2-(5+3*k2+10*k3-4*pow(k3,2)-9*eccPrime2)*pow(k5,4)/24 + (61+90*k2+298*k3+45*pow(k2,2)-252*eccPrime2-3*pow(k3,2))*pow(k5,6)/720))*(180.0 / M_PI);
	Longitude = ((UTMZoneNumber - 1)*6 - 180.0 + 3) + (k5-(1+2*k2+k3)*pow(k5,3)/6+(5-2*k3+28*k2-3*pow(k3,2)+8*eccPrime2+24*pow(k2,2))*pow(k5,5)/120)/cos(phi1Rad)*(180.0 / M_PI);
	//+3 puts origin in middle of zone

	fAstroLatLongCoordinates LatLongCoord(Latitude,Longitude);

	return(LatLongCoord);
}

fAstroLatLongCoordinates TROAstConversions::UTM2LL(const Int_t ReferenceEllipsoid, const fAstroUTMCoordinates UTMCoord)
{
	fAstroLatLongCoordinates LatLongCoord = UTM2LL(ReferenceEllipsoid, UTMCoord.UTMNorthing, UTMCoord.UTMEasting, UTMCoord.UTMZone);
	return(LatLongCoord);
}



fAstroEquatorialCoordinates TROAstConversions::Ecliptic2Equatorial(const Double_t JulianDay, const fAstroEclipticCoordinates EclipticCoord)
{
	Double_t RA, Decl;
	fAstroNutationCoordinates Nutation = GetNutation(JulianDay);

	Nutation.Ecliptic = (Nutation.Ecliptic)*(M_PI/180.0);

	RA   = atan2((sin(EclipticCoord.Longitude*(M_PI/180.0)) * cos(Nutation.Ecliptic) - tan((EclipticCoord.Latitude)*(M_PI/180.0)) * sin(Nutation.Ecliptic)), cos (EclipticCoord.Longitude*(M_PI/180.0)));
	Decl = asin(sin((EclipticCoord.Latitude)*(M_PI/180.0)) * cos(Nutation.Ecliptic) + cos((EclipticCoord.Latitude)*(M_PI/180.0)) * sin(Nutation.Ecliptic) * sin(EclipticCoord.Longitude*(M_PI/180.0)));


	fAstroEquatorialCoordinates EquatorialCoord(std::fmod(RA*(180.0/M_PI),360),Decl*(180.0/M_PI));
	return(EquatorialCoord);
}


fAstroEquatorialCoordinates TROAstConversions::Ecliptic2Equatorial(const fAstroEclipticCoordinates EclipticCoord)
{
	Double_t RA, Decl;

	RA   = atan2((sin(EclipticCoord.Longitude*(M_PI/180.0)) * cos((23.43)*(M_PI/180.0)) - tan((EclipticCoord.Latitude)*(M_PI/180.0)) * sin((23.43)*(M_PI/180.0))), cos (EclipticCoord.Longitude*(M_PI/180.0)));
	Decl = asin(sin((EclipticCoord.Latitude)*(M_PI/180.0)) * cos((23.43)*(M_PI/180.0)) + cos((EclipticCoord.Latitude)*(M_PI/180.0)) * sin((23.43)*(M_PI/180.0)) * sin(EclipticCoord.Longitude*(M_PI/180.0)));


	fAstroEquatorialCoordinates EquatorialCoord(std::fmod(RA*(180.0/M_PI),360),Decl*(180.0/M_PI));
	return(EquatorialCoord);
}




fAstroEclipticCoordinates TROAstConversions::Equatorial2Ecliptic(const Double_t JulianDay,const Double_t RA, const Double_t Decl)
{
	Double_t Latitude,Longitude;
	fAstroNutationCoordinates Nutation = GetNutation(JulianDay);

	Nutation.Ecliptic = (Nutation.Ecliptic)*(M_PI/180.0);

	Longitude = atan2((sin(RA*(M_PI/180.0)) * cos(Nutation.Ecliptic) + tan(Decl*(M_PI/180.0)) * sin(Nutation.Ecliptic)), cos(RA*(M_PI/180.0)));
	Latitude = asin(sin(Decl*(M_PI/180.0)) * cos(Nutation.Ecliptic) - cos(Decl*(M_PI/180.0)) * sin(Nutation.Ecliptic) * sin(RA*(M_PI/180.0)));

	fAstroEclipticCoordinates EclipticCoord(Latitude*(180.0/M_PI),std::fmod(Longitude*(180.0/M_PI),360));


	return(EclipticCoord);
}


fAstroEclipticCoordinates TROAstConversions::Equatorial2Ecliptic(const Double_t JulianDay, const fAstroEquatorialCoordinates EquatorialCoord)
{

	fAstroEclipticCoordinates EclipticCoord = Equatorial2Ecliptic(JulianDay,EquatorialCoord.RA,EquatorialCoord.Decl) ;


	return(EclipticCoord);
}



void TROAstConversions::Equatorial2Ecliptic(const Double_t JulianDay,std::vector<fAstroCatalogueObject> &Collection)
{
	fAstroEclipticCoordinates EclipticCoord;

	for(UInt_t i=0; i < Collection.size(); i++)
	{

		EclipticCoord = Equatorial2Ecliptic(JulianDay,Collection[i].EquatorialCoord.RA, Collection[i].EquatorialCoord.Decl);

		Collection[i].EclipticCoord.Latitude = EclipticCoord.Latitude;
		Collection[i].EclipticCoord.Longitude = EclipticCoord.Longitude;
		Collection[i].EclipticCoord.LatitudeSigma = abs(EclipticCoord.Latitude) * (Collection[i].EquatorialCoord.RASigma/Collection[i].EquatorialCoord.RA + Collection[i].EquatorialCoord.DeclSigma/abs(Collection[i].EquatorialCoord.Decl)) ;
		Collection[i].EclipticCoord.LongitudeSigma = abs(EclipticCoord.Longitude) * (Collection[i].EquatorialCoord.RASigma/Collection[i].EquatorialCoord.RA + Collection[i].EquatorialCoord.DeclSigma/abs(Collection[i].EquatorialCoord.Decl)) ;
		Collection[i].UsedEcliptic = 1;
	}
}

fAstroEclipticCoordinates TROAstConversions::Equatorial2Ecliptic(const Double_t RA, const Double_t Decl)
{
	Double_t Latitude,Longitude;

	Longitude = atan2((sin(RA*(M_PI/180.0)) * cos(23.43) + tan(Decl*(M_PI/180.0)) * sin(23.43)), cos(RA*(M_PI/180.0)));
	Latitude = asin(sin(Decl*(M_PI/180.0)) * cos(23.43) - cos(Decl*(M_PI/180.0)) * sin(23.43) * sin(RA*(M_PI/180.0)));

	fAstroEclipticCoordinates EclipticCoord(Latitude*(180.0/M_PI),std::fmod(Longitude*(180.0/M_PI),360));

	return(EclipticCoord);
}


fAstroEclipticCoordinates TROAstConversions::Equatorial2Ecliptic(const fAstroEquatorialCoordinates EquatorialCoord)
{

	fAstroEclipticCoordinates EclipticCoord = Equatorial2Ecliptic(EquatorialCoord.RA,EquatorialCoord.Decl) ;


	return(EclipticCoord);
}


void TROAstConversions::Equatorial2Ecliptic(std::vector<fAstroCatalogueObject> &Collection)
{
	fAstroEclipticCoordinates EclipticCoord;

	for(UInt_t i=0; i < Collection.size(); i++)
	{

		EclipticCoord = Equatorial2Ecliptic(Collection[i].EquatorialCoord.RA, Collection[i].EquatorialCoord.Decl);

		Collection[i].EclipticCoord.Latitude = EclipticCoord.Latitude;
		Collection[i].EclipticCoord.Longitude = EclipticCoord.Longitude;
		Collection[i].EclipticCoord.LatitudeSigma = abs(EclipticCoord.Latitude) * (Collection[i].EquatorialCoord.RASigma/Collection[i].EquatorialCoord.RA + Collection[i].EquatorialCoord.DeclSigma/abs(Collection[i].EquatorialCoord.Decl)) ;
		Collection[i].EclipticCoord.LongitudeSigma = abs(EclipticCoord.Longitude) * (Collection[i].EquatorialCoord.RASigma/Collection[i].EquatorialCoord.RA + Collection[i].EquatorialCoord.DeclSigma/abs(Collection[i].EquatorialCoord.Decl)) ;
		Collection[i].UsedEcliptic = 1;
	}
}




fAstroEclipticCoordinates TROAstConversions::Rectangular2Ecliptic(const fAstroRectangularCoordinates RectangularCoord)
{

	fAstroEclipticCoordinates EclipticCoord(atan2(RectangularCoord.Z,(sqrt((RectangularCoord.X * RectangularCoord.X) + (RectangularCoord.Y * RectangularCoord.Y)))),
			atan2(RectangularCoord.Y, RectangularCoord.X));

	EclipticCoord.Longitude = std::fmod((180.0/M_PI)*(EclipticCoord.Longitude),360);
	EclipticCoord.Latitude = (180.0/M_PI)*(EclipticCoord.Latitude);

	return(EclipticCoord);

}


fAstroRectangularCoordinates TROAstConversions::Heliocentric2Rectangular(const fAstroHeliosCoordinates HeliosPosition,Double_t JulianDay)
{

	Double_t sinE, cosE, cosB, sinB, sinL, cosL;

	/* get mean obliquity at of ecliptic epoch J2000 */
	fAstroNutationCoordinates Nutation=GetNutation(JulianDay);

	/* calculate cos, sin obliquity of ecliptic */
	sinE = sin((M_PI/180.0)*Nutation.Ecliptic);
	cosE = cos ((M_PI/180.0)*Nutation.Ecliptic);

	/* calc common values */
	cosB = cos((M_PI/180.0)*HeliosPosition.B);
	cosL = cos((M_PI/180.0)*HeliosPosition.L);
	sinB = sin((M_PI/180.0)*HeliosPosition.B);
	sinL = sin((M_PI/180.0)*HeliosPosition.L);

	fAstroRectangularCoordinates RectangularPosition(HeliosPosition.R * cosL * cosB,HeliosPosition.R * (sinL * cosB * cosE - sinB * sinE), HeliosPosition.R * (sinL * cosB * sinE + sinB * cosE));
	return(RectangularPosition);
}


}

