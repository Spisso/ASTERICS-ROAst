#include "TROAstGraphics.hh"

ClassImp(ROASt::TROAstGraphics);

namespace ROASt
{

 

void TROAstGraphics::Draw(std::vector<fAstroCatalogueObject> *Catalogue,const Option_t *option)
{


	fNpoints = Catalogue->size();
        TGraph::CtorAllocate();
        TGraphErrors::CtorAllocate();


	for(Int_t i=0; i < fNpoints ;i++ )
	{
		
                fX[i] = Catalogue->at(i).EquatorialCoord.RA;
		fEX[i] = Catalogue->at(i).EquatorialCoord.RASigma;
		fY[i] = Catalogue->at(i).EquatorialCoord.Decl;
		fEY[i] = Catalogue->at(i).EquatorialCoord.DeclSigma;


	}

	if(gAstroXAxisEnable == 1)
	{
		Double_t dx = TGraphErrors::GetXaxis()->GetXmin()+TGraphErrors::GetXaxis()->GetXmax();
		for (Int_t i=0; i<fNpoints; i++)
		{
			fX[i] = -fX[i]+dx;
		}
	}

	TAttMarker::ResetAttMarker(""); 	
        TAttMarker::SetMarkerStyle(1); 	
        TGraphErrors::Draw(option);

	
	if(gAstroXAxisEnable == 1)
	{
		// Remove the current axis
		TGraphErrors::GetXaxis()->SetLabelOffset(999);
		TGraphErrors::GetXaxis()->SetTickLength(0);

		// Redraw the new axis
		gPad->Update();
		TGaxis *newaxis = new TGaxis(gPad->GetUxmax(),
				gPad->GetUymin(),
				gPad->GetUxmin(),
				gPad->GetUymin(),
				TGraphErrors::GetXaxis()->GetXmin(),
				TGraphErrors::GetXaxis()->GetXmax(),
				510,"-SDH");
		newaxis->SetLabelOffset(-0.03);
		newaxis->Draw();



	}

}


void TROAstGraphics::Draw(fAstroEquatorialCoordinates &Position,const Option_t *option)
{

	fNpoints = 1; 	
	TGraph::CtorAllocate(); 	
	TGraphErrors::CtorAllocate();

        
	fX[0] = Position.RA;
	fY[0] = Position.Decl;
	fEX[0] = Position.RASigma;
	fEY[0] = Position.DeclSigma;

	if(gAstroXAxisEnable == 1)
	{
		Double_t dx = TGraphErrors::GetXaxis()->GetXmin()+TGraphErrors::GetXaxis()->GetXmax();
		fX[0] = -fX[0]+dx;
	}

	TAttMarker::ResetAttMarker(""); 	
	TAttMarker::SetMarkerStyle(3); 	
	TGraphErrors::Draw(option);


	if(gAstroXAxisEnable == 1)
	{
		// Remove the current axis
		TGraphErrors::GetXaxis()->SetLabelOffset(999);
		TGraphErrors::GetXaxis()->SetTickLength(0);

		// Redraw the new axis
		gPad->Update();
		TGaxis *newaxis = new TGaxis(gPad->GetUxmax(),
				gPad->GetUymin(),
				gPad->GetUxmin(),
				gPad->GetUymin(),
				TGraphErrors::GetXaxis()->GetXmin(),
				TGraphErrors::GetXaxis()->GetXmax(),
				510,"-SDH");
		newaxis->SetLabelOffset(-0.03);
		newaxis->Draw();



	}


}

void TROAstGraphics::Draw(std::vector<fAstroCatalogueObject>  *Catalogue,const std::string &CoordinateType,const Option_t *option)
{
	fNpoints = Catalogue->size();
	TGraph::CtorAllocate();
	TGraphErrors::CtorAllocate();

	if(CoordinateType == "Equatorial")
	{
		for(Int_t i=0; i < fNpoints;i++ )
		{
			fX[i] = Catalogue->at(i).EquatorialCoord.RA;
			fY[i] = Catalogue->at(i).EquatorialCoord.Decl;
			fEX[i] = Catalogue->at(i).EquatorialCoord.RASigma;
			fEY[i] = Catalogue->at(i).EquatorialCoord.DeclSigma;
		}
	}
	else if(CoordinateType == "Galactic")
	{
		for(Int_t i=0; i < fNpoints;i++ )
		{
			fX[i] = Catalogue->at(i).GalacticCoord.l;
			fY[i] = Catalogue->at(i).GalacticCoord.b;
			fEX[i] = Catalogue->at(i).GalacticCoord.lSigma;
			fEY[i] = Catalogue->at(i).GalacticCoord.bSigma;
		}
	}
	else if(CoordinateType == "Horizontal")
	{
		for(Int_t i=0; i < fNpoints;i++ )
		{
			fX[i] = Catalogue->at(i).HorizontalCoord.Az;
			fY[i] = Catalogue->at(i).HorizontalCoord.Alt;
			fEX[i] = Catalogue->at(i).HorizontalCoord.AzSigma;
			fEY[i] = Catalogue->at(i).HorizontalCoord.AltSigma;
		}
	}
	else throw GeneralException("Invalid coordinate type");    /* not a valid sequential number */

	if(gAstroXAxisEnable == 1)
	{
		Double_t dx = TGraphErrors::GetXaxis()->GetXmin()+TGraphErrors::GetXaxis()->GetXmax();
		for (Int_t i=0; i<fNpoints; i++)
		{
			fX[i] = -fX[i]+dx;
		}
	}

	TAttMarker::ResetAttMarker("");
	TGraphErrors::Draw(option);

	if(gAstroXAxisEnable == 1)
	{
		// Remove the current axis
		TGraphErrors::GetXaxis()->SetLabelOffset(999);
		TGraphErrors::GetXaxis()->SetTickLength(0);

		// Redraw the new axis
		gPad->Update();
		TGaxis *newaxis = new TGaxis(gPad->GetUxmax(),
				gPad->GetUymin(),
				gPad->GetUxmin(),
				gPad->GetUymin(),
				TGraphErrors::GetXaxis()->GetXmin(),
				TGraphErrors::GetXaxis()->GetXmax(),
				510,"-SDH");
		newaxis->SetLabelOffset(-0.03);
		newaxis->Draw();
	}

}


void TROAstGraphics::DrawFeature(std::vector<fAstroCatalogueObject> *Catalogue,const std::string &FeatureName,const Option_t *option)
{
	Int_t featurefound = 0;
	AstroGraph2D->Clear();
	AstroGraph2D->Set(Catalogue->size());

	std::map<std::string,Double_t> ::const_iterator MapIterator;
	std::map<std::string,Int_t> ::const_iterator IntMapIterator;

	for (IntMapIterator =  Catalogue->at(0).IntFeatures.begin(); IntMapIterator !=  Catalogue->at(0).IntFeatures.end();  IntMapIterator++)
	{
		if( FeatureName == IntMapIterator->first ) featurefound=1;
	}

	for (MapIterator =  Catalogue->at(0).RealFeatures.begin(); MapIterator !=  Catalogue->at(0).RealFeatures.end();  MapIterator++)
	{
		if( FeatureName == MapIterator->first ) featurefound=2;
	}

	if(featurefound == 1)
		for(UInt_t i=0; i < Catalogue->size();i++ )
		{
			AstroGraph2D->SetPoint(i,Catalogue->at(i).EquatorialCoord.RA,Catalogue->at(i).EquatorialCoord.Decl,(Catalogue->at(i).IntFeatures.find(FeatureName)->second));
		}
	else if(featurefound == 2)
		for(UInt_t i=0; i < Catalogue->size();i++ )
		{
			AstroGraph2D->SetPoint(i,Catalogue->at(i).EquatorialCoord.RA,Catalogue->at(i).EquatorialCoord.Decl,(Catalogue->at(i).RealFeatures.find(FeatureName)->second));
		}
	else  throw GeneralException("Invalid Feature name");

	gStyle->SetPalette(1);
	AstroGraph2D->SetMarkerStyle(20);
	AstroGraph2D->Draw(option);

}

void TROAstGraphics::DrawFeature(std::vector<fAstroCatalogueObject> *Catalogue,const std::string &CoordinateType,const std::string &FeatureName,const Option_t *option)
{
	Int_t featurefound = 0;
	AstroGraph2D->Clear();
	AstroGraph2D->Set(Catalogue->size());

	std::map<std::string,Double_t> ::const_iterator MapIterator;
	std::map<std::string,Int_t> ::const_iterator IntMapIterator;

	for (IntMapIterator =  Catalogue->at(0).IntFeatures.begin(); IntMapIterator !=  Catalogue->at(0).IntFeatures.end();  IntMapIterator++)
	{
		if( FeatureName == IntMapIterator->first ) featurefound=1;
	}

	for (MapIterator =  Catalogue->at(0).RealFeatures.begin(); MapIterator !=  Catalogue->at(0).RealFeatures.end();  MapIterator++)
	{
		if( FeatureName == MapIterator->first ) featurefound=2;
	}
	std::cout<<featurefound<<std::endl;

	if(CoordinateType == "Equatorial")
	{
		if(featurefound == 1)
			for(UInt_t i=0; i < Catalogue->size();i++ )
			{
				AstroGraph2D->SetPoint(i,Catalogue->at(i).EquatorialCoord.RA,Catalogue->at(i).EquatorialCoord.Decl,(Catalogue->at(i).IntFeatures.find(FeatureName)->second));
			}
		else if(featurefound == 2)
			for(UInt_t i=0; i < Catalogue->size();i++ )
			{
				AstroGraph2D->SetPoint(i,Catalogue->at(i).EquatorialCoord.RA,Catalogue->at(i).EquatorialCoord.Decl,(Catalogue->at(i).RealFeatures.find(FeatureName)->second));
			}
		else if (featurefound == 0) throw GeneralException("Invalid Feature name");
	}

	else if(CoordinateType == "Galactic")
	{
		if(featurefound == 1)
			for(UInt_t i=0; i < Catalogue->size();i++ )
			{
				AstroGraph2D->SetPoint(i,Catalogue->at(i).GalacticCoord.l,Catalogue->at(i).GalacticCoord.b,(Catalogue->at(i).IntFeatures.find(FeatureName)->second));
			}
		else if(featurefound == 2)
			for(UInt_t i=0; i < Catalogue->size();i++ )
			{
				AstroGraph2D->SetPoint(i,Catalogue->at(i).GalacticCoord.l,Catalogue->at(i).GalacticCoord.b,(Catalogue->at(i).RealFeatures.find(FeatureName)->second));
			}
		else throw GeneralException("Invalid Feature name");
	}
	else if(CoordinateType == "Horizontal")
	{
		if(featurefound == 1)
			for(UInt_t i=0; i < Catalogue->size();i++ )
			{
				AstroGraph2D->SetPoint(i,Catalogue->at(i).HorizontalCoord.Alt,Catalogue->at(i).HorizontalCoord.Az,(Catalogue->at(i).IntFeatures.find(FeatureName)->second));
			}
		else if(featurefound == 2)
			for(UInt_t i=0; i < Catalogue->size();i++ )
			{
				AstroGraph2D->SetPoint(i,Catalogue->at(i).HorizontalCoord.Alt,Catalogue->at(i).HorizontalCoord.Az,(Catalogue->at(i).RealFeatures.find(FeatureName)->second));
			}
		else throw GeneralException("Invalid Feature name");
	}
	else throw GeneralException("Invalid coordinate type");    /* not a valid sequential number */

	gStyle->SetPalette(1);
	AstroGraph2D->SetMarkerStyle(20);
	AstroGraph2D->Draw(option);
}


void TROAstGraphics::DrawAitoff(std::vector<fAstroCatalogueObject> *Catalogue,const Option_t *option)
{

	Double_t conv = M_PI/180;
	Double_t  x, y, z,alpha,Decl,RA;
	TGraphErrors::Clear();
	fNpoints = Catalogue->size();
	TGraph::CtorAllocate();
	TGraphErrors::CtorAllocate();
	std::vector<Double_t> X(fNpoints),Y(fNpoints);
	for(Int_t i=0; i < fNpoints;i++ )
	{
		alpha = acos(cos(Catalogue->at(i).EquatorialCoord.Decl*conv)*cos(-TMath::Pi()/2 + Catalogue->at(i).EquatorialCoord.RA*conv/2));
		z  = sin(alpha)/alpha;

		fX[i] = (180/M_PI)*2*cos(Catalogue->at(i).EquatorialCoord.Decl*conv)*sin(-TMath::Pi()/2 + Catalogue->at(i).EquatorialCoord.RA*conv/2)/z;
		fY[i] = (180/M_PI)*sin(Catalogue->at(i).EquatorialCoord.Decl*conv)/z;

		X[i] = Catalogue->at(i).EquatorialCoord.RA;
		Y[i] = Catalogue->at(i).EquatorialCoord.Decl;

		fEX[i] = 0 ;
		fEY[i] = 0 ;
	}

	TAttMarker::ResetAttMarker("");
	TAttMarker::SetMarkerStyle(1);

	TGraphErrors::Draw(option);


	Double_t ymin = *std::min_element(Y.begin(),Y.end());
	Double_t ymax = *std::max_element(Y.begin(),Y.end());
	Double_t xmin = *std::min_element(X.begin(),X.end());
	Double_t xmax = *std::max_element(X.begin(),X.end());
	Double_t dy = (ymax-ymin);
	Double_t dx = (xmax-xmin);
	ymin = ymin-0.1*dy;
	ymax = ymax+0.1*dy;
	xmin = xmin-0.1*dx;
	xmax = xmax+0.1*dx;
	dy = (ymax-ymin);
	dx = (xmax-xmin);

	const int Nl = 19; // Number of drawn latitudes
	const int NL = 19; // Number of drawn longitudes
	int       M  = 30;

	TGraph  *latitudes[Nl];
	TGraph  *longitudes[NL];

	for (int j=0;j<Nl;++j) {
		latitudes[j]=new TGraph();
		Decl = ymin + (Double_t)dy/(Nl-1)*j;
		for (int i=0;i<M+1;++i) {
			RA = xmin + (Double_t)dx/M*i;
			alpha  = acos(cos(Decl*conv)*cos(-TMath::Pi()/2 + RA*conv/2));
			z  = sin(alpha)/alpha;
			x  = (180/M_PI)*2*cos(Decl*conv)*sin(-TMath::Pi()/2 + RA*conv/2)/z;
			y  = (180/M_PI)*sin(Decl*conv)/z;

			latitudes[j]->SetPoint(i,x,y);
		}
	}

	for (int j=0;j<NL;++j) {
		longitudes[j]=new TGraph();
		RA = xmin + (Double_t)dx/(NL-1)*j;
		for (int i=0;i<M+1;++i) {
			Decl = ymin + (Double_t)dy/M*i;
			alpha  = acos(cos(Decl*conv)*cos( -TMath::Pi()/2 + RA*conv/2));
			z  = sin(alpha)/alpha;
			x  = (180/M_PI)*2*cos(Decl*conv)*sin( -TMath::Pi()/2 + RA*conv/2)/z;
			y  = (180/M_PI)*sin(Decl*conv)/z;
			longitudes[j]->SetPoint(i,x,y);
		}
	}





	for (int j=0;j<NL;++j){longitudes[j]->SetEditable(kFALSE); longitudes[j]->Draw("l");}
	for (int j=0;j<Nl;++j){latitudes[j]->SetEditable(kFALSE); latitudes[j]->Draw("l");}
}


void TROAstGraphics::DrawAitoff(fAstroEquatorialCoordinates &Position,Double_t Range ,const Option_t *option)
{

	Double_t conv = M_PI/180;
	Double_t  x, y, z,alpha,Decl,RA;
	TGraphErrors::Clear();
	fNpoints = 1;
	TGraph::CtorAllocate();
	TGraphErrors::CtorAllocate();

	alpha = acos(cos(Position.Decl*conv)*cos(-TMath::Pi()/2 + Position.RA*conv/2));
	z  = sin(alpha)/alpha;

	fX[0] = (180/M_PI)*2*cos(Position.Decl*conv)*sin(-TMath::Pi()/2 + Position.RA*conv/2)/z;
	fY[0] = (180/M_PI)*sin(Position.Decl*conv)/z;


	alpha = acos(cos((Position.Decl + Range)*conv)*cos(-TMath::Pi()/2 + (Position.RA + Range)*conv/2));
	z  = sin(alpha)/alpha;

	Double_t fXMax = (180/M_PI)*2*cos((Position.Decl + Range)*conv)*sin(-TMath::Pi()/2 + (Position.RA + Range)*conv/2)/z;
	Double_t fYMax = (180/M_PI)*sin((Position.Decl + Range)*conv)/z;


	alpha = acos(cos((Position.Decl - Range)*conv)*cos(-TMath::Pi()/2 + (Position.RA - Range)*conv/2));
	z  = sin(alpha)/alpha;

	Double_t fXMin = (180/M_PI)*2*cos((Position.Decl - Range)*conv)*sin(-TMath::Pi()/2 + (Position.RA - Range)*conv/2)/z;
	Double_t fYMin = (180/M_PI)*sin((Position.Decl - Range)*conv)/z;


	fEX[0] = 0 ;
	fEY[0] = 0 ;


	TGraph::GetXaxis()->SetLimits(fXMin,fXMax);
	TGraph::SetMaximum(fYMax);
	TGraph::SetMinimum(fYMin);


	TAttMarker::SetMarkerStyle(29);
	TAttMarker::SetMarkerColor(2);
	TGraphErrors::Draw(option);


	Double_t ymin = Position.Decl- Range;
	Double_t ymax = Position.Decl + Range;
	Double_t xmin = Position.RA - Range;
	Double_t xmax = Position.RA + Range;
	Double_t dy = (ymax-ymin);
	Double_t dx = (xmax-xmin);

	const int Nl = 19; // Number of drawn latitudes
	const int NL = 19; // Number of drawn longitudes
	int       M  = 30;

	TGraph  *latitudes[Nl];
	TGraph  *longitudes[NL];

	for (int j=0;j<Nl;++j) {
		latitudes[j]=new TGraph();
		Decl = ymin + (Double_t)dy/(Nl-1)*j;
		for (int i=0;i<M+1;++i) {
			RA = xmin + (Double_t)dx/M*i;
			alpha  = acos(cos(Decl*conv)*cos(-TMath::Pi()/2 + RA*conv/2));
			z  = sin(alpha)/alpha;
			x  = (180/M_PI)*2*cos(Decl*conv)*sin(-TMath::Pi()/2 + RA*conv/2)/z;
			y  = (180/M_PI)*sin(Decl*conv)/z;

			latitudes[j]->SetPoint(i,x,y);
		}
	}

	for (int j=0;j<NL;++j) {
		longitudes[j]=new TGraph();
		RA = xmin + (Double_t)dx/(NL-1)*j;
		for (int i=0;i<M+1;++i) {
			Decl = ymin + (Double_t)dy/M*i;
			alpha  = acos(cos(Decl*conv)*cos( -TMath::Pi()/2 + RA*conv/2));
			z  = sin(alpha)/alpha;
			x  = (180/M_PI)*2*cos(Decl*conv)*sin( -TMath::Pi()/2 + RA*conv/2)/z;
			y  = (180/M_PI)*sin(Decl*conv)/z;
			longitudes[j]->SetPoint(i,x,y);
		}
	}


	for (int j=0;j<NL;++j){longitudes[j]->SetEditable(kFALSE); longitudes[j]->Draw("l");}
	for (int j=0;j<Nl;++j){latitudes[j]->SetEditable(kFALSE); latitudes[j]->Draw("l");}
}





void TROAstGraphics::DrawSkyMap(std::vector<fAstroCatalogueObject> *Catalogue,const Option_t *option)
{

	Double_t conv=TMath::Pi()/180; // I am also aware of TMath::DegToRad() and TMath::Pi() which could be used there...
	Double_t  lo,la,x, y, z,alpha;

	fNpoints = Catalogue->size();
	TGraph::CtorAllocate();
	TGraphErrors::CtorAllocate();
	for(Int_t i=0; i < fNpoints;i++ )
	{
		alpha = acos(cos(Catalogue->at(i).EquatorialCoord.Decl*conv)*cos(-TMath::Pi()/2 + Catalogue->at(i).EquatorialCoord.RA*conv/2));
		z  = sin(alpha)/alpha;

		fX[i] = (180/M_PI)*2*cos(Catalogue->at(i).EquatorialCoord.Decl*conv)*sin(-TMath::Pi()/2 + Catalogue->at(i).EquatorialCoord.RA*conv/2)/z;
		fY[i] = (180/M_PI)*sin(Catalogue->at(i).EquatorialCoord.Decl*conv)/z;


		fEX[i] = 0;
		fEY[i] = 0 ;
	}

	TGraph::GetXaxis()->SetLimits(-180,180);
	TGraph::SetMaximum(90);
	TGraph::SetMinimum(-90);
	TAttMarker::SetMarkerStyle(1);

	TGraph::Draw(option);

	const int Nl = 19; // Number of drawn latitudes
	const int NL = 19; // Number of drawn longitudes
	int       M  = 30;

	TGraph  *latitudes[Nl];
	TGraph  *longitudes[NL];

	for (int j=0;j<Nl;++j) {
	      latitudes[j]=new TGraph();
	      la = -90+180/(Nl-1)*j;
	      for (int i=0;i<M+1;++i) {
	         lo = -180+360/M*i;
	         z  = sqrt(1+cos(la*conv)*cos(lo*conv/2));
	         x  = 180*cos(la*conv)*sin(lo*conv/2)/z;
	         y  = 90*sin(la*conv)/z;
	         latitudes[j]->SetPoint(i,x,y);
	      }
	   }

	   for (int j=0;j<NL;++j) {
	      longitudes[j]=new TGraph();
	      lo = -180+360/(NL-1)*j;
	      for (int i=0;i<M+1;++i) {
	         la = -90+180/M*i;
	         z  = sqrt(1+cos(la*conv)*cos(lo*conv/2));
	         x  = 180*cos(la*conv)*sin(lo*conv/2)/z;
	         y  = 90*sin(la*conv)/z;
	         longitudes[j]->SetPoint(i,x,y);
	      }
	   }

	// Draw the grid
	   TPad *pad2 = new TPad("pad2","",0,0,1,1);
	   pad2->SetFillStyle(4000);
	   pad2->SetFillColor(0);
	   pad2->SetBorderSize(0);
	   pad2->SetFrameBorderMode(0);
	   pad2->SetFrameLineColor(0);
	   pad2->SetFrameBorderMode(0);
	   pad2->Draw();
	   pad2->cd();
	   Double_t ymin = -89.5;
	   Double_t ymax = 89.5;
	   Double_t dy = (ymax-ymin)/0.8; //10 per cent margins top and bottom
	   Double_t xmin = -180;
	   Double_t xmax = 180;
	   Double_t dx = (xmax-xmin)/0.8; //10 per cent margins left and right

	   pad2->Range(xmin-0.1*dx,ymin-0.1*dy,xmax+0.1*dx,ymax+0.1*dy);

	   for (int j=0;j<NL;++j){longitudes[j]->SetEditable(kFALSE); longitudes[j]->Draw("l");}
	   for (int j=0;j<Nl;++j){latitudes[j]->SetEditable(kFALSE); latitudes[j]->Draw("l");}

}

void TROAstGraphics::DrawSkyMap(fAstroEquatorialCoordinates &Position ,const Option_t *option)
{

	Double_t conv = TMath::Pi()/180;
	Double_t  lo,la,x, y, z,alpha;
	TGraphErrors::Clear();
	fNpoints = 1 ;
	TGraph::CtorAllocate();
	TGraphErrors::CtorAllocate();

	alpha = acos(cos(Position.Decl*conv)*cos(-TMath::Pi()/2 + Position.RA*conv/2));
	z  = sin(alpha)/alpha;

	fX[0] = (180/M_PI)*2*cos(Position.Decl*conv)*sin(-TMath::Pi()/2 + Position.RA*conv/2)/z;
	fY[0] = (180/M_PI)*sin(Position.Decl*conv)/z;

	fEX[0] = 0 ;
	fEY[0] = 0 ;

	TGraph::GetXaxis()->SetLimits(-180,180);
	TGraph::SetMaximum(90);
	TGraph::SetMinimum(-90);
	TAttMarker::SetMarkerStyle(1);
	TAttMarker::SetMarkerColor(2);

	TGraphErrors::Draw(option);




	const int Nl = 19; // Number of drawn latitudes
	const int NL = 19; // Number of drawn longitudes
	int       M  = 30;

	TGraph  *latitudes[Nl];
	TGraph  *longitudes[NL];

	for (int j=0;j<Nl;++j) {
	      latitudes[j]=new TGraph();
	      la = -90+180/(Nl-1)*j;
	      for (int i=0;i<M+1;++i) {
	         lo = -180+360/M*i;
	         z  = sqrt(1+cos(la*conv)*cos(lo*conv/2));
	         x  = 180*cos(la*conv)*sin(lo*conv/2)/z;
	         y  = 90*sin(la*conv)/z;
	         latitudes[j]->SetPoint(i,x,y);
	      }
	   }

	   for (int j=0;j<NL;++j) {
	      longitudes[j]=new TGraph();
	      lo = -180+360/(NL-1)*j;
	      for (int i=0;i<M+1;++i) {
	         la = -90+180/M*i;
	         z  = sqrt(1+cos(la*conv)*cos(lo*conv/2));
	         x  = 180*cos(la*conv)*sin(lo*conv/2)/z;
	         y  = 90*sin(la*conv)/z;
	         longitudes[j]->SetPoint(i,x,y);
	      }
	   }

	// Draw the grid
	   TPad *pad2 = new TPad("pad2","",0,0,1,1);
	   pad2->SetFillStyle(4000);
	   pad2->SetFillColor(0);
	   pad2->SetBorderSize(0);
	   pad2->SetFrameBorderMode(0);
	   pad2->SetFrameLineColor(0);
	   pad2->SetFrameBorderMode(0);
	   pad2->Draw();
	   pad2->cd();
	   Double_t ymin = -89.5;
	   Double_t ymax = 89.5;
	   Double_t dy = (ymax-ymin)/0.8; //10 per cent margins top and bottom
	   Double_t xmin = -180;
	   Double_t xmax = 180;
	   Double_t dx = (xmax-xmin)/0.8; //10 per cent margins left and right

	   pad2->Range(xmin-0.1*dx,ymin-0.1*dy,xmax+0.1*dx,ymax+0.1*dy);

	   for (int j=0;j<NL;++j){longitudes[j]->SetEditable(kFALSE); longitudes[j]->Draw("l");}
	   for (int j=0;j<Nl;++j){latitudes[j]->SetEditable(kFALSE); latitudes[j]->Draw("l");}

}


}
