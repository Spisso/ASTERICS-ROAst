#include "TROAstURAT1.h"

//ClassImp(TROAstURAT1);
namespace ROASt
{


/* In the layout of the index file there are 1440
lines per dec Zone (of which there are,  of course,  900). Each line
contains 21 bytes,  except for the first,  which includes the dec
and is therefore six bytes longer. */


Long_t TROAstURAT1::GetIndexFileOffset(const Int_t Zone, const Int_t RaStart) const
{
	const Int_t FileOffset = ((Zone - 1) + RaStart * 900)*sizeof(int32_t);

	return(FileOffset);
}

void TROAstURAT1::ListFeatures()
{
	Object = (fAstroCatalogueObject)BaseObject;
	std::map<std::string,Double_t> ::const_iterator MapIterator;
	std::map<std::string,Int_t> ::const_iterator IntMapIterator;

	for ( IntMapIterator = Object.IntFeatures.begin(); IntMapIterator != Object.IntFeatures.end();  IntMapIterator++)
	{
		std::cout << IntMapIterator->first <<std::endl;
	}
	for ( MapIterator = Object.RealFeatures.begin(); MapIterator != Object.RealFeatures.end();  MapIterator++)
	{
		std::cout << MapIterator->first <<std::endl;
	}

}


void TROAstURAT1::Print()
{

	for(UInt_t j=0; j < ObjectsCollection.size(); j++)
	{
		std::map<std::string,Double_t> ::const_iterator MapIterator;
		std::map<std::string,Int_t> ::const_iterator IntMapIterator;

		std::cout<<"RA = "<<ObjectsCollection[j].EquatorialCoord.RA<<std::endl
				<<"Decl = "<<ObjectsCollection[j].EquatorialCoord.Decl<<std::endl
				<<"RASigma = "<<ObjectsCollection[j].EquatorialCoord.RASigma<<std::endl
				<<"DeclSigma = "<<ObjectsCollection[j].EquatorialCoord.DeclSigma<<std::endl
				<<"UsedHorizontal = "<<ObjectsCollection[j].UsedHorizontal<<std::endl
				<<"Alt = "<<ObjectsCollection[j].HorizontalCoord.Alt<<std::endl
				<<"Az = "<<ObjectsCollection[j].HorizontalCoord.Az<<std::endl
				<<"UsedGalactic = "<<ObjectsCollection[j].UsedGalactic<<std::endl
				<<"l = "<<ObjectsCollection[j].GalacticCoord.l<<std::endl
				<<"b = "<<ObjectsCollection[j].GalacticCoord.b<<std::endl
				<<"UsedEcliptic = "<<ObjectsCollection[j].UsedEcliptic<<std::endl
				<<"Latitude = "<<ObjectsCollection[j].EclipticCoord.Latitude<<std::endl
				<<"Longitude = "<<ObjectsCollection[j].EclipticCoord.Longitude<<std::endl;



		for (IntMapIterator = ObjectsCollection[j].IntFeatures.begin(); IntMapIterator != ObjectsCollection[j].IntFeatures.end();  IntMapIterator++)
		{
			std::cout << IntMapIterator->first << " = " <<IntMapIterator->second << std::endl;
		}

		for (MapIterator = ObjectsCollection[j].RealFeatures.begin(); MapIterator != ObjectsCollection[j].RealFeatures.end();  MapIterator++)
		{
			std::cout << MapIterator->first << " = " <<MapIterator->second << std::endl;
		}

		std::cout<<std::endl<<"--------------------------------------------"<<std::endl;
	}
}

void TROAstURAT1::Print(const Int_t j)
{

	std::map<std::string,Double_t> ::const_iterator MapIterator;
	std::map<std::string,Int_t> ::const_iterator IntMapIterator;

	std::cout<<"RA = "<<ObjectsCollection[j].EquatorialCoord.RA<<std::endl
			<<"Decl = "<<ObjectsCollection[j].EquatorialCoord.Decl<<std::endl
			<<"RASigma = "<<ObjectsCollection[j].EquatorialCoord.RASigma<<std::endl
			<<"DeclSigma = "<<ObjectsCollection[j].EquatorialCoord.DeclSigma<<std::endl
			<<"UsedHorizontal = "<<ObjectsCollection[j].UsedHorizontal<<std::endl
			<<"Alt = "<<ObjectsCollection[j].HorizontalCoord.Alt<<std::endl
			<<"Az = "<<ObjectsCollection[j].HorizontalCoord.Az<<std::endl
			<<"UsedGalactic = "<<ObjectsCollection[j].UsedGalactic<<std::endl
			<<"l = "<<ObjectsCollection[j].GalacticCoord.l<<std::endl
			<<"b = "<<ObjectsCollection[j].GalacticCoord.b<<std::endl
			<<"UsedEcliptic = "<<ObjectsCollection[j].UsedEcliptic<<std::endl
			<<"Latitude = "<<ObjectsCollection[j].EclipticCoord.Latitude<<std::endl
			<<"Longitude = "<<ObjectsCollection[j].EclipticCoord.Longitude<<std::endl;

	for (IntMapIterator = ObjectsCollection[j].IntFeatures.begin(); IntMapIterator != ObjectsCollection[j].IntFeatures.end();  IntMapIterator++)
	{
		std::cout << IntMapIterator->first << " = " <<IntMapIterator->second << std::endl;
	}

	for (MapIterator = ObjectsCollection[j].RealFeatures.begin(); MapIterator != ObjectsCollection[j].RealFeatures.end();  MapIterator++)
	{
		std::cout << MapIterator->first << " = " <<MapIterator->second << std::endl;
	}

	std::cout<<std::endl<<"--------------------------------------------"<<std::endl;
}



void TROAstURAT1::WriteObjects(const std::string &OutFileName)
{

	try{
		std::ofstream Outfile;
		Outfile.exceptions(std::ofstream::failbit | std::ofstream::badbit);
		Outfile.open(OutFileName, std::ios::out | std::ios::app);

		for(UInt_t j=0; j < ObjectsCollection.size(); j++)
		{
			std::map<std::string,Double_t> ::const_iterator MapIterator;
			std::map<std::string,Int_t> ::const_iterator IntMapIterator;

			Outfile<<"RA = "<<ObjectsCollection[j].EquatorialCoord.RA<<std::endl
					<<"Decl = "<<ObjectsCollection[j].EquatorialCoord.Decl<<std::endl
					<<"RASigma = "<<ObjectsCollection[j].EquatorialCoord.RASigma<<std::endl
					<<"DeclSigma = "<<ObjectsCollection[j].EquatorialCoord.DeclSigma<<std::endl
					<<"UsedHorizontal = "<<ObjectsCollection[j].UsedHorizontal<<std::endl
					<<"Alt = "<<ObjectsCollection[j].HorizontalCoord.Alt<<std::endl
					<<"Az = "<<ObjectsCollection[j].HorizontalCoord.Az<<std::endl
					<<"UsedGalactic = "<<ObjectsCollection[j].UsedGalactic<<std::endl
					<<"l = "<<ObjectsCollection[j].GalacticCoord.l<<std::endl
					<<"b = "<<ObjectsCollection[j].GalacticCoord.b<<std::endl
					<<"UsedEcliptic = "<<ObjectsCollection[j].UsedEcliptic<<std::endl
					<<"Latitude = "<<ObjectsCollection[j].EclipticCoord.Latitude<<std::endl
					<<"Longitude = "<<ObjectsCollection[j].EclipticCoord.Longitude<<std::endl;

			for (IntMapIterator = ObjectsCollection[j].IntFeatures.begin(); IntMapIterator != ObjectsCollection[j].IntFeatures.end();  IntMapIterator++)
			{
				Outfile << IntMapIterator->first << " = " <<IntMapIterator->second << std::endl;
			}

			for (MapIterator = ObjectsCollection[j].RealFeatures.begin(); MapIterator != ObjectsCollection[j].RealFeatures.end();  MapIterator++)
			{
				Outfile << MapIterator->first << " = " <<MapIterator->second << std::endl;
			}

			std::cout<<std::endl<<"--------------------------------------------"<<std::endl;

		}

		Outfile.close();
	}
	catch(std::exception const& e ) {std::cout << "Exception: " << e.what() << std::endl;}

}


void TROAstURAT1::ExtractObject(const Int_t Zone,const Long_t Offset, const std::string &PathZone)
{

	try{
		if(Zone < 1 || Zone > 900) throw GeneralException("Invalid Zone number.");    /* not a valid sequential number */


		std::ifstream ZoneFile;
		const std::string ZoneFilename = "z" + std::string(3 - std::to_string(Zone).length(), '0') + std::to_string(Zone);
		const std::string URAT1SdtPath = "./URAT1/u4b/";
		ObjectsCollection.clear();

		ZoneFile.open((URAT1SdtPath + ZoneFilename).c_str(), std::ios::binary | std::ios::in); /* First,  look for file in std Path: */

		if( !ZoneFile.is_open() && !PathZone.empty())  /* If file isn't there,  use the 'Path' passed in as an argument: */
			ZoneFile.open((PathZone + ZoneFilename).c_str(), std::ios::binary | std::ios::in);

		if(!ZoneFile.is_open()) throw GeneralException("Failed to open ZoneFile.");

		ZoneFile.seekg(Offset * SizefURAT1BaseObject);
		ZoneFile.read(reinterpret_cast<char *>(&BaseObject), SizefURAT1BaseObject);

		Object = (fAstroCatalogueObject)BaseObject;
		Object.RealFeatures.insert(std::make_pair("Zone",(Double_t)Zone));
		Object.RealFeatures.insert(std::make_pair("Offset",(Double_t)Offset));
		ObjectsCollection.push_back(Object);

		if(!ZoneFile.gcount()) throw GeneralException("Object not found.");    /* BaseObject not found */

		ZoneFile.close();

	}
	catch( std::exception const& e) {std::cout << "Exception: " << e.what() << std::endl;}

}


Int_t TROAstURAT1::ExtractObjects(const Double_t RA, const Double_t Decl, const Double_t Width, const Double_t Height,  const std::string &PathZone)
{

	if(RA < 0 || RA > 360 || Decl > 90 || Decl < -90  ) throw GeneralException(" Right ascension or declination out of range.");    /* Right ascension or declination out of range  */
	const Double_t Dec1 = Decl - Height / 2., Dec2 = Decl + Height / 2.;
	const Double_t Ra1 = RA - Width / 2., Ra2 = RA + Width / 2.;
	const Double_t kZoneHeight = .2;    /* zones are .2 degrees each */
	Int_t Zone = (Int_t)((Dec1  + 90.) / kZoneHeight) + 1;
	const Int_t EndZone = (Int_t)((Dec2 + 90.) / kZoneHeight) + 1;
	const Int_t kIndexRaResolution = 1440;  /* = .25 degrees */
	Int_t RaStart = (Int_t)(Ra1 * (Double_t)kIndexRaResolution / 360.);
	std::ifstream IndexFile;
	const std::string IndexFilename = "/v1index.unf";
	std::string URAT1SdtPath = "./URAT1/v12/";
	ObjectsCollection.clear();

	try
	{
		if(Zone < 326 || EndZone > 900) throw GeneralException(" Not allowed declination limit.");    /* Not allowed declination limit  */

		IndexFile.open((URAT1SdtPath + IndexFilename).c_str(),std::ios::binary | std::ios::in);  /* First,  look for file in sdt Path: */

		if(!IndexFile.is_open() && !URAT1IndexFilePath.empty())  /* If file isn't there,  use the 'Path' passed in as an argument: */
			IndexFile.open((URAT1IndexFilePath + IndexFilename).c_str(),std::ios::binary | std::ios::in);

		if(!IndexFile.is_open()) std::cout<<"Index file not found."<<std::endl;



		if(RaStart < 0) RaStart = 0;

		while( Zone <= EndZone)
		{
			std::ifstream ZoneFile;
			const std::string ZoneFilename = "z" +  std::string(3 - std::to_string(Zone).length(), '0') + std::to_string(Zone);

			URAT1SdtPath = "./URAT1/v12/";

			ZoneFile.open((URAT1SdtPath + ZoneFilename).c_str(), std::ios::binary | std::ios::in); /* First,  look for file in std Path: */

			if(!ZoneFile.is_open() && !PathZone.empty())  /* If file isn't there,  use the 'Path' passed in as an argument: */
				ZoneFile.open((PathZone + ZoneFilename).c_str(), std::ios::binary | std::ios::in);

			if(!ZoneFile.is_open()) throw GeneralException("Failed to open ZoneFile.");

			const int32_t MaxRa  = (int32_t)( Ra2 * 3600. * 1000.);
			const int32_t MinRa  = (int32_t)( Ra1 * 3600. * 1000.);
			const int32_t MinDec = (int32_t)( (Dec1 + 90.) * 3600. * 1000.);
			const int32_t MaxDec = (int32_t)( (Dec2 + 90.) * 3600. * 1000.);
			uint32_t Offset, EndOffset;
			const uint32_t acceptableStartLimit = 40;
			Long_t IndexFileOffset = GetIndexFileOffset(Zone,RaStart);
			static Long_t CachedIndexData[3] = {-1L, 0L, 0L};
			const uint32_t RaRange = (uint32_t)(360 * 3600 * 1000);
			uint32_t RaLo = (uint32_t)(RaStart * (RaRange / kIndexRaResolution));
			uint32_t RaHi = RaLo + RaRange / kIndexRaResolution;

			if(IndexFileOffset == CachedIndexData[0])
			{
				Offset = CachedIndexData[1];
				EndOffset = CachedIndexData[2];
			}
			else
			{
				if(IndexFile.is_open())
				{

					uint32_t loc;

					CachedIndexData[0] = IndexFileOffset;

					if(RaStart <= 0.25 )
						Offset = 0;
					else
					{
						IndexFile.seekg(IndexFileOffset - 900 * sizeof(int32_t),std::ios_base::beg);
						IndexFile.get(reinterpret_cast<char *>(&loc),sizeof(int32_t));
						Offset = (long)loc;
					}

					CachedIndexData[1] = Offset;
					IndexFile.seekg(IndexFileOffset,std::ios_base::beg);
					IndexFile.get(reinterpret_cast<char *>(&loc),sizeof(int32_t));
					CachedIndexData[2] = EndOffset=(long)loc;
				}
				else     /* no index:  binary-search within entire Zone: */
				{
					Offset = 0;
					ZoneFile.seekg(0L,std::ios_base::end);
					EndOffset = ZoneFile.tellg() / SizefURAT1BaseObject;
					RaLo = 0;
					RaHi = RaRange;
				}
			}



			while(EndOffset - Offset > acceptableStartLimit)
			{
				uint32_t DeltaOffset = EndOffset - Offset, TotalOtoffset;
				uint32_t MinimumOffset = DeltaOffset / 8 + 1;
				uint64_t NewOffset = (long)((uint64_t)DeltaOffset * (uint64_t)( MinRa - RaLo) / (uint64_t)( RaHi - RaLo));

				if(NewOffset < MinimumOffset) NewOffset = MinimumOffset;
				else if(NewOffset > DeltaOffset - MinimumOffset) NewOffset = DeltaOffset - MinimumOffset;

				TotalOtoffset = Offset + (uint32_t)NewOffset;
				ZoneFile.seekg(TotalOtoffset * SizefURAT1BaseObject,std::ios_base::beg);
				ZoneFile.read(reinterpret_cast<char *>(&BaseObject.RA), sizeof(BaseObject.RA));

				if(BaseObject.RA < MinRa)
				{
					Offset = TotalOtoffset;
					RaLo = BaseObject.RA;
				}
				else
				{
					EndOffset = TotalOtoffset;
					RaHi = BaseObject.RA;
				}
			}

			ZoneFile.seekg(Offset * SizefURAT1BaseObject,std::ios_base::beg);
			ZoneFile.read(reinterpret_cast<char *>(&BaseObject), SizefURAT1BaseObject);

			//std::cout<<" z"<< Zone <<std::endl;

			while(BaseObject.RA <= MaxRa && !ZoneFile.eof())
			{

				if(BaseObject.RA > MinRa && BaseObject.Spd > MinDec && BaseObject.Spd < MaxDec)
				{

					Object = (fAstroCatalogueObject)BaseObject;
					Object.IntFeatures.insert(std::make_pair("Zone",(Int_t)Zone));
					Object.IntFeatures.insert(std::make_pair("Offset",(Int_t)Offset));
					ObjectsCollection.push_back(Object);
				}
				Offset++;
				ZoneFile.seekg(Offset * SizefURAT1BaseObject,std::ios_base::beg);
				ZoneFile.read(reinterpret_cast<char *>(&BaseObject),SizefURAT1BaseObject);

			}
			ZoneFile.close();
			Zone++;
		}

		if(IndexFile.is_open()) IndexFile.close();

		if(RA > 0. &&  RA < 360.) /* We need some special handling for cases where the area to be extracted crosses RA=0 or RA=360: */
		{
			if(Ra1 < 0.)      /* left side crosses over RA=0h */
				ExtractObjects(RA + 360, Decl,Width,Height,PathZone);
			if(Ra2 > 360.)    /* right side crosses over RA=24h */
				ExtractObjects(RA - 360, Decl, Width, Height,PathZone);
		}


	}
	catch(std::exception const& e ) {std::cout << "Exception: " << e.what() << std::endl;}

	return(0);
}


std::vector<fAstroCatalogueObject>*  TROAstURAT1::ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height)
{

	ExtractObjectsRectangle(CoordinateType,RaGall,RaGalb,Width,Height,URAT1FilePath);
	return(&ObjectsCollection);
	
}	


std::vector<fAstroCatalogueObject>*  TROAstURAT1::ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Width, const Double_t Height,  const std::string &PathZone)
{

	if(CoordinateType == "Equatorial")
	{


		ExtractObjects(RaGall,DecGalb,Width,Height,PathZone);

	}
	else if(CoordinateType == "Galactic")
	{
		fAstroEquatorialCoordinates EquatorialCoord = Conv->Galactic2Equatorial(RaGall, DecGalb);
		fAstroEquatorialCoordinates EquatorialP1 = Conv->Galactic2Equatorial(RaGall - Width/2, DecGalb + Height/2);
		fAstroEquatorialCoordinates EquatorialP2 = Conv->Galactic2Equatorial(RaGall + Width/2, DecGalb + Height/2);
		fAstroEquatorialCoordinates EquatorialP3 = Conv->Galactic2Equatorial(RaGall + Width/2, DecGalb - Height/2);
		fAstroEquatorialCoordinates EquatorialP4 = Conv->Galactic2Equatorial(RaGall - Width/2, DecGalb - Height/2);

		std::vector<Double_t> Ras {EquatorialP1.RA,EquatorialP2.RA,EquatorialP3.RA,EquatorialP4.RA};
		std::vector<Double_t> Decls {EquatorialP1.Decl,EquatorialP2.Decl,EquatorialP3.Decl,EquatorialP4.Decl};

		ExtractObjects(EquatorialCoord.RA,EquatorialCoord.Decl,fabs( *min_element(Ras.begin(), Ras.end())- *max_element(Ras.begin(), Ras.end())),fabs( *min_element(Decls.begin(), Decls.end())- *max_element(Decls.begin(), Decls.end())),PathZone);

		Conv->Equatorial2Galactic(ObjectsCollection);

		for(UInt_t i=0; i < ObjectsCollection.size();)
		{
			if( !(fabs(ObjectsCollection[i].GalacticCoord.l - RaGall) < Width/2 && fabs(ObjectsCollection[i].GalacticCoord.b - DecGalb) < Height/2) )
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}
		}

	}
	else throw GeneralException("Invalid coordinate type.");    /* not a valid sequential number */
        std::cout<<"Objects found: "<< ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}

std::vector<fAstroCatalogueObject>*  TROAstURAT1::ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height,  const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude)
{

	ExtractObjectsRectangle(Alt,Az,Width,Height,day,month,year,hour,min,sec,Longitude,Latitude,URAT1FilePath);
	return(&ObjectsCollection);
	
}	

std::vector<fAstroCatalogueObject>*  TROAstURAT1::ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height, const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude,  const std::string &PathZone)
{

	fAstroEquatorialCoordinates EquatorialCoord = Conv->Horizontal2Equatorial(Alt, Az,day,month,year,hour,min,sec,Longitude,Latitude);
	fAstroEquatorialCoordinates EquatorialP1 = Conv->Horizontal2Equatorial(Alt - Width/2, Az + Height/2,day,month,year,hour,min,sec,Longitude,Latitude);
	fAstroEquatorialCoordinates EquatorialP2 = Conv->Horizontal2Equatorial(Alt + Width/2, Az + Height/2,day,month,year,hour,min,sec,Longitude,Latitude);
	fAstroEquatorialCoordinates EquatorialP3 = Conv->Horizontal2Equatorial(Alt + Width/2, Az - Height/2,day,month,year,hour,min,sec,Longitude,Latitude);
	fAstroEquatorialCoordinates EquatorialP4 = Conv->Horizontal2Equatorial(Alt - Width/2, Az - Height/2,day,month,year,hour,min,sec,Longitude,Latitude);

	std::vector<Double_t> Ras {EquatorialP1.RA,EquatorialP2.RA,EquatorialP3.RA,EquatorialP4.RA};
	std::vector<Double_t> Decls {EquatorialP1.Decl,EquatorialP2.Decl,EquatorialP3.Decl,EquatorialP4.Decl};

	//std::cout<<EquatorialCoord.first<<" "<<EquatorialCoord.second<<std::endl;

	//std::pair<Double_t,Double_t> HorizontalCoord=Conv->Equatorial2Horizontal(EquatorialCoord.first, EquatorialCoord.second,day,month,year,hour,min,sec,Longitude,Latitude);

	//std::cout<<HorizontalCoord.first<<" "<<HorizontalCoord.second<<std::endl;

	ExtractObjects(EquatorialCoord.RA,EquatorialCoord.Decl,fabs( *min_element(Ras.begin(), Ras.end())- *max_element(Ras.begin(), Ras.end())),fabs( *min_element(Decls.begin(), Decls.end())- *max_element(Decls.begin(), Decls.end())),PathZone);

	Conv->Equatorial2Horizontal(ObjectsCollection,day,month,year,hour,min,sec,Longitude,Latitude);

	for(UInt_t i=0; i < ObjectsCollection.size();)
	{
		if( !(fabs(ObjectsCollection[i].HorizontalCoord.Alt - Alt) < Width/2 && fabs(ObjectsCollection[i].HorizontalCoord.Az - Az) < Height/2) )
		{
			ObjectsCollection.erase(ObjectsCollection.begin()+i);
		}
		else{
			i++;
		}
	}
        std::cout<<"Objects found: "<< ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}



std::vector<fAstroCatalogueObject>*  TROAstURAT1::ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius)
{

	ExtractObjectsCircle(CoordinateType,RaGall,DecGalb,Radius,URAT1FilePath);
	return(&ObjectsCollection);
	
}


std::vector<fAstroCatalogueObject>* TROAstURAT1::ExtractObjectsCircle(const std::string &CoordinateType,const Double_t CenterRaGall, const Double_t CenterDecGalb,const Double_t Radius,  const std::string &PathZone)
{

	if(CoordinateType == "Equatorial")
	{

		ExtractObjects(CenterRaGall, CenterDecGalb, 2 * Radius, 2 * Radius ,PathZone);

		for(UInt_t i=0; i < ObjectsCollection.size();)
		{
			if(  pow((CenterRaGall - ObjectsCollection[i].EquatorialCoord.RA),2) +  pow((CenterDecGalb - ObjectsCollection[i].EquatorialCoord.Decl),2)  > pow(Radius,2) )
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}
		}
	}
	else if(CoordinateType == "Galactic")
	{

		ExtractObjectsRectangle("Galactic",CenterRaGall, CenterDecGalb, 2 * Radius, 2 * Radius ,PathZone);

		for(UInt_t i=0; i < ObjectsCollection.size();)
		{
			if( pow((CenterRaGall - ObjectsCollection[i].GalacticCoord.l),2) +  pow((CenterDecGalb - ObjectsCollection[i].GalacticCoord.b),2)  > pow(Radius,2) )
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}
		}
		Conv->Equatorial2Galactic(ObjectsCollection);
	}
	else throw GeneralException("Invalid coordinate type.");    /* not a valid sequential number */
        std::cout<<"Objects found: "<< ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);

}

std::vector<fAstroCatalogueObject>*  TROAstURAT1::ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius,const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude)
{

	ExtractObjectsCircle(Alt,Az,Radius,day,month,year,hour,min,sec,Longitude,Latitude,URAT1FilePath);
	return(&ObjectsCollection);

}

std::vector<fAstroCatalogueObject>* TROAstURAT1::ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude,  const std::string &PathZone)
{

	ExtractObjectsRectangle(Alt,Az, 2 * Radius, 2 * Radius,day, month,year,hour,min,sec,Longitude,Latitude,PathZone);

	for(UInt_t i=0; i < ObjectsCollection.size();){
		if(  pow((Alt - ObjectsCollection[i].HorizontalCoord.Alt),2) +  pow((Az - ObjectsCollection[i].HorizontalCoord.Az),2)  > pow(Radius,2) )
		{
			ObjectsCollection.erase(ObjectsCollection.begin()+i);

		}
		else{
			i++;
		}
	}
        std::cout<<"Objects found: "<< ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}

std::vector<fAstroCatalogueObject>*  TROAstURAT1::ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis)
{

	ExtractObjectsEllipse(CoordinateType,RaGall,DecGalb,FirstAxis,SecondAxis,URAT1FilePath);
	return(&ObjectsCollection);
	
}

std::vector<fAstroCatalogueObject>* TROAstURAT1::ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t CenterRaGall, const Double_t CenterDecGalb,const Double_t FirstAxis,const Double_t SecondAxis,  const std::string &PathZone)
{

	if(CoordinateType == "Equatorial")
	{


		ExtractObjects(CenterRaGall, CenterDecGalb,  FirstAxis,  SecondAxis, PathZone);

		for(UInt_t i=0; i < ObjectsCollection.size(); )
		{
			if( pow(2*(CenterRaGall - ObjectsCollection[i].EquatorialCoord.RA)/FirstAxis ,2)+ pow( 2*(CenterDecGalb - ObjectsCollection[i].EquatorialCoord.Decl)/SecondAxis,2) > 1.)
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}

		}
	}
	else if(CoordinateType == "Galactic")
	{

		ExtractObjectsRectangle("Galactic",CenterRaGall,CenterDecGalb,  FirstAxis, SecondAxis ,PathZone);

		for(UInt_t i=0; i < ObjectsCollection.size();)
		{
			if( pow(2*(CenterRaGall - ObjectsCollection[i].GalacticCoord.l)/FirstAxis ,2)+ pow( 2*(CenterDecGalb - ObjectsCollection[i].GalacticCoord.b)/SecondAxis,2) > 1.)
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}
		}
		Conv->Equatorial2Galactic(ObjectsCollection);
	}
	else throw GeneralException("Invalid coordinate type");    /* not a valid sequential number */
        std::cout<<"Objects found: "<< ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}

std::vector<fAstroCatalogueObject>*  TROAstURAT1::ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis,const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude)
{

	ExtractObjectsEllipse(Alt,Az,FirstAxis,SecondAxis,day,month,year,hour,min,sec,Longitude,Latitude,URAT1FilePath);
	return(&ObjectsCollection);

}

std::vector<fAstroCatalogueObject>* TROAstURAT1::ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude,  const std::string &PathZone)
{

	ExtractObjectsRectangle(Alt,Az, FirstAxis, SecondAxis,day, month,year,hour,min,sec,Longitude,Latitude,PathZone);

	for(UInt_t i=0; i < ObjectsCollection.size(); ){
		if(  pow(2*(Double_t)(Alt - ObjectsCollection[i].HorizontalCoord.Alt)/FirstAxis ,2)+ pow( 2*(Double_t)(Az - ObjectsCollection[i].HorizontalCoord.Az)/SecondAxis,2) > 1.)
		{
			ObjectsCollection.erase(ObjectsCollection.begin()+i);

		}
		else{
			i++;
		}

	}
        std::cout<<"Objects found: "<< ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}



Bool_t TROAstURAT1::FindObject(const std::string &Options,const std::string &FeatureName, const Double_t MinFeature, const Double_t MaxFeature)
{

	Int_t featurefound = 0;
	Bool_t found = false;
	Int_t j = 0;

	std::map<std::string,Double_t> ::const_iterator MapIterator;
	std::map<std::string,Int_t> ::const_iterator IntMapIterator;

	for (IntMapIterator =  ObjectsCollection[0].IntFeatures.begin(); IntMapIterator !=  ObjectsCollection[0].IntFeatures.end();  IntMapIterator++)
	{
		if( FeatureName == IntMapIterator->first ) featurefound=1;
	}

	for (MapIterator =  ObjectsCollection[0].RealFeatures.begin(); MapIterator !=  ObjectsCollection[0].RealFeatures.end();  MapIterator++)
	{
		if( FeatureName == MapIterator->first ) featurefound=2;
	}

	if(featurefound == 1)
		if(Options == "CheckOnly")
		{
			for(UInt_t i=0; i < ObjectsCollection.size();)
			{
				Double_t FeatureValue=ObjectsCollection[i].IntFeatures.find(FeatureName)->second ;
				if(MinFeature <= FeatureValue && FeatureValue <= MaxFeature)
				{
					j++;
					found = true;
				}
			}

			std::cout<<"Found "<<j<<" Object/s"<<std::endl;
		}
		else if(Options == "Filter")
		{
			for(UInt_t i=0; i < ObjectsCollection.size();)
			{
				Double_t FeatureValue=ObjectsCollection[i].IntFeatures.find(FeatureName)->second ;
				if(!(MinFeature <= FeatureValue && FeatureValue <= MaxFeature))
				{
					ObjectsCollection.erase(ObjectsCollection.begin()+i);
				}
				else
				{
					i++;
					found= true;
				}
			}
			std::cout<<"Found "<<ObjectsCollection.size()<<" Object/s"<<std::endl;
		}
		else throw GeneralException("Invalid option.");    /* not a valid sequential number */
	else if(featurefound == 1)
		if(Options == "CheckOnly")
		{
			for(UInt_t i=0; i < ObjectsCollection.size();)
			{
				Double_t FeatureValue=ObjectsCollection[i].RealFeatures.find(FeatureName)->second ;
				if(MinFeature <= FeatureValue && FeatureValue <= MaxFeature)
				{
					j++;
					found = true;
				}
			}

			std::cout<<"Found "<<j<<" Object/s"<<std::endl;
		}
		else if(Options == "Filter")
		{
			for(UInt_t i=0; i < ObjectsCollection.size();)
			{
				Double_t FeatureValue=ObjectsCollection[i].RealFeatures.find(FeatureName)->second ;
				if(!(MinFeature <= FeatureValue && FeatureValue <= MaxFeature))
				{
					ObjectsCollection.erase(ObjectsCollection.begin()+i);
				}
				else
				{
					i++;
					found= true;
				}
			}
			std::cout<<"Found "<<ObjectsCollection.size()<<" Object/s"<<std::endl;
		}
		else throw GeneralException("Invalid option.");    /* not a valid sequential number */
	else if(featurefound == 0) throw GeneralException("Invalid Feature name");

	return(found);
};

};


