#include "TROAstVizR.h"
#include "TROAstVOtParse.h"

//ClassImp(TROAstVizR);
namespace ROASt
{



Int_t TROAstVizR::CallConeService(const std::string &Source, Double_t RA, Double_t Decl, Double_t Radius)
{

	if(RA < 0 || RA > 360 || Decl > 90 || Decl < -90  ) throw GeneralException(" Right ascension or declination out of range  ");    /* Right ascension or declination out of range  */
	const Double_t Ra1 = RA - Radius / 2.;
    Int_t  NRows = 0;
	ObjectsCollection.clear();
    try
    {

    	Int_t  VOT = 0,Warn = 0, SigmaRACol=-1,SigmaDeclCol=-1,RACol = -1, DeclCol = -1, ErrRAOn=1,ErrDeclOn=1;
    	Int_t  i,Desc, Res, Tab, Data, TData, Field, NrAttr; //NrCols;
    	std::vector<std::string> AttrList, DataType;
    	std::string SAttr, cmd=VizQueryPath+"/vizquery -site="+VizieRSite+" -mime=votable -source="+Source+" -out.max=9999 -out.add=_1 -out.add=_r  -sort=_r  -c.rm="+std::to_string(Radius)+" -c="+std::to_string(RA)+"-"+std::to_string(Decl)+" 2> /dev/null";

    	char buffer[128];
    	std::string VizRVOTable = "";
    	FILE* pipe = popen(cmd.c_str(), "r");
    	if (!pipe) throw std::runtime_error("popen() failed!");
    	try
    	{
    		while (!feof(pipe))
    		{
    			if (fgets(buffer, 128, pipe) != NULL)
    				VizRVOTable += buffer;
    		}
    	}
    	catch (...)
    	{
    		pclose(pipe);
    		throw;
    	}
    	pclose(pipe);

    	vot_setWarnings(Warn);

    	VOT = vot_openVOTABLE(const_cast<char*>(VizRVOTable.c_str()));

    	Res   = vot_getRESOURCE (VOT);
    	Tab   = vot_getTABLE (Res);
    	Data  = vot_getDATA (Tab);

    	if((Desc = vot_getDESCRIPTION(VOT)))
    	{
    		std::cout<<std::endl<<vot_getValue(Desc)<<std::endl;
    	}

    	if (!Data) throw std::runtime_error("No objects in the selected region");
    	TData = vot_getTABLEDATA (Data);
    	NRows = vot_getNRows (TData);


    	//NrCols = vot_getNCols (TData);

    	for ( i=0, Field=vot_getFIELD(Tab); Field; Field=vot_getNext (Field),i++)
    	{

            AttrList.push_back(vot_getAttr(Field, (char*)"name"));
            DataType.push_back(vot_getAttr(Field, (char*)"datatype"));
            if((Desc = vot_getDESCRIPTION(Field)))
            {
            	Descripions.push_back(vot_getValue(Desc));
            }

            if( vot_getAttr(Field, (char*)"ID"))
            {
            	SAttr = vot_getAttr(Field, (char*)"ID");
            }
            else
            {
            	SAttr = vot_getAttr(Field, (char*)"name") ;
            }

            if (!SAttr.empty())
    		{
    			if ((SAttr == "RA") || (SAttr == "RA") ||(SAttr == "pos.eq.ra")) RACol = i;
    			if ((SAttr == "DEC") ||(SAttr == "DEC") || (SAttr == "pos.eq.dec")) DeclCol = i;
    			if (SAttr == "sRa") SigmaRACol = i;
    			if (SAttr == "sDec") SigmaDeclCol = i;
    		}

    	}
    	NrAttr = i;

    	if(RACol == -1 || DeclCol == -1 || SigmaRACol == -1 || SigmaDeclCol == -1)
    	{
    		std::cout<<std::endl<<"Standard Right Ascension and/or declination fields (and/or the related errors) not found please select them among the following catalogue fields: "<<std::endl<<std::endl;
    		for (i=0 ; i < NrAttr ; i++)
    		{
    			if(!Descripions.empty())
    			{
    				if(!Descripions[i].empty())
    				{
    				    Descripions[i].erase(Descripions[i].find_last_not_of(" \t\f\v\n\r")+1);
    				    std::cout<<(i+1)<<")"<<AttrList[i]<<" ("<<DataType[i]<<")"<<"   "<<Descripions[i]<<std::endl;
    				}
    			}
    			else std::cout<<(i+1)<<")"<<AttrList[i]<<" ("<<DataType[i]<<")"<<std::endl;
    		}

    		if(RACol == -1)
    		{
    			std::cout<<std::endl<<std::endl<<"Enter RA field: ";
    			std::cin>>RACol;
    			while(std::cin.fail()|| RACol < 1 || RACol > i+1)
    			{
    				std::cin.clear();
    				std::cin.ignore(10000, '\n');
    				std::cout << "Please, it must be a number between 1 and "<<NrAttr<<"!" << std::endl;
    				std::cin >> RACol;
    			}
    			RACol--;
    		}

    		if(SigmaRACol == -1)
    		{
    			std::cout<<std::endl<<"Enter RA error field, type 0 for none: ";
    			std::cin>>SigmaRACol;

    			while(std::cin.fail()|| SigmaRACol < 0 || SigmaRACol > NrAttr)
    			{
    				std::cin.clear();
    				std::cin.ignore(10000, '\n');
    				std::cout << "Please, it must be a number between 0 and "<<NrAttr<<"!" << std::endl;
    				std::cin >> SigmaRACol;
    			}
    			if (SigmaRACol == 0) { std::cout << "RA error not used"<<std::endl; ErrRAOn = 0;}
    			else {SigmaRACol--;}
    		}

    		if(DeclCol == -1)
    		{
    			std::cout<<std::endl<<"Enter declination field: ";
    			std::cin>>DeclCol;
    			while(std::cin.fail()|| DeclCol < 1 || DeclCol >NrAttr)
    			{
    				std::cin.clear();
    				std::cin.ignore(10000, '\n');
    				std::cout << "Please, it must be a number between 1 and "<<NrAttr<<"!" << std::endl;
    				std::cin >> DeclCol;
    			}

    			DeclCol--;
    		}

    		if(SigmaDeclCol == -1)
    		{
    			std::cout<<std::endl<<"Enter declination error field, type 0 for none: ";
    			std::cin>>SigmaDeclCol;
    			while(std::cin.fail()|| SigmaDeclCol < 0 || SigmaDeclCol > i+1)
    			{
    				std::cin.clear();
    				std::cin.ignore(10000, '\n');
    				std::cout << "Please, it must be a number between 0 and "<<NrAttr<<"!" << std::endl;
    				std::cin >> SigmaDeclCol;
    			}
    			if (SigmaDeclCol == 0) { std::cout << "declination error not used"<<std::endl; ErrDeclOn = 0;}
    			else {SigmaDeclCol--;}

    		}
    	}

    	for(i = 0;i < NrAttr;i++)
    	{
    		if((AttrList[i] != AttrList[RACol])&&(AttrList[i] != AttrList[DeclCol]))
    		{
    			if (DataType[i] == "float" || DataType[i]=="double" )
    			{
    				Object.RealFeatures.insert(std::make_pair(AttrList[i],0.));
    			}
    			else if(DataType[i] == "intr" || DataType[i] == "int" || DataType[i] == "unsignedByte"|| DataType[i] == "short")
    			{
    				Object.IntFeatures.insert(std::make_pair(AttrList[i],0.));
    			}
    			else if(DataType[i] == "char")
    			{
    				Object.StringFeatures.insert(std::make_pair(AttrList[i]," "));
    			}
    		}
    	}

    	for (i=0; i < NRows; i++)
    	{
    		Object.EquatorialCoord.RA   = atof(vot_getTableCell (TData, i, RACol));
    		Object.EquatorialCoord.Decl = atof(vot_getTableCell (TData, i, DeclCol));

    		if(ErrRAOn!=0)
    		{
    			Object.EquatorialCoord.RASigma = atof(vot_getTableCell (TData, i, SigmaRACol))/ 3600000;
    		}
    		else
    		{
    			Object.EquatorialCoord.RASigma = 0.;
    		}

    		if(ErrDeclOn)
    		{
    			Object.EquatorialCoord.DeclSigma = atof(vot_getTableCell (TData, i, SigmaDeclCol))/ 3600000;
    		}
    		else
    		{
    			Object.EquatorialCoord.DeclSigma = 0.;
    		}

    		for(Int_t j = 0;j < NrAttr;j++)
    		{
    			if((AttrList[j] != AttrList[RACol])&&(AttrList[j] != AttrList[DeclCol]))
    			{
    				if (DataType[j] == "float" || DataType[j]=="double" )
    				{

    					Object.RealFeatures[AttrList[j]]=atof(vot_getTableCell (TData, i, j));

    				}
    				else if(DataType[j] == "intr" || DataType[j] == "int" )
    				{

    					Object.IntFeatures[AttrList[j]]=atoi(vot_getTableCell (TData, i, j));

    				}
    				else if(DataType[j] == "char")
    				{
    					Object.StringFeatures[AttrList[j]]=vot_getTableCell(TData, i, j);

    				}
    			}
    		}

    		ObjectsCollection.push_back(Object);
    	}


    	vot_closeVOTABLE (VOT);

    	if(RA > 0. &&  RA < 360.)
    	{
    		if(Ra1 < 0.)
    			CallConeService(Source,RA + 360, Decl, Radius);
    		if(Ra1 > 360.)
    			CallConeService(Source,RA - 360, Decl, Radius);
    	}


	}
	catch(std::exception const& e ) {std::cout << "Exception: " << e.what() << std::endl;}

	return(NRows);

}




void TROAstVizR::ListFeatures()
{

	std::map<std::string,Double_t> ::const_iterator MapIterator;
	std::map<std::string,Int_t> ::const_iterator IntMapIterator;
	std::map<std::string,std::string> ::const_iterator SMapIterator;


	if(!Descripions.empty())
	{
		std::cout <<std::endl;
		Int_t i = 0;
		for ( IntMapIterator = Object.IntFeatures.begin(); IntMapIterator != Object.IntFeatures.end();IntMapIterator++)
		{


			if(!Descripions[i].empty())
			{
				Descripions[i].erase(Descripions[i].find_last_not_of(" \t\f\v\n\r")+1);
				std::cout << IntMapIterator->first <<" (int)   "<<Descripions[i]<<std::endl;
			}
			i++;
		}

		i = 0;

		for (MapIterator = Object.RealFeatures.begin(); MapIterator != Object.RealFeatures.end();  MapIterator++)
		{

			if(!Descripions[i].empty())
			{
				Descripions[i].erase(Descripions[i].find_last_not_of(" \t\f\v\n\r")+1);
				std::cout << MapIterator->first<<" (double)   "<<Descripions[i]<<std::endl;
			}
			i++;
		}

		i = 0;
		for ( SMapIterator = Object.StringFeatures.begin(); SMapIterator != Object.StringFeatures.end(); SMapIterator++)
		{


			if(!Descripions[i].empty())
			{
				Descripions[i].erase(Descripions[i].find_last_not_of(" \t\f\v\n\r")+1);
				std::cout << SMapIterator->first<<" (string)   "<<Descripions[i]<<std::endl;
			}
			i++;

		}

	}
	else
	{

		for ( IntMapIterator = Object.IntFeatures.begin(); IntMapIterator != Object.IntFeatures.end();  IntMapIterator++)
		{
			std::cout << IntMapIterator->first <<" (int)"<<std::endl;
		}
		for ( MapIterator = Object.RealFeatures.begin(); MapIterator != Object.RealFeatures.end();  MapIterator++)
		{
			std::cout << MapIterator->first<<" (double)"<<std::endl;
		}
		for ( SMapIterator = Object.StringFeatures.begin(); SMapIterator != Object.StringFeatures.end();  SMapIterator++)
		{
			std::cout << SMapIterator->first<<" (string)" <<std::endl;
		}

	}
}


void TROAstVizR::Print()
{

	for(UInt_t j=0; j < ObjectsCollection.size(); j++)
	{
		std::map<std::string,Double_t> ::const_iterator MapIterator;
		std::map<std::string,Int_t> ::const_iterator IntMapIterator;
		std::map<std::string,std::string> ::const_iterator SMapIterator;

		std::cout<<"RA = "<<ObjectsCollection[j].EquatorialCoord.RA<<std::endl
			 	 <<"Decl = "<<ObjectsCollection[j].EquatorialCoord.Decl<<std::endl
			  	 <<"RASigma = "<<ObjectsCollection[j].EquatorialCoord.RASigma<<std::endl
				 <<"DeclSigma = "<<ObjectsCollection[j].EquatorialCoord.DeclSigma<<std::endl
				 <<"UsedHorizontal = "<<ObjectsCollection[j].UsedHorizontal<<std::endl
				 <<"Alt = "<<ObjectsCollection[j].HorizontalCoord.Alt<<std::endl
				 <<"Az = "<<ObjectsCollection[j].HorizontalCoord.Az<<std::endl
				 <<"UsedGalactic = "<<ObjectsCollection[j].UsedGalactic<<std::endl
				 <<"l = "<<ObjectsCollection[j].GalacticCoord.l<<std::endl
				 <<"b = "<<ObjectsCollection[j].GalacticCoord.b<<std::endl
				 <<"UsedEcliptic = "<<ObjectsCollection[j].UsedEcliptic<<std::endl
				 <<"Latitude = "<<ObjectsCollection[j].EclipticCoord.Latitude<<std::endl
				 <<"Longitude = "<<ObjectsCollection[j].EclipticCoord.Longitude<<std::endl;



		for (IntMapIterator = ObjectsCollection[j].IntFeatures.begin(); IntMapIterator != ObjectsCollection[j].IntFeatures.end();  IntMapIterator++)
		{
			std::cout << IntMapIterator->first << " = " <<IntMapIterator->second << std::endl;
		}

		for (MapIterator = ObjectsCollection[j].RealFeatures.begin(); MapIterator != ObjectsCollection[j].RealFeatures.end();  MapIterator++)
		{
			std::cout << MapIterator->first << " = " <<MapIterator->second << std::endl;
		}

		for ( SMapIterator = Object.StringFeatures.begin(); SMapIterator != Object.StringFeatures.end();  SMapIterator++)
		{
			std::cout << SMapIterator->first << " = " <<SMapIterator->second << std::endl;
		}

		std::cout<<std::endl<<"--------------------------------------------"<<std::endl;


	}
}

void TROAstVizR::Print(const Int_t j)
{

	std::map<std::string,Double_t> ::const_iterator MapIterator;
	std::map<std::string,Int_t> ::const_iterator IntMapIterator;
	std::map<std::string,std::string> ::const_iterator SMapIterator;

	std::cout<<"RA = "<<ObjectsCollection[j].EquatorialCoord.RA<<std::endl
			<<"Decl = "<<ObjectsCollection[j].EquatorialCoord.Decl<<std::endl
			<<"RASigma = "<<ObjectsCollection[j].EquatorialCoord.RASigma<<std::endl
			<<"DeclSigma = "<<ObjectsCollection[j].EquatorialCoord.DeclSigma<<std::endl
			<<"UsedHorizontal = "<<ObjectsCollection[j].UsedHorizontal<<std::endl
			<<"Alt = "<<ObjectsCollection[j].HorizontalCoord.Alt<<std::endl
			<<"Az = "<<ObjectsCollection[j].HorizontalCoord.Az<<std::endl
			<<"UsedGalactic = "<<ObjectsCollection[j].UsedGalactic<<std::endl
			<<"l = "<<ObjectsCollection[j].GalacticCoord.l<<std::endl
			<<"b = "<<ObjectsCollection[j].GalacticCoord.b<<std::endl
			<<"UsedEcliptic = "<<ObjectsCollection[j].UsedEcliptic<<std::endl
			<<"Latitude = "<<ObjectsCollection[j].EclipticCoord.Latitude<<std::endl
			<<"Longitude = "<<ObjectsCollection[j].EclipticCoord.Longitude<<std::endl;

	for (IntMapIterator = ObjectsCollection[j].IntFeatures.begin(); IntMapIterator != ObjectsCollection[j].IntFeatures.end();  IntMapIterator++)
	{
		std::cout << IntMapIterator->first << " = " <<IntMapIterator->second << std::endl;
	}

	for (MapIterator = ObjectsCollection[j].RealFeatures.begin(); MapIterator != ObjectsCollection[j].RealFeatures.end();  MapIterator++)
	{
		std::cout << MapIterator->first << " = " <<MapIterator->second << std::endl;
	}

	for ( SMapIterator = Object.StringFeatures.begin(); SMapIterator != Object.StringFeatures.end();  SMapIterator++)
	{
		std::cout << SMapIterator->first << " = " <<SMapIterator->second << std::endl;
	}


	std::cout<<std::endl<<"--------------------------------------------"<<std::endl;
}



void TROAstVizR::WriteObjects(const std::string &OutFileName)
{

	try{
		std::ofstream Outfile;
		Outfile.exceptions(std::ofstream::failbit | std::ofstream::badbit);
		Outfile.open(OutFileName, std::ios::out | std::ios::app);
		std::map<std::string,std::string> ::const_iterator SMapIterator;

		for(UInt_t j=0; j < ObjectsCollection.size(); j++)
		{
			std::map<std::string,Double_t> ::const_iterator MapIterator;
			std::map<std::string,Int_t> ::const_iterator IntMapIterator;

			Outfile<<"RA = "<<ObjectsCollection[j].EquatorialCoord.RA<<std::endl
					<<"Decl = "<<ObjectsCollection[j].EquatorialCoord.Decl<<std::endl
					<<"RASigma = "<<ObjectsCollection[j].EquatorialCoord.RASigma<<std::endl
					<<"DeclSigma = "<<ObjectsCollection[j].EquatorialCoord.DeclSigma<<std::endl
					<<"UsedHorizontal = "<<ObjectsCollection[j].UsedHorizontal<<std::endl
					<<"Alt = "<<ObjectsCollection[j].HorizontalCoord.Alt<<std::endl
					<<"Az = "<<ObjectsCollection[j].HorizontalCoord.Az<<std::endl
					<<"UsedGalactic = "<<ObjectsCollection[j].UsedGalactic<<std::endl
					<<"l = "<<ObjectsCollection[j].GalacticCoord.l<<std::endl
					<<"b = "<<ObjectsCollection[j].GalacticCoord.b<<std::endl
					<<"UsedEcliptic = "<<ObjectsCollection[j].UsedEcliptic<<std::endl
					<<"Latitude = "<<ObjectsCollection[j].EclipticCoord.Latitude<<std::endl
					<<"Longitude = "<<ObjectsCollection[j].EclipticCoord.Longitude<<std::endl;

			for (IntMapIterator = ObjectsCollection[j].IntFeatures.begin(); IntMapIterator != ObjectsCollection[j].IntFeatures.end();  IntMapIterator++)
			{
				Outfile << IntMapIterator->first << " = " <<IntMapIterator->second << std::endl;
			}

			for (MapIterator = ObjectsCollection[j].RealFeatures.begin(); MapIterator != ObjectsCollection[j].RealFeatures.end();  MapIterator++)
			{
				Outfile << MapIterator->first << " = " <<MapIterator->second << std::endl;
			}

			for ( SMapIterator = Object.StringFeatures.begin(); SMapIterator != Object.StringFeatures.end();  SMapIterator++)
			{
				Outfile << SMapIterator->first << " = " <<SMapIterator->second << std::endl;
			}

			std::cout<<std::endl<<"--------------------------------------------"<<std::endl;

		}

		Outfile.close();
	}
	catch(std::exception const& e ) {std::cout << "Exception: " << e.what() << std::endl;}

}


std::vector<fAstroCatalogueObject>*  TROAstVizR::ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t RaGalb,const Double_t Width, const Double_t Height)
{

	ExtractObjectsRectangle(CoordinateType,RaGall,RaGalb,Width,Height,VizQueryPath);
	return(&ObjectsCollection);
	
}	



std::vector<fAstroCatalogueObject>* TROAstVizR::ExtractObjectsRectangle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Width, const Double_t Height, const std::string &Source)
{


	if(CoordinateType == "Equatorial")
	{

	CallConeService(Source, RaGall, DecGalb,  sqrt(pow(Width/2,2)+pow(Height/2,2)));

	for(UInt_t i=0; i < ObjectsCollection.size();)
		{
			if(  fabs(RaGall - ObjectsCollection[i].EquatorialCoord.RA) > Width/2 || fabs(DecGalb - ObjectsCollection[i].EquatorialCoord.Decl) > Height/2  )
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}
		}
	}


	else if(CoordinateType == "Galactic")
	{
		fAstroEquatorialCoordinates EquatorialCoord = Conv->Galactic2Equatorial(RaGall, DecGalb);
		fAstroEquatorialCoordinates EquatorialP1 = Conv->Galactic2Equatorial(RaGall - Width/2, DecGalb + Height/2);
		fAstroEquatorialCoordinates EquatorialP2 = Conv->Galactic2Equatorial(RaGall + Width/2, DecGalb + Height/2);
		fAstroEquatorialCoordinates EquatorialP3 = Conv->Galactic2Equatorial(RaGall + Width/2, DecGalb - Height/2);
		fAstroEquatorialCoordinates EquatorialP4 = Conv->Galactic2Equatorial(RaGall - Width/2, DecGalb - Height/2);

		std::vector<Double_t> Ras {EquatorialP1.RA,EquatorialP2.RA,EquatorialP3.RA,EquatorialP4.RA};
		std::vector<Double_t> Decls {EquatorialP1.Decl,EquatorialP2.Decl,EquatorialP3.Decl,EquatorialP4.Decl};

		ExtractObjectsRectangle("Equatorial",EquatorialCoord.RA,EquatorialCoord.Decl,fabs( *min_element(Ras.begin(), Ras.end())- *max_element(Ras.begin(), Ras.end())),fabs( *min_element(Decls.begin(), Decls.end())- *max_element(Decls.begin(), Decls.end())),Source);

		Conv->Equatorial2Galactic(ObjectsCollection);

		for(UInt_t i=0; i < ObjectsCollection.size();)
		{
			if( !(fabs(ObjectsCollection[i].GalacticCoord.l - RaGall) < Width/2 && fabs(ObjectsCollection[i].GalacticCoord.b - DecGalb) < Height/2) )
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}
		}

	}
	else throw GeneralException("Invalid coordinate type");    /* not a valid sequential number */
	std::cout<<"Objects found: "<<ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}

std::vector<fAstroCatalogueObject>* TROAstVizR::ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height,  const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude)
{

	ExtractObjectsRectangle(Alt,Az,Width,Height,day,month,year,hour,min,sec,Longitude,Latitude,VizQueryPath);
	return(&ObjectsCollection);
	
}	

std::vector<fAstroCatalogueObject>* TROAstVizR::ExtractObjectsRectangle(const Double_t Alt, const Double_t Az,const Double_t Width, const Double_t Height, const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &Source)
{

	fAstroEquatorialCoordinates EquatorialCoord = Conv->Horizontal2Equatorial(Alt, Az,day,month,year,hour,min,sec,Longitude,Latitude);
	fAstroEquatorialCoordinates EquatorialP1 = Conv->Horizontal2Equatorial(Alt - Width/2, Az + Height/2,day,month,year,hour,min,sec,Longitude,Latitude);
	fAstroEquatorialCoordinates EquatorialP2 = Conv->Horizontal2Equatorial(Alt + Width/2, Az + Height/2,day,month,year,hour,min,sec,Longitude,Latitude);
	fAstroEquatorialCoordinates EquatorialP3 = Conv->Horizontal2Equatorial(Alt + Width/2, Az - Height/2,day,month,year,hour,min,sec,Longitude,Latitude);
	fAstroEquatorialCoordinates EquatorialP4 = Conv->Horizontal2Equatorial(Alt - Width/2, Az - Height/2,day,month,year,hour,min,sec,Longitude,Latitude);

	std::vector<Double_t> Ras {EquatorialP1.RA,EquatorialP2.RA,EquatorialP3.RA,EquatorialP4.RA};
	std::vector<Double_t> Decls {EquatorialP1.Decl,EquatorialP2.Decl,EquatorialP3.Decl,EquatorialP4.Decl};

	//std::cout<<EquatorialCoord.first<<" "<<EquatorialCoord.second<<std::endl;

	//std::pair<Double_t,Double_t> HorizontalCoord=Conv->Equatorial2Horizontal(EquatorialCoord.first, EquatorialCoord.second,day,month,year,hour,min,sec,Longitude,Latitude);

	//std::cout<<HorizontalCoord.first<<" "<<HorizontalCoord.second<<std::endl;

	ExtractObjectsRectangle("Equatorial",EquatorialCoord.RA,EquatorialCoord.Decl,fabs( *min_element(Ras.begin(), Ras.end())- *max_element(Ras.begin(), Ras.end())),fabs( *min_element(Decls.begin(), Decls.end())- *max_element(Decls.begin(), Decls.end())),Source);

	Conv->Equatorial2Horizontal(ObjectsCollection,day,month,year,hour,min,sec,Longitude,Latitude);

	for(UInt_t i=0; i < ObjectsCollection.size();)
	{
		if( !(fabs(ObjectsCollection[i].HorizontalCoord.Alt - Alt) < Width/2 && fabs(ObjectsCollection[i].HorizontalCoord.Az - Az) < Height/2) )
		{
			ObjectsCollection.erase(ObjectsCollection.begin()+i);
		}
		else{
			i++;
		}
	}
	std::cout<<"Objects found: "<<ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);

}

std::vector<fAstroCatalogueObject>*  TROAstVizR::ExtractObjectsCircle(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t Radius)
{

	ExtractObjectsCircle(CoordinateType,RaGall,DecGalb,Radius,VizQueryPath);
	return(&ObjectsCollection);
	
}

std::vector<fAstroCatalogueObject>* TROAstVizR::ExtractObjectsCircle(const std::string &CoordinateType,const Double_t CenterRaGall, const Double_t CenterDecGalb,const Double_t Radius, const std::string &Source)
{

	if(CoordinateType == "Equatorial")
	{
		CallConeService(Source, CenterRaGall, CenterDecGalb,Radius);


	}
	else if(CoordinateType == "Galactic")
	{

		fAstroEquatorialCoordinates EquatorialCoord = Conv->Galactic2Equatorial(CenterRaGall, CenterDecGalb);
		fAstroEquatorialCoordinates EquatorialR1 = Conv->Galactic2Equatorial(CenterRaGall - Radius, CenterDecGalb);
		fAstroEquatorialCoordinates EquatorialR2 = Conv->Galactic2Equatorial(CenterRaGall + Radius, CenterDecGalb);
		fAstroEquatorialCoordinates EquatorialR3 = Conv->Galactic2Equatorial(CenterRaGall, CenterDecGalb - Radius);
		fAstroEquatorialCoordinates EquatorialR4 = Conv->Galactic2Equatorial(CenterRaGall, CenterDecGalb + Radius);

		std::vector<Double_t> RadiusS { sqrt(pow(EquatorialCoord.RA-EquatorialR1.RA,2)+ pow(EquatorialCoord.Decl-EquatorialR1.Decl,2)),
										sqrt(pow(EquatorialCoord.RA-EquatorialR2.RA,2)+ pow(EquatorialCoord.Decl-EquatorialR2.Decl,2)),
										sqrt(pow(EquatorialCoord.RA-EquatorialR3.RA,2)+ pow(EquatorialCoord.Decl-EquatorialR3.Decl,2)),
										sqrt(pow(EquatorialCoord.RA-EquatorialR4.RA,2)+ pow(EquatorialCoord.Decl-EquatorialR4.Decl,2))};

		CallConeService(Source,EquatorialCoord.RA,EquatorialCoord.Decl, *max_element(RadiusS.begin(), RadiusS.end()));

		Conv->Equatorial2Galactic(ObjectsCollection);

		for(UInt_t i=0; i < ObjectsCollection.size();)
		{
			if( pow((CenterRaGall - ObjectsCollection[i].GalacticCoord.l),2) +  pow((CenterDecGalb - ObjectsCollection[i].GalacticCoord.b),2)  > pow(Radius,2) )
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}
		}
	}
	else throw GeneralException("Invalid coordinate type");    /* not a valid sequential number */
	std::cout<<"Objects found: "<<ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}

std::vector<fAstroCatalogueObject>*  TROAstVizR::ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius,const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude)
{

	ExtractObjectsCircle(Alt,Az,Radius,day,month,year,hour,min,sec,Longitude,Latitude,VizQueryPath);
	return(&ObjectsCollection);

}

std::vector<fAstroCatalogueObject>* TROAstVizR::ExtractObjectsCircle(const Double_t Alt, const Double_t Az,const Double_t Radius, const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &Source)
{

	ExtractObjectsRectangle(Alt,Az, 2 * Radius, 2 * Radius,day, month,year,hour,min,sec,Longitude,Latitude,Source);

	for(UInt_t i=0; i < ObjectsCollection.size();){
		if(  pow((Alt - ObjectsCollection[i].HorizontalCoord.Alt),2) +  pow((Az - ObjectsCollection[i].HorizontalCoord.Az),2)  > pow(Radius,2) )
		{
			ObjectsCollection.erase(ObjectsCollection.begin()+i);

		}
		else{
			i++;
		}
	}
	std::cout<<"Objects found: "<<ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}

std::vector<fAstroCatalogueObject>*  TROAstVizR::ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t RaGall, const Double_t DecGalb,const Double_t FirstAxis,const Double_t SecondAxis)
{

	ExtractObjectsEllipse(CoordinateType,RaGall,DecGalb,FirstAxis,SecondAxis,VizQueryPath);
	return(&ObjectsCollection);
	
}

std::vector<fAstroCatalogueObject>* TROAstVizR::ExtractObjectsEllipse(const std::string &CoordinateType,const Double_t CenterRaGall, const Double_t CenterDecGalb,const Double_t FirstAxis,const Double_t SecondAxis, const std::string &Source)
{

	if(CoordinateType == "Equatorial")
	{


		ExtractObjectsRectangle("Equatorial",CenterRaGall, CenterDecGalb,  FirstAxis, SecondAxis, Source);

		for(UInt_t i=0; i < ObjectsCollection.size(); )
		{
			if( pow(2*(CenterRaGall - ObjectsCollection[i].EquatorialCoord.RA)/FirstAxis ,2)+ pow( 2*(CenterDecGalb - ObjectsCollection[i].EquatorialCoord.Decl)/SecondAxis,2) > 1.)
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}

		}
	}
	else if(CoordinateType == "Galactic")
	{

		ExtractObjectsRectangle("Galactic",CenterRaGall,CenterDecGalb,  FirstAxis, SecondAxis ,Source);

		for(UInt_t i=0; i < ObjectsCollection.size();)
		{
			if( pow(2*(CenterRaGall - ObjectsCollection[i].GalacticCoord.l)/FirstAxis ,2)+ pow( 2*(CenterDecGalb - ObjectsCollection[i].GalacticCoord.b)/SecondAxis,2) > 1.)
			{
				ObjectsCollection.erase(ObjectsCollection.begin()+i);
			}
			else{
				i++;
			}
		}
		Conv->Equatorial2Galactic(ObjectsCollection);
	}
	else throw GeneralException("Invalid coordinate type");    /* not a valid sequential number */
	std::cout<<"Objects found: "<<ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}

std::vector<fAstroCatalogueObject>*  TROAstVizR::ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis,const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude)
{

	ExtractObjectsEllipse(Alt,Az,FirstAxis,SecondAxis,day,month,year,hour,min,sec,Longitude,Latitude,VizQueryPath);
	return(&ObjectsCollection);

}

std::vector<fAstroCatalogueObject>* TROAstVizR::ExtractObjectsEllipse(const Double_t Alt, const Double_t Az,const Double_t FirstAxis,const Double_t SecondAxis, const Int_t day, const Int_t month, const Int_t year, const Int_t hour, const Int_t min,const Double_t sec, const Double_t Longitude, const Double_t Latitude, const std::string &Source)
{

	ExtractObjectsRectangle(Alt,Az, FirstAxis, SecondAxis,day, month,year,hour,min,sec,Longitude,Latitude,Source);

	for(UInt_t i=0; i < ObjectsCollection.size(); ){
		if(  pow(2*(Double_t)(Alt - ObjectsCollection[i].HorizontalCoord.Alt)/FirstAxis ,2)+ pow( 2*(Double_t)(Az - ObjectsCollection[i].HorizontalCoord.Az)/SecondAxis,2) > 1.)
		{
			ObjectsCollection.erase(ObjectsCollection.begin()+i);

		}
		else{
			i++;
		}

	}
	std::cout<<"Objects found: "<<ObjectsCollection.size()<<std::endl;
	return(&ObjectsCollection);
}



Bool_t TROAstVizR::FindObject(const std::string &Options,const std::string &FeatureName, const Double_t MinFeature, const Double_t MaxFeature)
{

	Int_t featurefound = 0;
	Bool_t found = false;
	Int_t j = 0;

	std::map<std::string,Double_t> ::const_iterator MapIterator;
	std::map<std::string,Int_t> ::const_iterator IntMapIterator;

	for (IntMapIterator =  ObjectsCollection[0].IntFeatures.begin(); IntMapIterator !=  ObjectsCollection[0].IntFeatures.end();  IntMapIterator++)
	{
		if( FeatureName == IntMapIterator->first ) featurefound=1;
	}

	for (MapIterator =  ObjectsCollection[0].RealFeatures.begin(); MapIterator !=  ObjectsCollection[0].RealFeatures.end();  MapIterator++)
	{
		if( FeatureName == MapIterator->first ) featurefound=2;
	}

	if(featurefound == 1)
		if(Options == "CheckOnly")
		{
			for(UInt_t i=0; i < ObjectsCollection.size();)
			{
				Double_t FeatureValue=ObjectsCollection[i].IntFeatures.find(FeatureName)->second ;
				if(MinFeature <= FeatureValue && FeatureValue <= MaxFeature)
				{
					j++;
					found = true;
				}
			}

			std::cout<<"Found "<<j<<" Object/s"<<std::endl;
		}
		else if(Options == "Filter")
		{
			for(UInt_t i=0; i < ObjectsCollection.size();)
			{
				Double_t FeatureValue=ObjectsCollection[i].IntFeatures.find(FeatureName)->second ;
				if(!(MinFeature <= FeatureValue && FeatureValue <= MaxFeature))
				{
					ObjectsCollection.erase(ObjectsCollection.begin()+i);
				}
				else
				{
					i++;
					found= true;
				}
			}
			std::cout<<"Found "<<ObjectsCollection.size()<<" Object/s"<<std::endl;
		}
		else throw GeneralException("Invalid option.");    /* not a valid sequential number */
	else if(featurefound == 1)
		if(Options == "CheckOnly")
		{
			for(UInt_t i=0; i < ObjectsCollection.size();)
			{
				Double_t FeatureValue=ObjectsCollection[i].RealFeatures.find(FeatureName)->second ;
				if(MinFeature <= FeatureValue && FeatureValue <= MaxFeature)
				{
					j++;
					found = true;
				}
			}

			std::cout<<"Found "<<j<<" Object/s"<<std::endl;
		}
		else if(Options == "Filter")
		{
			for(UInt_t i=0; i < ObjectsCollection.size();)
			{
				Double_t FeatureValue=ObjectsCollection[i].RealFeatures.find(FeatureName)->second ;
				if(!(MinFeature <= FeatureValue && FeatureValue <= MaxFeature))
				{
					ObjectsCollection.erase(ObjectsCollection.begin()+i);
				}
				else
				{
					i++;
					found= true;
				}
			}
			std::cout<<"Found "<<ObjectsCollection.size()<<" Object/s"<<std::endl;
		}
		else throw GeneralException("Invalid option.");    /* not a valid sequential number */
	else if(featurefound == 0) throw GeneralException("Invalid Feature name");

	return(found);
};





};

